Extreme commuting! Your way to work has never been so slightly extreme!

LICENSES:

Fonts:
======================================

Remarcle
Free font by Tup Wanders
http://www.tupwanders.nl
Licensed with a Creative Commons attribution license.
http://www.fontsc.com/font/remarcle

Sounds:
======================================

Car breaking sound, modified, source: https://freesound.org/people/audible-edge/sounds/76804/

Cheering sounds, edited:
https://freesound.org/people/jayfrosting/sounds/333404/
https://freesound.org/people/BerlinGameScene/sounds/267246/
https://freesound.org/people/sagetyrtle/sounds/32260/
https://freesound.org/people/nickrave/sounds/245639/

Car crash:
https://freesound.org/people/squareal/sounds/237375/
https://freesound.org/people/musicmasta1/sounds/131385/

Losing sounds, edited:
https://freesound.org/people/kirbydx/sounds/175409/ - wah wah - trombone
https://freesound.org/people/freki3333/sounds/131594/ - boo sound.

Thanks to www.bfxr.net for:
Player performing stunt sound,

Cash register sound: https://freesound.org/people/Zott820/sounds/209578/

Crowd clapping: https://freesound.org/people/Adam_N/sounds/324891/

Crystal glass: https://freesound.org/people/lmr9/sounds/178178/

Disappointed crowd (edited): https://freesound.org/people/Corsica_S/sounds/336998/

Car horn: https://freesound.org/people/guitarguy1985/sounds/54086/

Police siren: https://freesound.org/people/conleec/sounds/159753/

Achievement 'ding' sound: https://freesound.org/people/domrodrig/sounds/116779/
Achievement 'boom' sound: https://freesound.org/people/studiomandragore/sounds/401630/

Fireworks:
https://freesound.org/people/Rudmer_Rotteveel/sounds/336008/
https://freesound.org/people/Kinoton/sounds/347163/
https://freesound.org/people/zihengc/sounds/335983/
https://freesound.org/people/nfrae/sounds/167242/
https://freesound.org/people/j1987/sounds/140725/
https://freesound.org/people/insanity54/sounds/195181/
https://freesound.org/people/soundmary/sounds/117616/

Muscle car sounds:
https://www.youtube.com/watch?v=Q1sRxQoUh5g
https://www.youtube.com/watch?v=ZNQr1SxbtlU

Turbo sound (heavily edited):
https://freesound.org/people/applauseav/sounds/331610/

Music:
======================================

"Exit the Premises" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Ouroboros" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Severe Tire Damage" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Special Spotlight" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/';

"The Lift" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Militaire Electronic" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Presenterator" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Andreas Theme" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

