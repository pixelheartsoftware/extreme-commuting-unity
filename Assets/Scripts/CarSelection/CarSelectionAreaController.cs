﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CarSelectionAreaController : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    public float distance = 1;

    public float minDragToSwipe = 1.5F;

    public CarSelectionController carSelectionController;

    public CarAvailabilityManager carAvailabilityManager;

    public GameObject leftArrow, rightArrow;

    private CarItem currentSelection;

    public List<CarItem> carItems;

    public List<CarSelectionBackground> backgrounds;

    public CarSelectionSoundsManager soundsManager;

    private bool isDragging;
    private float dragOffsetX;

    public void OnDrag(PointerEventData eventData)
    {
        isDragging = true;

        int carPos = carItems.IndexOf(currentSelection);

        Vector2 newPosition = new Vector2(eventData.position.x + dragOffsetX, currentSelection.transform.position.y);

        if (carPos == 0)
        {
            if (newPosition.x > transform.position.x + 0.1F * Camera.main.pixelWidth)
            {
                return;
            }
        } else
        if (carPos == carItems.Count - 1)
        {
            if (newPosition.x < transform.position.x - 0.1F * Camera.main.pixelWidth)
            {
                return;
            }
        }

        currentSelection.transform.position = newPosition;

        SetCarPositions();

        SetBackgroundPositions();
    }

    private void Update()
    {
        if (isDragging)
        {
            return;
        }

        float targetPosition = Mathf.Lerp(currentSelection.transform.position.x, transform.position.x, Time.deltaTime * 8F);
        currentSelection.transform.position = new Vector2(targetPosition, currentSelection.transform.position.y);

        SetCarPositions();

        SetBackgroundPositions();
    }

    private void SetBackgroundPositions()
    {
        float referenceCarShift = carItems[0].transform.position.x - transform.position.x;
        for (int i = 0; i < backgrounds.Count; i++)
        {
            CarSelectionBackground bckground = backgrounds[i];

            float targetBackgroundX = (bckground.movementMultiplier * referenceCarShift) + transform.position.x + bckground.offset;
            bckground.transform.position = new Vector2(targetBackgroundX, bckground.transform.position.y);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;

        float distance = transform.position.x - currentSelection.transform.position.x;

        if (distance > minDragToSwipe)
        {
            ChangeCarToRight();
        } else
        if (distance < -minDragToSwipe)
        {
            ChangeCarToLeft();
        }
    }


    internal void SetSelection(CarItem carItem)
    {
        currentSelection = carItem;

        carAvailabilityManager.ChangeCar(carItem);

        int selPos = carItems.IndexOf(currentSelection);
        leftArrow.SetActive(selPos != 0);
        rightArrow.SetActive(selPos != carItems.Count - 1);
    }

    private CarItem FindNearestItem(Vector2 position)
    {
        float minDist = float.MaxValue;
        CarItem selected = null;

        foreach (CarItem item in carItems)
        {
            float dist = Vector2.Distance(item.transform.position, transform.position);

            if (dist < minDist)
            {
                minDist = dist;
                selected = item;
            }
        }

        return selected;
    }

    void Start()
    {
        string lastSelectedCarName = PlayerPrefs.GetString("LastSelectedCarName", "Blue");

        foreach (CarItem item in carItems)
        {
            if (item.carName == lastSelectedCarName)
            {
                SetSelection(item);
                break;
            }
        }

        SetCarPositions();

        foreach (CarItem item in carItems)
        {
            item.carSelectionAreaController = this;
        }

        foreach (CarSelectionBackground bckgrnd in backgrounds)
        {
            bckgrnd.SetOffset(transform);
        }
    }

    private void SetCarPositions()
    {
        int curPos = carItems.IndexOf(currentSelection);

        for (int i = 0; i < carItems.Count; i++)
        {
            carItems[i].transform.position = new Vector3(Camera.main.pixelWidth * distance * (i - curPos), 0, 0) + currentSelection.transform.position;
        }
    }

    public void ChangeCarToLeft()
    {
        int selPos = carItems.IndexOf(currentSelection);

        if (selPos > 0)
        {
            selPos--;
        }

        SetSelection(carItems[selPos]);
        SetCarPositions();

        soundsManager.PlaySwooshSound();
    }

    public void ChangeCarToRight()
    {
        int selPos = carItems.IndexOf(currentSelection);

        if (selPos < carItems.Count - 1)
        {
            selPos++;
        }

        SetSelection(carItems[selPos]);
        SetCarPositions();

        soundsManager.PlaySwooshSound();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        dragOffsetX = transform.position.x - eventData.position.x;
    }
}
