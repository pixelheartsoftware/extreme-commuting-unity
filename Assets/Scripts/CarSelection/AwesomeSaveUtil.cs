﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwesomeSaveUtil : Singleton<AwesomeSaveUtil>
{
    public static AwesomeSaveUtil Instance
    {
        get { return ((AwesomeSaveUtil)_Instance); }
        set { _Instance = value; }
    }

    public bool IsCarUnlocked(string carName)
    {
        return 1 == PlayerPrefs.GetInt("CarUnlocked" + carName, 0);
    }

    public void SetCarUnlocked(string carName)
    {
        PlayerPrefs.SetInt("CarUnlocked" + carName, 1);
    }

    public int GetCollectedAwesome()
    {
        return PlayerPrefs.GetInt("CollectedAwesome", 0);
    }

    public void AddCollectedAwesome(int awesome)
    {
        PlayerPrefs.SetInt("CollectedAwesome", GetCollectedAwesome() + awesome);
    }

    public void DecreaseCollectedAwesome(int awesome)
    {
        int collectedAwesome = GetCollectedAwesome();

        if (awesome > collectedAwesome)
        {
            return;
        }
        collectedAwesome -= awesome;

        PlayerPrefs.SetInt("CollectedAwesome", collectedAwesome);
    }

    internal bool AllCarsUnlocked()
    {
        return IsCarUnlocked("Red") && IsCarUnlocked("Blue") && IsCarUnlocked("Yellow");
    }
}
