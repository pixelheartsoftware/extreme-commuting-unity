﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarSelectionButtonsManager : MonoBehaviour
{
    public CarAvailabilityManager availabilityManager;

    public CarSelectionSoundsManager soundsManager;

    public AchievementNotificationController achievementNotificationController;

    private void Start()
    {
        if (AwesomeSaveUtil.Instance.AllCarsUnlocked())
        {
#if !UNITY_ANDROID
                if (AchievementStateManager.Instance.AddAchievementProgress("Collector", 3))
                {
                    achievementNotificationController.FireAchievementUnlocked("Collector");
                }
#else
            AchievementStateManager.Instance.AddAchievementProgress("Collector", 3);
#endif
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetAxis("PadBack") > 0)
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
            return;
        }
    }

    public void OnPlay()
    {
        soundsManager.PlayClickSound();

        if (availabilityManager.carUnlocked)
        {
            PlayerHolder.Instance.selectedCar = availabilityManager.currentCar.GetCarDefinition();

            PlayerPrefs.SetString("LastSelectedCarName", availabilityManager.currentCar.carName);

            SceneManager.LoadScene("Gameplay");
        } else
        {
            availabilityManager.UnlockCurrentCar();
            availabilityManager.ResetSelection();

            soundsManager.PlayChimesSound();
            soundsManager.PlayFirecrackerSound();

            if (AwesomeSaveUtil.Instance.AllCarsUnlocked())
            {
#if !UNITY_ANDROID
                if (AchievementStateManager.Instance.AddAchievementProgress("Collector", 3))
                {
                    achievementNotificationController.FireAchievementUnlocked("Collector");
                }
#else
                AchievementStateManager.Instance.AddAchievementProgress("Collector", 3);
#endif
            } else
            {
#if !UNITY_ANDROID
                if (AchievementStateManager.Instance.AddAchievementProgress("Collector", 1))
                {
                    achievementNotificationController.FireAchievementUnlocked("Collector");
                }
#else
                AchievementStateManager.Instance.AddAchievementProgress("Collector", 1);
#endif
            }
        }
    }

    public void OnBack()
    {
        soundsManager.PlayClickSound();
        SceneManager.LoadScene("MainMenu");
    }
}
