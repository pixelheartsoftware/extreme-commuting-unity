﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CarItem : MonoBehaviour, IDragHandler, IEndDragHandler
{
    internal CarSelectionAreaController carSelectionAreaController;

    public string carName;
    public int carCost;
    public GameObject playerPrefab;

    private CarDefinition carDefinition;

    public void OnDrag(PointerEventData eventData)
    {
        carSelectionAreaController.SetSelection(this);
        carSelectionAreaController.OnDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        carSelectionAreaController.OnEndDrag(eventData);
    }

    internal CarDefinition GetCarDefinition()
    {
        return new CarDefinition(playerPrefab, carName, carCost);
    }
}

public class CarDefinition
{
    public GameObject playerPrefab;
    public string carName;
    public int carCost;

    public CarDefinition(GameObject playerPrefab, string carName, int carCost)
    {
        this.playerPrefab = playerPrefab;
        this.carName = carName;
        this.carCost = carCost;
    }
}