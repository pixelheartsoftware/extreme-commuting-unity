﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSelectionBackground : MonoBehaviour
{
    public float movementMultiplier = 1;

    internal float offset = 0;

    public void SetOffset(Transform reference)
    {
        offset = transform.position.x - reference.position.x;
    }
}
