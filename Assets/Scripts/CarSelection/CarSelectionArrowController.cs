﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CarSelectionArrowController : MonoBehaviour, IPointerClickHandler
{
    public CarSelectionSoundsManager soundsManager;

    public UnityEvent onClickAction;

    public void OnPointerClick(PointerEventData eventData)
    {
        soundsManager.PlayClickSound();
        onClickAction.Invoke();
    }
}
