﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CarAvailabilityManager : MonoBehaviour
{
    public ButtonController playButton;

    public TextMeshProUGUI playLabel;

    public TextMeshProUGUI carCostLabel;
    public TextMeshProUGUI collectedAwesomeLabel;

    internal bool carUnlocked;

    internal CarItem currentCar;

    public bool debugMode;

    public void ChangeCar(CarItem car)
    {
        currentCar = car;

        if (AwesomeSaveUtil.Instance.IsCarUnlocked(car.GetCarDefinition().carName) && !debugMode)
        {
            carCostLabel.enabled = false;
            collectedAwesomeLabel.enabled = false;
            SetReady();
            carUnlocked = true;
            return;
        }

        carUnlocked = false;

        carCostLabel.enabled = true;
        collectedAwesomeLabel.enabled = true;

        int collectedAwesome = AwesomeSaveUtil.Instance.GetCollectedAwesome();
        collectedAwesomeLabel.text = "Collected Awesome: " + collectedAwesome.ToString();

        int carCost = car.GetCarDefinition().carCost;
        carCostLabel.text = "Car Cost: " + carCost.ToString();

        if (carCost <= collectedAwesome || debugMode)
        {
            carCostLabel.color = Color.green;
            SetAvailable();
        } else
        {
            carCostLabel.color = Color.red;
            SetUnavailable();
        }
    }

    internal void UnlockCurrentCar()
    {
        AwesomeSaveUtil.Instance.DecreaseCollectedAwesome(currentCar.GetCarDefinition().carCost);
        AwesomeSaveUtil.Instance.SetCarUnlocked(currentCar.GetCarDefinition().carName);
    }

    internal void ResetSelection()
    {
        ChangeCar(currentCar);
    }

    private void SetUnavailable()
    {
        playButton.interactable = false;
        playLabel.text = "LOCKED";
        playLabel.color = Color.red;
    }

    private void SetAvailable()
    {
        playButton.interactable = true;
        playLabel.text = "BUY";
        playLabel.color = Color.green;
    }

    private void SetReady()
    {
        playButton.interactable = true;
        playLabel.text = "PLAY";
        playLabel.color = Color.yellow;
    }
}
