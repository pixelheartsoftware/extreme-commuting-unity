﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSelectionSoundsManager : SoundsManager
{
    public AudioClip swooshSound;

    public AudioClip chimesSound;

    public AudioClip firecrackerSound;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySwooshSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        audioSource.PlayOneShot(swooshSound);
    }

    public void PlayChimesSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        audioSource.PlayOneShot(chimesSound);
    }

    public void PlayFirecrackerSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        audioSource.PlayOneShot(firecrackerSound);
    }
}
