﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallOfFameSoundsManager : SoundsManager
{
    public AudioClip letterChangeSound;

    public void PlayLetterChangeSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        audioSource.PlayOneShot(letterChangeSound);
    }

}
