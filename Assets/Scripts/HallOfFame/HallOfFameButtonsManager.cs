﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HallOfFameButtonsManager : MonoBehaviour
{
    public SoundsManager soundsManager;

    public void RaceAgainClick()
    {
        soundsManager.PlayClickSound();
        SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
    }

    public void BackClick()
    {
        soundsManager.PlayClickSound();
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
}
