﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using ExtremeCommuting;

public class ScoreSaveController : MonoBehaviour
{
    public TextMeshProUGUI playerNameLabel;
    public TextMeshProUGUI hiscoreLabel;
    public TextMeshProUGUI offensesLabel;

    public HallOfFameSoundsManager soundsManager;

    public Canvas nameEntryCanvas;

    public int maxNameLength = 10;

    public ButtonController playButton;
    public ButtonController backButton;

    private string playerName = "";

    public bool debugMode;

    void Awake()
    {
        AwesomeInfo info = AwesomeInfo.Instance;

#if !UNITY_ANDROID
        int hiscore = PlayerPrefs.GetInt("Hiscore", 0);

        int newPoints = info.GetPoints();

        if (newPoints > hiscore || debugMode)
        {
            playerNameLabel.SetText(PlayerPrefs.GetString("HiscorePlayerName", "PETE1010"));
            hiscoreLabel.text = newPoints.ToString();
            offensesLabel.text = CreateOffensesString(info.GetStuntCounts());

            playButton.interactable = false;
            backButton.interactable = false;

            nameEntryCanvas.enabled = true;

            ControlLoaded();
        }
        else
        {
            playerNameLabel.SetText(PlayerPrefs.GetString("HiscorePlayerName", "PETE1010"));
            hiscoreLabel.text = hiscore.ToString();
            offensesLabel.text = PlayerPrefs.GetString("HiscoreOffenses", "D.U.I.");

            nameEntryCanvas.enabled = false;
        }
#endif
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetAxis("PadBack") > 0)
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
            return;
        }
    }

    private void SaveScore()
    {
        PlayerPrefs.SetString("HiscorePlayerName", playerName);
        PlayerPrefs.SetInt("Hiscore", AwesomeInfo.Instance.GetPoints());
        PlayerPrefs.SetString("HiscoreOffenses", CreateOffensesString(AwesomeInfo.Instance.GetStuntCounts()));
    }

    internal void HandleBackspace()
    {
        if (playerName.Length == 0)
        {
            return;
        }
        playerName = playerName.Substring(0, playerName.Length - 1);

        playerNameLabel.SetText(playerName);
    }

    internal void HandleInput(string text)
    {
        if (playerName.Length == maxNameLength)
        {
            return;
        }
        playerName += text;

        playerNameLabel.SetText(playerName);
    }

    private void ControlLoaded()
    {
        GameObject[] letters = GameObject.FindGameObjectsWithTag("NameEntryLetter");
        
        foreach (GameObject letter in letters)
        {
            TextEntryButtonController button = letter.GetComponent<TextEntryButtonController>();

            button.saveController = this;
        }
    }

    internal void TextEntryFinished()
    {
        if (playerName.Length == 0)
        {
            return;
        }

        SaveScore();

        playButton.interactable = true;
        backButton.interactable = true;

        nameEntryCanvas.enabled = false;
    }

    private string CreateOffensesString(Dictionary<StuntType, int> stuntCounts)
    {
        string offenses = "";
        foreach (KeyValuePair<StuntType, int> entry in stuntCounts)
        {
            if (entry.Value == 0)
            {
                continue;
            }
            string stuntNameFormat = GetStuntNameFormat(entry.Key);

            if (offenses.Length != 0)
            {
                offenses += ", "; 
            }
            offenses += string.Format(stuntNameFormat, entry.Value);
        }

        if (offenses.Length == 0)
        {
            return "IRRESPONSIBLE DRIVING";
        }
        return offenses;
    }

    private string GetStuntNameFormat(StuntType key)
    {
        switch (key)
        {
            case StuntType.ClosePassing:
                {
                    return "{0}x CLOSE PASSING";
                }
            case StuntType.Passing:
                {
                    return "{0}x PASSING";
                }
            case StuntType.NearMiss:
                {
                    return "{0}x NEAR MISS";
                }
            case StuntType.Nudge:
                {
                    return "{0}x NUDGE";
                }
            case StuntType.Push:
                {
                    return "{0}x PUSH";
                }
            case StuntType.Ram:
                {
                    return "{0}x RAM";
                }
            case StuntType.Torpedo:
                {
                    return "{0}x TORPEDO";
                }
            case StuntType.BrakeCheck:
                {
                    return "{0}x BRAKE-CHECK";
                }
            case StuntType.Shield:
                {
                    return "{0}x SHIELD";
                }
            case StuntType.Chicken:
                {
                    return "{0}x CHICKEN";
                }
            case StuntType.Tailgate:
                {
                    return "{0}x TAILGATE";
                }
            case StuntType.Slalom:
                {
                    return "{0}x SLALOM";
                }
            default:
                return "";
        }
    }
}
