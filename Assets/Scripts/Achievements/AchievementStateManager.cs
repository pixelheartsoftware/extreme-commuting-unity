﻿using GooglePlayGames;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class AchievementStateManager : Singleton<AchievementStateManager>
{
    public static AchievementStateManager Instance
    {
        get { return ((AchievementStateManager)_Instance); }
        set { _Instance = value; }
    }

    public AchievementModel GetAchievementByName(String achName)
    {
        List<AchievementModel> achievements = GetAchievements();

        foreach (AchievementModel ach in achievements)
        {
            if (achName == ach.name)
            {
                return ach;
            }
        }

        return null;
    }

    public List<AchievementModel> GetAchievements()
    {
        TextAsset textFile = Resources.Load("achievements") as TextAsset;

        List<AchievementModel> achievements = new List<AchievementModel>(AchievementsConfiguration.CreateFromJson(textFile.text).achievements);

        foreach (AchievementModel achievement in achievements)
        {
            achievement.percent = GetAchievementState(achievement.name);

            achievement.sprite = Resources.Load<Sprite>("Sprites/" + achievement.image) as Sprite;
            achievement.spriteLocked = Resources.Load<Sprite>("Sprites/" + achievement.image + "_locked") as Sprite;
        }

        return achievements;
    }

    private float GetAchievementState(string name)
    {
        return PlayerPrefs.GetInt("AchievementState_" + Regex.Replace(name, @"s", ""), 0);
    }

    private void SetAchievementState(string name, float percent)
    {
        PlayerPrefs.SetInt("AchievementState_" + Regex.Replace(name, @"s", ""), (int)percent);
    }


    public void ClearAchievementProgress(string name)
    {
        AchievementModel ach = GetAchievementByName(name);

        if (ach.IsUnlocked())
        {
            return;
        }

        SetAchievementState(name, 0);
    }

    public bool AddAchievementProgress(string name, int count)
    {
        AchievementModel ach = GetAchievementByName(name);

#if !UNITY_ANDROID
        if (ach.IsUnlocked())
        {
            return false;
        }

        float newPercent = Mathf.Clamp(ach.percent + (count * (100 / ach.counts)), 0, 100);

        if (ach.singleRace && newPercent < 100)
        {
            return false;
        }

        SetAchievementState(name, newPercent);

        if ((newPercent == 100) && GameJoltUtil.IsGameJoltEnabled())
        {
            GameJoltUtil.UlockAchievement(ach.gameJoltId);
        }

        return newPercent == 100;
#else
        if (Social.localUser.authenticated)
        {
            if (ach.counts <= count)
            {
                Social.ReportProgress(ach.googleId, 100F, (bool success) => {});
            } else
            {
                PlayGamesPlatform.Instance.IncrementAchievement(ach.googleId, count, (bool success) => { });
            }

            return false;
        }
        return false;
#endif
    }
}

class AchievementsConfiguration
{
    public AchievementModel[] achievements;

    public static AchievementsConfiguration CreateFromJson(string jsonString)
    {
        return JsonUtility.FromJson<AchievementsConfiguration>(jsonString);
    }
}

[System.Serializable]
public class AchievementModel
{
    public float percent;

    public Sprite sprite;
    public Sprite spriteLocked;

    public string image;
    public string name;
    public string description;
    public string unlockedDescription;
    public float counts;
    public bool singleRace;
    public string googleId;
    public int gameJoltId;

    public bool IsUnlocked()
    {
        return percent == 100;
    }
}