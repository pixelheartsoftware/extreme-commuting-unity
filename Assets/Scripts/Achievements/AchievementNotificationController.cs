﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AchievementNotificationController : MonoBehaviour
{
#if !UNITY_ANDROID
    private AudioSource audioSource;

    private Image image;

    private List<string> notifications = new List<string>();

    private bool sequenceRunning = false;

    private void Start()
    {
        image = GetComponent<Image>();
        audioSource = GetComponent<AudioSource>();

        transform.localScale = Vector2.zero;
    }

    private void Update()
    {
        if (sequenceRunning || notifications.Count == 0)
        {
            return;
        }

        AchievementModel achievement = AchievementStateManager.Instance.GetAchievementByName(notifications[0]);
        notifications.RemoveAt(0);

        if (achievement != null)
        {
            RunSequence(achievement);
        }

    }

    public void FireAchievementUnlocked(string name)
    {
        notifications.Add(name);
    }

    private void RunSequence(AchievementModel achievement)
    {
        StopAllCoroutines();

        image.sprite = achievement.sprite;
        transform.localScale = Vector2.zero;
        transform.rotation = Quaternion.identity;

        StartCoroutine(Sequence());
    }

    IEnumerator Sequence()
    {
        sequenceRunning = true;

        if (!SoundsManager.SoundsDisabled())
        {
            audioSource.Stop();
            audioSource.Play();
        }

        while (transform.localScale.x < 0.99F)
        {
            transform.localScale = PixelheartMath.Lerp2D(transform.localScale, Vector3.one, 0.05F, 1F);
            yield return null;
        }

        int counter = 2500;

        while (counter > 0)
        {
            counter -= 10;
            yield return null;
        }

        transform.rotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        counter = 2*360;
        while (counter > 0)
        {
            transform.Rotate(Vector3.forward, counter, Space.Self);
            counter-=10;
            transform.localScale = PixelheartMath.Lerp2D(transform.localScale, Vector3.zero, 0.05F, 1F);
            yield return null;
        }
        transform.localScale = Vector3.zero;

        sequenceRunning = false;
    }
#else
    
    private void Start()
    {
        transform.localScale = Vector2.zero;
    }

    public void FireAchievementUnlocked(string name)
    {
    }
#endif
}
