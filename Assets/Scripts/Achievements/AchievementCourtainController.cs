﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AchievementCourtainController : MonoBehaviour, IPointerClickHandler
{
    public AchievementListManager achievementListManager;

    public void OnPointerClick(PointerEventData eventData)
    {
        achievementListManager.achievementExpanded.OnClick();
    }
}
