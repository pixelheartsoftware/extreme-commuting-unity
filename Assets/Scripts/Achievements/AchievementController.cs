﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AchievementController : MonoBehaviour
{
    private bool expanded = false;

    internal float progressPercent = 50;

    internal RectTransform iconMarker;

    public RectTransform curtain;
    public RectTransform progressBar;

    internal Transform defaultParent;

    internal string title;
    internal string description;

    public TextMeshProUGUI titleUI;
    public TextMeshProUGUI descriptionUI;

    public float expandSpeed = 0.2f;

    internal Vector3 defaultPosition;

    public Vector3 iconSmallScale = new Vector3(0.5F, 0.5F, 0);

    private static Vector3 ONE = new Vector3(1, 1, 0);

    private Image canvasImage;

    public Image checkImage;

    internal AchievementListManager achievementListManager;

    private ButtonController buttonController;

    private void Start()
    {
        curtain.localScale = Vector2.zero;
        transform.localScale = iconSmallScale;

        progressBar.localScale = new Vector2(progressPercent/100F, 1F);

        canvasImage = curtain.GetComponent<Image>();

        buttonController = GetComponent<ButtonController>();
    }

    IEnumerator Expand()
    {
        canvasImage.color = new Color(canvasImage.color.r, canvasImage.color.g, canvasImage.color.b, 1F);
        titleUI.text = title;
        descriptionUI.text = description;

        while (true)
        {
            bool allExpanded = true;

            if ((curtain.localScale - ONE).magnitude > 0.001F)
            {
                allExpanded = false;
                curtain.localScale = PixelheartMath.Lerp(curtain.localScale, ONE, 0.2F);
            }
            if ((transform.position - iconMarker.position).magnitude > 0.001F)
            {
                allExpanded = false;
                transform.position = PixelheartMath.Lerp(transform.position, iconMarker.position, 0.2F);
            }
            if ((transform.localScale - iconMarker.localScale).magnitude > 0.001F)
            {
                allExpanded = false;
                transform.localScale = PixelheartMath.Lerp(transform.localScale, iconMarker.localScale, 0.2F);
            }

            if (allExpanded)
            {
                yield break;
            }
            yield return null;
        }
        
    }

    IEnumerator Collapse()
    {
        canvasImage.color = new Color(canvasImage.color.r, canvasImage.color.g, canvasImage.color.b, 0F);
        titleUI.text = "";
        descriptionUI.text = "";

        while (true)
        {
            bool allCollapsed = true;

            if ((curtain.localScale - Vector3.zero).magnitude > 0.001F)
            {
                allCollapsed = false;
                curtain.localScale = PixelheartMath.Lerp(curtain.localScale, Vector3.zero, 0.2F);
            }
            if ((transform.position - defaultPosition).magnitude > 0.001F)
            {
                allCollapsed = false;
                transform.position = PixelheartMath.Lerp(transform.position, defaultPosition, 0.2F);
            }
            if ((transform.localScale - iconSmallScale).magnitude > 0.001F)
            {
                allCollapsed = false;
                transform.localScale = PixelheartMath.Lerp(transform.localScale, iconSmallScale, 0.2F);
            }

            if (allCollapsed)
            {
                achievementListManager.achievementExpanded = null;
                yield break;
            }

            yield return null;
        }
    }

    internal void SetProgress(float percent)
    {
        progressPercent = percent;

        checkImage.enabled = percent >= 100;
    }

    public void OnClick()
    {
        if ((achievementListManager.achievementExpanded != null) && (achievementListManager.achievementExpanded != this) && !expanded)
        {
            //Some other achievement is already expanded.
            return;
        }

        expanded = !expanded;

        StopAllCoroutines();
        if (expanded)
        {
            buttonController.blocked = true;
            checkImage.enabled = false;
            achievementListManager.achievementExpanded = this;
            transform.SetParent(curtain, false);
            StartCoroutine(Expand());
        }
        else
        {
            buttonController.blocked = false;
            checkImage.enabled = progressPercent >= 100;
            transform.SetParent(defaultParent, false);
            StartCoroutine(Collapse());
        }
    }
}
