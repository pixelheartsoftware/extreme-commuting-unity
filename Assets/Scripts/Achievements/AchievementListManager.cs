﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AchievementListManager : MonoBehaviour
{
    public RectTransform shelf;

    public Canvas canvas;

    public Vector2 gridSize;

    public GameObject achievementPrefab;

    public TextMeshProUGUI title;
    public TextMeshProUGUI description;

    public RectTransform curtain;
    public RectTransform iconMarker;

    public SelectedButtonBorderController selectedBorderController;

    public ButtonController backButtonController;

    internal AchievementController achievementExpanded;

    void Start()
    {
        RectTransform prefabTransform = achievementPrefab.GetComponent<RectTransform>();

        AchievementController prefabController = achievementPrefab.GetComponent<AchievementController>();

        float achievementPrefabSizeX = Mathf.Abs(prefabTransform.rect.width * prefabController.iconSmallScale.x);
        float achievementPrefabSizeY = Mathf.Abs(prefabTransform.rect.height * prefabController.iconSmallScale.y);

        float horizontalSpace = (shelf.rect.width - gridSize.x * achievementPrefabSizeX) / (gridSize.x - 1);
        float verticalSpace = (shelf.rect.height - gridSize.y * achievementPrefabSizeY) / (gridSize.y - 1);

        iconMarker.localScale = new Vector3(iconMarker.localScale.x, iconMarker.localScale.y, 0F);

        List<AchievementModel> achievements = AchievementStateManager.Instance.GetAchievements();

        ButtonController[,] achArray = new ButtonController[(int)gridSize.y, (int)gridSize.x];

        for (int row = 0; row < gridSize.y; row++)
        {
            for (int col = 0; col < gridSize.x; col++)
            {
                AchievementModel achievementModel = achievements[0];

                GameObject ach = CreateAchievement(achievementPrefabSizeX, achievementPrefabSizeY, horizontalSpace, verticalSpace, row, col, achievementModel);

                achievements.Remove(achievementModel);

                achArray[row, col] = ach.GetComponent<ButtonController>();

                if (achievements.Count == 0)
                {
                    break;
                }
            }

            if (achievements.Count == 0)
            {
                break;
            }
        }

        for (int i = 0; i < (int)gridSize.x; i++)
        {
            for (int j = 0; j < (int)gridSize.y; j++)
            {
                if (i > 0)
                {
                    achArray[i, j].up = achArray[i-1, j].gameObject;
                }
                if (i < (int)gridSize.x - 1)
                {
                    achArray[i, j].down = achArray[i + 1, j].gameObject;
                }
                if (j > 0)
                {
                    achArray[i, j].left = achArray[i, j - 1].gameObject;
                }
                if (j < (int)gridSize.y - 1)
                {
                    achArray[i, j].right = achArray[i, j + 1].gameObject;
                }

                if (j == 0)
                {
                    achArray[i, j].left = backButtonController.gameObject;
                    backButtonController.right = achArray[i, j].gameObject;
                }

                achArray[i, j].selectController = selectedBorderController;
            }
        }
        achArray[0, 0].gameObject.transform.localScale = achArray[0, 0].GetComponent<AchievementController>().iconSmallScale;
        selectedBorderController.SelectButton(achArray[0, 0].gameObject, SelectionMethod.NULL);
    }

    private GameObject CreateAchievement(float achievementPrefabSizeX, float achievementPrefabSizeY, float horizontalSpace, float verticalSpace, int row, int col, AchievementModel achievementModel)
    {
        GameObject inst = Instantiate(achievementPrefab, shelf);

        RectTransform trans = inst.GetComponent<RectTransform>();

        trans.localPosition = new Vector2((col * (horizontalSpace + achievementPrefabSizeX)), -(row * (verticalSpace + achievementPrefabSizeY)));

        AchievementController con = inst.GetComponent<AchievementController>();

        con.title = achievementModel.name;
        con.description = achievementModel.description;
        con.titleUI = title;
        con.descriptionUI = description;
        con.curtain = curtain;
        con.iconMarker = iconMarker;
        con.defaultPosition = inst.transform.position;
        con.defaultParent = canvas.transform;
        con.SetProgress(achievementModel.percent);

        LoadSprite(achievementModel, con);

        con.achievementListManager = this;

        return inst;
    }

    private void LoadSprite(AchievementModel model, AchievementController controller)
    {
        if (model.sprite == null)
        {
            return;
        }

        Image achImage = controller.GetComponent<Image>();
        if (model.IsUnlocked())
        {
            achImage.sprite = model.sprite;
        } else
        {
            achImage.sprite = model.spriteLocked;
        }  
    }

    public void BackButtonPress()
    {
        SceneManager.LoadScene("MainMenu");
    }
}