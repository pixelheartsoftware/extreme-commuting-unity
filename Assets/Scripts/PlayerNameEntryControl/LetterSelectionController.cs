﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterSelectionController : MonoBehaviour
{
    public Image leftBracket;
    public Image rightBracket;

    public void SetSelection(TextEntryButtonController selected, float width)
    {
        Vector3 halfWidth = new Vector3(Camera.main.pixelWidth * 0.5F * width, 0, 0);

        leftBracket.transform.position = selected.transform.position - halfWidth;
        rightBracket.transform.position = selected.transform.position + halfWidth;
    }
}
