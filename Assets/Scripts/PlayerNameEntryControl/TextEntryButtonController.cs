﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class TextEntryButtonController : MonoBehaviour, ISelectHandler, IPointerEnterHandler
{
    public bool defaultSelected = false;

    public float width = 0;

    public bool isBackspace = false;
    public bool isOkButton = false;

    LetterSelectionController controller;

    public ScoreSaveController saveController;

    internal TextMeshProUGUI myLabel;

    private void Start()
    {
        controller = GameObject.FindGameObjectWithTag("LetterSelectionController").GetComponent<LetterSelectionController>();

        myLabel = gameObject.GetComponentInChildren<TextMeshProUGUI>();

        if (defaultSelected)
        {
            EventSystem.current.SetSelectedGameObject(gameObject);
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        saveController.soundsManager.PlayLetterChangeSound();
        controller.SetSelection(this, width);
    }

    public void OnClick()
    {
        saveController.soundsManager.PlayClickSound();
        if (isOkButton)
        {
            saveController.TextEntryFinished();
            return;
        }
        if (isBackspace)
        {
            saveController.HandleBackspace();
            return;
        }
        saveController.HandleInput(myLabel.text);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
    }
}
