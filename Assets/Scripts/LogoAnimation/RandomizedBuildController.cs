﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizedBuildController : MonoBehaviour
{
    Vector2 originalPosition;

    public float minTranslation = 10, maxTranslation = 100;

    public float speed = 40F;

    void Start()
    {
        originalPosition = transform.position;

        int multi = UnityEngine.Random.Range(0, 2);

        multi = multi == 0 ? -1 : 1;

        if (UnityEngine.Random.Range(0, 100) > 50F)
        {
            transform.position = new Vector2(transform.position.x, UnityEngine.Random.Range(minTranslation, maxTranslation) * multi);
        } else
        {
            transform.position = new Vector2(UnityEngine.Random.Range(minTranslation, maxTranslation) * multi, transform.position.y);
        }
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector2.Distance(originalPosition, transform.position);
        if (distance > 1)
        {
            float newX = transform.position.x;
            float newY = transform.position.y;

            if (transform.position.x > originalPosition.x)
            {
                newX = transform.position.x - speed * Time.deltaTime;
            }
            else if (transform.position.x < originalPosition.x)
            {
                newX = transform.position.x + speed * Time.deltaTime;
            }

            if (transform.position.y > originalPosition.y)
            {
                newY = transform.position.y - speed * Time.deltaTime;
            }
            else if (transform.position.y < originalPosition.y)
            {
                newY = transform.position.y + speed * Time.deltaTime;
            }

            transform.position = new Vector2(newX, newY);
        } else if (distance > 0.00001F)
        {
            float newX = Mathf.Lerp(transform.position.x, originalPosition.x, 0.5F);
            float newY = Mathf.Lerp(transform.position.y, originalPosition.y, 0.5F);

            transform.position = new Vector2(newX, newY);
        } else
        {
            gameObject.AddComponent<BoxCollider2D>();
            Destroy(this);
        }
    }
}
