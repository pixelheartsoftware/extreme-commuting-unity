﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioClip engine;
    public AudioClip brakes;

    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        if (collision.CompareTag("StartEngine"))
        {
            audioSource.PlayOneShot(engine, 5);

        } else
        if (collision.CompareTag("StartBrakes"))
        {
            audioSource.PlayOneShot(brakes, 20);
        }
    }
}
