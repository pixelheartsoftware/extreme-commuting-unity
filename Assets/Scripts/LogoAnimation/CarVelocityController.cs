﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarVelocityController : MonoBehaviour
{
    public Vector2 myVelocity = new Vector2(-1, 0);

    public bool disabled = false;

    void Start()
    {
        Cursor.visible = false;

        if (!disabled) {
            GetComponent<Rigidbody2D>().velocity = myVelocity;
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Physics2D.gravity = new Vector2(0, -9F);

        collision.gameObject.GetComponent<Rigidbody2D>().AddForce(collision.relativeVelocity * -100, ForceMode2D.Force);
    }
}
