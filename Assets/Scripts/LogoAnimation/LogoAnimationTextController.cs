﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LogoAnimationTextController : MonoBehaviour
{
    public float waitTimeBeforeShow = 3;

    private TextMeshProUGUI gui;

    void Start()
    {
        gui = GetComponent<TextMeshProUGUI>();
        gui.enabled = false;
    }

    void Update()
    {
        if (waitTimeBeforeShow > 0)
        {
            waitTimeBeforeShow -= Time.deltaTime;
            return;
        }

        gui.enabled = true;
    }
}
