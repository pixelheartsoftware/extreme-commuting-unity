﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuLoader : MonoBehaviour
{
    public List<Renderer> renderers;

    private AsyncOperation async;

    private float delay = 3;

    void Start()
    {
        StartCoroutine(LoadNewScene());
    }

    void Update()
    {
        if (async == null)
        {
            return;
        }

        if (Input.anyKeyDown)
        {
            Cursor.visible = true;
            async.allowSceneActivation = true;
        }

        if (delay > 0)
        {
            delay -= Time.deltaTime;
            return;
        }

        foreach (Renderer ren in renderers)
        {
            if (ren.isVisible)
            {
                return;
            }
        }

        Cursor.visible = true;
        async.allowSceneActivation = true;
    }

    IEnumerator LoadNewScene()
    {
        async = SceneManager.LoadSceneAsync("MainMenu");

        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            yield return null;
        }
    }
}
