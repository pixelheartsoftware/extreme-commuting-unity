﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StuntMessageManager : MonoBehaviour
{
    public Camera referenceCamera;

    public float vanishingOffset = 200F;

    public float creationOffsetY = 80;

    private float separator = 40;

    public GameObject stuntMessagePrefab;

    public Vector2 velocity = new Vector2(0, 0.1F);

    public Canvas canvas;

    public bool debug;

    private List<TextMeshProUGUI> messages = new List<TextMeshProUGUI>();

    private Vector2 basePosition;

    private float baseFontSize;

    void Start()
    {
        Dictionary<StuntType, string> mapping = new Dictionary<StuntType, string>();

        mapping.Add(StuntType.BrakeCheck, "{0}x BRAKE CHECK");
        mapping.Add(StuntType.Chicken, "{0}x CHICKEN");
        mapping.Add(StuntType.ClosePassing, "{0}x CLOSE PASSING");
        mapping.Add(StuntType.Passing, "{0}x PASSING");
        mapping.Add(StuntType.NearMiss, "{0}x NEAR MISS");
        mapping.Add(StuntType.Nudge, "{0}x NUDGE");
        mapping.Add(StuntType.Push, "{0}x PUSH");
        mapping.Add(StuntType.Ram, "{0}x RAM");
        mapping.Add(StuntType.Shield, "{0}x SHIELD");
        mapping.Add(StuntType.Torpedo, "{0}x TORPEDO!");
        mapping.Add(StuntType.Tailgate, "{0}x TAILGATE");
        mapping.Add(StuntType.Slalom, "{0}x SLALOM");

        basePosition = transform.position;

        Dictionary<StuntType, int> stuntCounts = AwesomeInfo.Instance.GetStuntCounts();

        if (debug)
        {
            stuntCounts.Clear();
            stuntCounts.Add(StuntType.BrakeCheck, 12);
            stuntCounts.Add(StuntType.Chicken, 12);
            stuntCounts.Add(StuntType.ClosePassing, 12);
            stuntCounts.Add(StuntType.NearMiss, 12);
            stuntCounts.Add(StuntType.Nudge, 12);
            stuntCounts.Add(StuntType.Push, 12);
            stuntCounts.Add(StuntType.Ram, 12);
            stuntCounts.Add(StuntType.Shield, 12);
            stuntCounts.Add(StuntType.Torpedo, 12);
            stuntCounts.Add(StuntType.Tailgate, 3);
            stuntCounts.Add(StuntType.Slalom, 5);
        }

        float separation = 0;

        separation = CreateMessage("Longest Combo: {0}", AwesomeInfo.Instance.GetLongestCombo(), separation);

        foreach (StuntType stuntType in (StuntType[])System.Enum.GetValues(typeof(StuntType)))
        {
            if (!stuntCounts.ContainsKey(stuntType) || stuntCounts[stuntType] == 0)
            {
                continue;
            }

            separation = CreateMessage(mapping[stuntType], stuntCounts[stuntType], separation);
        }

        baseFontSize = stuntMessagePrefab.GetComponent<TextMeshProUGUI>().fontSize;
    }

    private float CreateMessage(string textFormat, int stuntCount, float separation)
    {
        GameObject message = Instantiate(stuntMessagePrefab, basePosition - new Vector2(0, creationOffsetY - separation), Quaternion.identity, canvas.transform);

        TextMeshProUGUI textMesh = message.GetComponent<TextMeshProUGUI>();

        separator = textMesh.preferredHeight * 3;

        textMesh.SetText(string.Format(textFormat, stuntCount));

        messages.Add(textMesh);

        separation -= separator;
        return separation;
    }

    private void Update()
    {
        if (messages.Count <= 0)
        {
            return;
        }
        TextMeshProUGUI last = messages[messages.Count - 1];
       
        if (last.transform.position.y > basePosition.y + vanishingOffset)
        {
            float separation = 0;
            foreach (TextMeshProUGUI textMesh in messages)
            {
                // the top instance vanished, wraping
                textMesh.transform.position = basePosition - new Vector2(0, creationOffsetY - separation);
                separation -= separator;
            }
        }

        foreach (TextMeshProUGUI textMesh in messages)
        {
            float distance = Mathf.Abs(basePosition.y - textMesh.transform.position.y);
            float fraction = Mathf.Clamp(distance / vanishingOffset, 0, 1);
            float newAlpha = 1 - fraction;

            textMesh.alpha = newAlpha;
            textMesh.fontSize = baseFontSize - (distance / vanishingOffset)*2;
            textMesh.transform.position += (Vector3)velocity * Time.deltaTime;
        }
    }
}
