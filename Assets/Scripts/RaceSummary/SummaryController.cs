﻿using ExtremeCommuting;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SummaryController : MonoBehaviour
{
    public string totalAwesomeFormat = "Awesome: {0} x {1}% = {2}";
    public string totalAwesomeFormatComplete = "Awesome: {0}";

    public string raceCompletionFormat = "{0} Race Completed";
    public string raceCompletion100PercentFormat = "100% Race Completed!!!";

    public TextMeshProUGUI totalAwesome;

    public TextMeshProUGUI raceCompletion;

    public AchievementNotificationController achievementNotificationController;

    void Awake()
    {
        Time.timeScale = 1;

        AwesomeInfo info = AwesomeInfo.Instance;

        int points = info.GetPoints();
        int percent = info.GetRacePercentage();

        int totalResult = info.GetCalculatedPoints();

        totalAwesome.SetText(string.Format(totalAwesomeFormat, points, percent, totalResult));

        AwesomeSaveUtil.Instance.AddCollectedAwesome(totalResult);

        string text = string.Format(raceCompletionFormat, percent);
        if (percent == 100)
        {
            text = raceCompletion100PercentFormat;
            totalAwesome.SetText(string.Format(totalAwesomeFormatComplete, totalResult));
#if UNITY_ANDROID
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(info.GetPoints(), GPGSIds.leaderboard_awesome_leaders, (bool success) => { });

            switch (info.CarName)
            {
                case "Red":
                    Social.ReportScore(info.GetPoints(), GPGSIds.leaderboard_red_leaders, (bool success) => { });
                    break;
                case "Blue":
                    Social.ReportScore(info.GetPoints(), GPGSIds.leaderboard_blue_leaders, (bool success) => { });
                    break;
                case "Yellow":
                    Social.ReportScore(info.GetPoints(), GPGSIds.leaderboard_yellow_leaders, (bool success) => { });
                    break;
            }
        }
#endif
            if (GameJoltUtil.IsGameJoltEnabled())
            {
                GameJoltUtil.PublishLeaderboards(info.CarName, totalResult);
            }
        }
        else
        if(percent < 50) {
            raceCompletion.color = Color.red;
        }
        else
        {
            raceCompletion.color = Color.yellow;
        }

        raceCompletion.SetText(text);

        AddAchievements(info);
    }

    private void AddAchievements(AwesomeInfo info)
    {

        // Stunt counts
        Dictionary<StuntType, int> stuntCounts = info.GetStuntCounts();

        if (AchievementStateManager.Instance.AddAchievementProgress("U-Boot", stuntCounts[StuntType.Torpedo]))
        {
            achievementNotificationController.FireAchievementUnlocked("U-Boot");
        }
        if (AchievementStateManager.Instance.AddAchievementProgress("Troll", stuntCounts[StuntType.BrakeCheck]))
        {
            achievementNotificationController.FireAchievementUnlocked("Troll");
        }
        if (AchievementStateManager.Instance.AddAchievementProgress("Driver Without A Cause", stuntCounts[StuntType.Chicken]))
        {
            achievementNotificationController.FireAchievementUnlocked("Driver Without A Cause");
        }

        // Combo lengths:
        if (AchievementStateManager.Instance.AddAchievementProgress("Chain Reaction", info.GetLongestCombo()))
        {
            achievementNotificationController.FireAchievementUnlocked("Chain Reaction");
        }
        if (AchievementStateManager.Instance.AddAchievementProgress("Tsar Combo", info.GetLongestCombo()))
        {
            achievementNotificationController.FireAchievementUnlocked("Tsar Combo");
        }


        // Abc:
        if (info.GetRacePercentage() == 100)
        {
            if (AchievementStateManager.Instance.AddAchievementProgress("A.B.C.", 1))
            {
                achievementNotificationController.FireAchievementUnlocked("A.B.C.");
            }
        } else
        {
            AchievementStateManager.Instance.ClearAchievementProgress("A.B.C.");
        }

        //Get Rich Quick Scheme
        if (info.GetPoints() >= 1000000)
        {
            if (AchievementStateManager.Instance.AddAchievementProgress("Get Rich Quick Scheme", 1))
            {
                achievementNotificationController.FireAchievementUnlocked("Get Rich Quick Scheme");
            }
        }

        // Thank You:
        if (info.GetRacePercentage() == 100)
        {
            if (AchievementStateManager.Instance.AddAchievementProgress("Thank You", 1))
            {
                achievementNotificationController.FireAchievementUnlocked("Thank You");
            }

            // Gentleman:
            if (info.GetCrashedCars() == 0)
            {
                if (AchievementStateManager.Instance.AddAchievementProgress("Gentleman", 1))
                {
                    achievementNotificationController.FireAchievementUnlocked("Gentleman");
                }
            }

            // Didn't know it could accelerate...
            if (!info.WasAccelerationUsed())
            {
                if (AchievementStateManager.Instance.AddAchievementProgress("Didn't Know It Could Accelerate...", 1))
                {
                    achievementNotificationController.FireAchievementUnlocked("Didn't Know It Could Accelerate...");
                }
            }

            // Impossible
            if (info.ImpossibleAchieved())
            {
                if (AchievementStateManager.Instance.AddAchievementProgress("Impossible", 1))
                {
                    achievementNotificationController.FireAchievementUnlocked("Impossible");
                }
            }
        } else
        {
            if (AchievementStateManager.Instance.AddAchievementProgress("Good Effort", 1))
            {
                achievementNotificationController.FireAchievementUnlocked("Good Effort");
            }
        }

        // Fury Road:
        if (AchievementStateManager.Instance.AddAchievementProgress("Fury Road", info.GetCrashedCars()))
        {
            achievementNotificationController.FireAchievementUnlocked("Fury Road");
        }

        // Blues, Brother!:
        if (AchievementStateManager.Instance.AddAchievementProgress("Blues, Brother!", info.GetCrashedCars()))
        {
            achievementNotificationController.FireAchievementUnlocked("Blues, Brother!");
        }

        // Risk Addict
        if (info.GetLeftLaneTotalTime() >= 30)
        {
            if (AchievementStateManager.Instance.AddAchievementProgress("Risk Addict", 1))
            {
                achievementNotificationController.FireAchievementUnlocked("Risk Addict");
            }
        }
    }
}
