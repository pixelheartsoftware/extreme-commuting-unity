﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SummaryButtonsManager : MonoBehaviour
{
    public SoundsManager soundsManager;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetAxis("PadBack") > 0)
        {
            SceneManager.LoadScene("CarSelection", LoadSceneMode.Single);
            return;
        }
    }

    public void RaceAgainClick()
    {
        soundsManager.PlayClickSound();
        if (HandleHighScore())
        {
            return;
        }
        SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
    }

    public void BackClick()
    {
        soundsManager.PlayClickSound();
        if (HandleHighScore())
        {
            return;
        }
        SceneManager.LoadScene("CarSelection", LoadSceneMode.Single);
    }

    private bool HandleHighScore()
    {
#if !UNITY_ANDROID
        if (GameJoltUtil.IsGameJoltEnabled())
        {
            return false;
        }

        int hiscore = PlayerPrefs.GetInt("Hiscore", 0);

        int newScore = AwesomeInfo.Instance.GetCalculatedPoints();

        if (newScore > hiscore)
        {
            SceneManager.LoadScene("HallOfFame", LoadSceneMode.Single);
            return true;
        }
#endif
        return false;
    }
}
