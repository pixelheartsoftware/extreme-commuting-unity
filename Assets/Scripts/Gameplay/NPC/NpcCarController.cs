﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcCarController : MonoBehaviour, StuntSubject
{
    public ParticleSystem smokeEffect;

    public List<ParticleSystem> explosionEffects;

    public float dragAfterCrash;

    internal Rigidbody2D myRigidbody;

    internal CarGenerator carGenerator;

    internal bool wasHit;
    internal bool wasDestroyed;

    public List<Sprite> sprites = new List<Sprite>();

    //Stunts:
    internal bool rammed = false;
    internal int pileupCounter = 0;

    internal float linearDrag = 0;

    internal NpcSoundsManager soundsManager;

    private SpriteRenderer spriteRenderer;
    private PolygonCollider2D myCollider;

    public List<GameObject> carPartPrefabs;

    private HashSet<StuntType> stunts = new HashSet<StuntType>();

    public float wreckResistance = 1F;

    void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        soundsManager = GetComponent<NpcSoundsManager>();

        spriteRenderer = GetComponent<SpriteRenderer>();

        myCollider = GetComponent<PolygonCollider2D>();

        linearDrag = myRigidbody.drag;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collGameObject = collision.gameObject;

        bool isPolice = collGameObject.CompareTag("Police") || collGameObject.CompareTag("PoliceBack");
        if (collGameObject.CompareTag("ForwardCar") || collGameObject.CompareTag("BackwardCar") || isPolice)
        {
            if (wasHit)
            {
                if (!wasDestroyed) {
                    Explode();
                }
                return;
            }
            
            AwesomeInfo.Instance.AddCrashedCar();

            myRigidbody.drag = linearDrag;

            smokeEffect.Play();

            soundsManager.StopHonk();
            soundsManager.PlayCrash();

            if (isPolice)
            {
                PoliceController collCarController = collGameObject.GetComponent<PoliceController>();

                if (collCarController.rammed)
                {
                    float distToPlayerY = Mathf.Abs(collGameObject.transform.position.y - PlayerHolder.Instance.GetPlayerTransform().position.y);
                    if (distToPlayerY > 1.5F)
                    {
                        StuntManager.Instance.HandleStunt(StuntType.Torpedo, PixelheartMath.Lerp(transform.position, collGameObject.transform.position, 0.5F), this, true);
                    }
                    else if (distToPlayerY < 0.8F)
                    {
                        float distToPlayerX = Mathf.Abs(collGameObject.transform.position.x - PlayerHolder.Instance.GetPlayerTransform().position.x);
                        if (distToPlayerX < 0.3F)
                        {
                            StuntManager.Instance.HandleStunt(StuntType.Shield, PixelheartMath.Lerp(transform.position, collGameObject.transform.position, 0.5F), this, true);
                        }
                    }
                    collCarController.rammed = false;
                }

                if (collCarController.wasHit || wasHit)
                {
                    Explode();
                }
            } else
            {
                NpcCarController collCarController = collGameObject.GetComponent<NpcCarController>();

                if (collCarController.rammed)
                {
                    float distToPlayerY = Mathf.Abs(collGameObject.transform.position.y - PlayerHolder.Instance.GetPlayerTransform().position.y);
                    if (distToPlayerY > 1.5F)
                    {
                        StuntManager.Instance.HandleStunt(StuntType.Torpedo, PixelheartMath.Lerp(transform.position, collGameObject.transform.position, 0.5F), this, false);
                    }
                    else if (distToPlayerY < 0.8F)
                    {
                        float distToPlayerX = Mathf.Abs(collGameObject.transform.position.x - PlayerHolder.Instance.GetPlayerTransform().position.x);
                        if (distToPlayerX < 0.3F)
                        {
                            StuntManager.Instance.HandleStunt(StuntType.Shield, PixelheartMath.Lerp(transform.position, collGameObject.transform.position, 0.5F), this, false);
                        }
                    }
                    collCarController.rammed = false;
                }

                if (collCarController.wasHit || wasHit)
                {
                    Explode();
                }
            }

            wasHit = true;
        }
    }

    internal void Init(float speed)
    {
        Sprite sprite = RandomUtil.RandomElement(sprites);

        spriteRenderer.enabled = true;
        spriteRenderer.sprite = sprite;

        wasHit = false;
        wasDestroyed = false;

        smokeEffect.Stop();

        myRigidbody.drag = 0;

        myCollider.enabled = true;

        myRigidbody.rotation = 0;

        myRigidbody.bodyType = RigidbodyType2D.Dynamic;
        myRigidbody.velocity = new Vector2(0F, speed);

        wreckResistance = 1;

        stunts.Clear();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BackgroundParallax") 
            || collision.CompareTag("HouseLeft") 
            || collision.CompareTag("HouseRight"))
        {
            Explode();
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Police")
            || collision.gameObject.CompareTag("PoliceBack"))
        {
            wreckResistance -= Time.deltaTime;

            if (wreckResistance <= 0)
            {
                Explode();
            }
        }
    }

    public void Explode()
    {
        foreach (ParticleSystem explosionEffect in explosionEffects)
        {
            explosionEffect.Play();
        }
        
        smokeEffect.Stop();
        soundsManager.PlayExplosion();
        myRigidbody.velocity = Vector2.zero;
        wasDestroyed = true;
        spriteRenderer.enabled = false;
        myCollider.enabled = false;

        GenerateCarParts();
    }

    private void GenerateCarParts()
    {
        foreach (GameObject carPartPrefab in carPartPrefabs)
        {
            if (RandomUtil.Chance(50))
            {
                GameObject inst = InstancePoolManager.Instance.CreateInstance(carPartPrefab);

                inst.transform.position = transform.position;

                Rigidbody2D rb = inst.GetComponent<Rigidbody2D>();

                rb.AddForce(new Vector2(UnityEngine.Random.Range(-400, 400), UnityEngine.Random.Range(-400, 400)));
            }
        }
    }

    public bool CanAddStunt(StuntType type)
    {
        if ((type == StuntType.ClosePassing) || (type == StuntType.Passing))
        {
            return !stunts.Contains(StuntType.ClosePassing) && !stunts.Contains(StuntType.Passing);
        }

        return !stunts.Contains(type);
    }

    public void AddStunt(StuntType type)
    {
        stunts.Add(type);
    }

    public Transform GetTransform()
    {
        return transform;
    }
}


public interface StuntSubject
{
    bool CanAddStunt(StuntType type);
    void AddStunt(StuntType type);
    Transform GetTransform();
}