﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceController : MonoBehaviour, StuntSubject
{
    public ParticleSystem smokeEffect;
    public ParticleSystem explosionEffect;

    internal Rigidbody2D myRigidbody;

    internal bool wasHit;
    internal bool wasDestroyed;

    public bool isForwardPolice;

    internal bool rammed = false;

    internal float linearDrag = 0;

    public float dragAfterCrash;

    private Vector2 baseVelocity;

    private Vector2 basePosition;

    private Quaternion baseRotation;

    internal NpcSoundsManager soundsManager;

    public AudioSource sirenSource;

    private SpriteRenderer spriteRenderer;

    private PolygonCollider2D myCollider;

    public List<GameObject> carPartPrefabs;

    private HashSet<StuntType> stunts = new HashSet<StuntType>();

    void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        soundsManager = GetComponent<NpcSoundsManager>();

        spriteRenderer = GetComponent<SpriteRenderer>();

        myCollider = GetComponent<PolygonCollider2D>();

        linearDrag = myRigidbody.drag;
    }

    public void Init()
    {
        baseVelocity = myRigidbody.velocity;
        basePosition = transform.position;
        baseRotation = transform.rotation;

       
        if (!SoundsManager.SoundsDisabled())
        {
            sirenSource.Play();
        }

        spriteRenderer.enabled = true;
        myCollider.enabled = true;

        myRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;

        stunts.Clear();
    }

    private void FixedUpdate()
    {
        if (!wasHit)
        {
            myRigidbody.velocity = baseVelocity;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collGameObject = collision.gameObject;
        
        if (collGameObject.CompareTag("Police") || collGameObject.CompareTag("PoliceBack"))
        {
            if (wasHit)
            {
                if (!wasDestroyed)
                {
                    soundsManager.PlayExplosion();
                    explosionEffect.Play();
                    smokeEffect.Stop();
                    wasDestroyed = true;
                    spriteRenderer.enabled = false;
                    myCollider.enabled = false;
                }
                return;
            }
            wasHit = true;

            myRigidbody.constraints = RigidbodyConstraints2D.None;

            sirenSource.Stop();

            smokeEffect.Play();
        }
        else if (collGameObject.CompareTag("BackwardCar") || collGameObject.CompareTag("ForwardCar"))
        {
            if (wasHit)
            {
                if (!wasDestroyed)
                {
                    Explode();
                }
                return;
            }


            NpcCarController carController = collGameObject.GetComponent<NpcCarController>();
            if (carController.rammed)
            {
                wasHit = true;
                Explode();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BackgroundParallax")
            || collision.CompareTag("Police")
            || collision.CompareTag("PoliceBack")
            || collision.CompareTag("HouseLeft")
            || collision.CompareTag("HouseRight"))
        {
            Explode();
        }
    }

    public void Explode()
    {
        explosionEffect.Play();
        smokeEffect.Stop();
        soundsManager.PlayExplosion();
        myRigidbody.velocity = Vector2.zero;
        wasDestroyed = true;
        spriteRenderer.enabled = false;
        myCollider.enabled = false;

        GenerateCarParts();
    }

    private void GenerateCarParts()
    {
        foreach (GameObject carPartPrefab in carPartPrefabs)
        {
            if (RandomUtil.Chance(50))
            {
                GameObject inst = InstancePoolManager.Instance.CreateInstance(carPartPrefab);

                inst.transform.position = transform.position;

                Rigidbody2D rb = inst.GetComponent<Rigidbody2D>();

                rb.AddForce(new Vector2(UnityEngine.Random.Range(-400, 400), UnityEngine.Random.Range(-400, 400)));
                rb.AddTorque(UnityEngine.Random.Range(-200, 200));
            }
        }
    }

    public bool CanAddStunt(StuntType type)
    {
        if ((type == StuntType.ClosePassing) || (type == StuntType.Passing))
        {
            return !stunts.Contains(StuntType.ClosePassing) && !stunts.Contains(StuntType.Passing);
        }

        return !stunts.Contains(type);
    }

    public void AddStunt(StuntType type)
    {
        stunts.Add(type);
    }

    public Transform GetTransform()
    {
        return transform;
    }
}
