﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncommingCarAlertController : MonoBehaviour
{
    public Transform relative;

    private SpriteRenderer spriteRenderer;

    private float yDistToPlayer;

    public List<NpcCarController> collidingObjects = new List<NpcCarController>();

    private void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        yDistToPlayer = transform.position.y - PlayerHolder.Instance.GetPlayerTransform().position.y;

        spriteRenderer.enabled = false;

        gameObject.SetActive(true);
    }

    private void Update()
    {
        transform.position = new Vector2(transform.position.x, PlayerHolder.Instance.GetPlayerTransform().position.y + yDistToPlayer);

        collidingObjects.RemoveAll(c => c.wasHit);

        spriteRenderer.enabled = collidingObjects.Count > 0;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BackwardCar"))
        {
            NpcCarController controller = collision.GetComponent<NpcCarController>();

            if (!controller.wasHit)
            {
                collidingObjects.Add(controller);
            }
        }

        spriteRenderer.enabled = collidingObjects.Count > 0;
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("BackwardCar"))
        {
            NpcCarController controller = collision.GetComponent<NpcCarController>();
            collidingObjects.Remove(controller);
        }

        spriteRenderer.enabled = collidingObjects.Count > 0;
    }
}
