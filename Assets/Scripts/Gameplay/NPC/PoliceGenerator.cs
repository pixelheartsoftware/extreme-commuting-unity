﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceGenerator : MonoBehaviour
{
    public GameObject policePrefab;

    public int chance = 3;

    public float policeSpeed = 0.8F;

    public float removeDistance = 9;

    public PoliceIndicatorController indicator;

    private PoliceController policeInstance = null;

    private float playerVerticalOffset;

    private float interval = 6;

    private void Start()
    {
        playerVerticalOffset = PlayerHolder.Instance.GetPlayerTransform().position.y - transform.position.y;
    }

    void Update()
    {
        if (RaceManager.Instance.IsRaceComplete())
        {
            indicator.active = false;
            if (policeInstance != null)
            {
                policeInstance.Explode();
                RemoveCar(policeInstance);
                policeInstance = null;
            }
            return;
        }

        indicator.active = false;

        if (policeInstance == null)
        {
            if (interval > 0)
            {
                interval -= Time.deltaTime;
                return;
            }

            interval = 10;

            int policeChance = chance;

            float points = AwesomeInfo.Instance.GetPoints();
            if (points >= 1000)
            {
                policeChance += (int)Mathf.Pow(Mathf.Log10(points/100F), 2) * 4;
            }

            if (RandomUtil.Chance(policeChance)) {
                GameObject instance = InstancePoolManager.Instance.CreateInstance(policePrefab);

                instance.transform.position = new Vector2(transform.position.x, PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset);

                instance.transform.rotation = Quaternion.Euler(Vector3.zero);

                Rigidbody2D rigidBody = instance.GetComponent<Rigidbody2D>();
                rigidBody.bodyType = RigidbodyType2D.Dynamic;
                rigidBody.velocity = new Vector2(0F, policeSpeed);

                policeInstance = instance.GetComponent<PoliceController>();

                policeInstance.Init();
                indicator.active = true;
            }
        } else
        {
            indicator.active = true;

            if ((!policeInstance.isForwardPolice && (policeInstance.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y)) || policeInstance.isForwardPolice)
            {
                if ((!policeInstance.isForwardPolice && (policeInstance.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y)))
                {
                    indicator.active = false;
                }

                if ((policeInstance.wasDestroyed && !policeInstance.explosionEffect.IsAlive()) || Vector2.Distance(policeInstance.transform.position, PlayerHolder.Instance.GetPlayerTransform().position) > removeDistance)
                {
                    RemoveCar(policeInstance);
                    indicator.active = false;
                }
            }
        }

        if (policeInstance == null || policeInstance.wasHit || policeInstance.wasDestroyed)
        {
            indicator.active = false;
        }
    }

    private void RemoveCar(PoliceController inst)
    {
        inst.wasHit = false;
        inst.wasDestroyed = false;

        inst.rammed = false;

        Rigidbody2D rigidBody = inst.GetComponent<Rigidbody2D>();

        rigidBody.velocity = Vector2.zero;
        rigidBody.angularVelocity = 0F;
        rigidBody.drag = 0;

        inst.smokeEffect.Stop();
        policeInstance = null;

        InstancePoolManager.Instance.DestroyInstance(inst.gameObject);
    }
}
