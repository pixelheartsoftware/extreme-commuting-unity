﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcSoundsManager : MonoBehaviour
{
    public AudioClip[] explosion;

    public AudioClip[] crash;

    public AudioClip horn;

    public AudioClip[] punts;

    public AudioSource audioSource;

    public AudioSource crashSource;

    public AudioSource honkSource;

    private void Start()
    {
        if (!SoundsManager.SoundsDisabled())
        {
            audioSource.Play();
        }
    }

    public void PlayPuntSound()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        if (crashSource.isPlaying)
        {
            return;
        }
        crashSource.clip = punts[UnityEngine.Random.Range(0, punts.Length)];
        crashSource.Play();
    }

    public void PlayExplosion()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        AudioClip toPlay = explosion[Random.Range(0, explosion.Length)];

        audioSource.PlayOneShot(toPlay);
    }

    public void PlayCrash()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        AudioClip toPlay = crash[Random.Range(0, crash.Length)];

        audioSource.PlayOneShot(toPlay);
    }

    public void PlayHonk()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }

        honkSource.Play();
    }

    public void StopHonk()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }

        honkSource.Stop();
    }

    public void PlayHorn()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }

        audioSource.PlayOneShot(horn);
    }
}
