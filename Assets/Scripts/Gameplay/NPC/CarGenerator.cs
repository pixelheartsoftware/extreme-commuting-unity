﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarGenerator : MonoBehaviour
{
    public List<GameObject> carPrefabs;

    public SideLaneGenerator sideLaneGenerator;

    public float removeDistance = 9;

    public float minSpace = 1F;

    public float carSpeed = 0.2F;

    public int carCreateChancePercent = 60;

    public float rightOffsetX = 0F;
    public float leftOffsetX = 0F;

    GameObject topCar = null;

    float playerVerticalOffset;

    float notCreatedCarSpaceMarkerY;

    List<NpcCarController> instances  = new List<NpcCarController>();

    private void Start()
    {
        playerVerticalOffset = PlayerHolder.Instance.GetPlayerTransform().position.y - transform.position.y;
    }

    void Update()
    {
        if (RaceManager.Instance.IsRaceComplete())
        {
            if (instances.Count > 0) {
                foreach (NpcCarController car in instances)
                {
                    car.Explode();
                }
                instances.Clear();
            }
            return;
        }

        // Remove bottom instance if out of sight
        if (instances.Count > 0)
        {
            List<NpcCarController> toRemove = new List<NpcCarController>();
            foreach (NpcCarController car in instances)
            {
                if (Vector2.Distance(car.transform.position, PlayerHolder.Instance.GetPlayerTransform().position) > removeDistance)
                {
                    toRemove.Add(car);
                }

                if (!car.wasDestroyed && !car.wasHit)
                {
                    if (car.myRigidbody.rotation != 0)
                    {
                        car.myRigidbody.rotation = 0;
                    }
                }
            }
            foreach (NpcCarController carToRemove in toRemove)
            {
                RemoveCar(carToRemove);
            }
        }

        // Check is should create a car

        if (notCreatedCarSpaceMarkerY > PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset - minSpace)
        {
            return;
        }

        if (!RandomUtil.Chance(carCreateChancePercent))
        {
            notCreatedCarSpaceMarkerY = PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset;
            return;
        }

        // create a new instance, above the screen
        if ((topCar == null) || (topCar.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset - minSpace))
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(RandomUtil.RandomElement(carPrefabs));

            NpcCarController instCarController = instance.GetComponent<NpcCarController>();
            instCarController.carGenerator = this;

            if (RandomUtil.Chance(50) && CanGenerateRightCar())
            {
                instance.transform.position = new Vector2(transform.position.x + rightOffsetX, PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset);
            }
            else
            {
                instance.transform.position = new Vector2(transform.position.x + leftOffsetX, PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset);
            }

            instCarController.Init(carSpeed);

            instances.Add(instCarController);

            topCar = instance;
        }
    }

    private void RemoveCar(NpcCarController inst)
    {
        inst.wasHit = false;

        inst.pileupCounter = 0;
        inst.rammed = false;

        Rigidbody2D rigidBody = inst.GetComponent<Rigidbody2D>();

        rigidBody.velocity = Vector2.zero;
        rigidBody.angularVelocity = 0F;
        rigidBody.drag = 0;
        inst.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));

        inst.smokeEffect.Stop();
        instances.Remove(inst);

        InstancePoolManager.Instance.DestroyInstance(inst.gameObject);
    }

    private bool CanGenerateRightCar()
    {
        return (sideLaneGenerator.currentElementsLeftToAdd > 2) && (sideLaneGenerator.currentElementsAlreadyAdded > 2);
    }
}
