﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPartController : MonoBehaviour
{
    private float maxContactTime = 0.5F;

    void Update()
    {
        if ((PlayerHolder.Instance.GetPlayerTransform().position - transform.position).magnitude > 10)
        {
            InstancePoolManager.Instance.DestroyInstance(gameObject);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!isActiveAndEnabled)
        {
            return;
        }

        maxContactTime -= Time.deltaTime;

        if (maxContactTime <= 0)
        {
            InstancePoolManager.Instance.DestroyInstance(gameObject);
        }
    }
}
