﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSequenceMusicManager : MonoBehaviour
{
    public AudioSource almostSource;

    public AudioSource cheeringSource;

    public List<AudioClip> cheeringClips;

    public float endingSequenceStartPercent = 0.9F;

    private bool winSequenceStarted = false;

    private float clappingOffset = 0;

    void Update()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }

        float racePercent = RaceManager.Instance.GetRacePercent();

        if (racePercent < endingSequenceStartPercent)
        {
            return;
        }

        if (!RaceManager.Instance.IsRaceComplete()) {
            if (!almostSource.isPlaying)
            {
                almostSource.Play();
            }

            float animationPercent = (racePercent - endingSequenceStartPercent) / (1 - endingSequenceStartPercent);

            MusicManager.Instance.LerpVolume(0, animationPercent + 0.3F);

            almostSource.volume = Mathf.Lerp(almostSource.volume, 1, animationPercent);
        } else if (!winSequenceStarted)
        {
            almostSource.Stop();
            MusicManager.Instance.PlayWinMusic();
            MusicManager.Instance.LerpVolume(1, 1);
            winSequenceStarted = true;
        }

        if (RaceManager.Instance.IsRaceComplete())
        {
            if (clappingOffset <= 0)
            {
                cheeringSource.PlayOneShot(RandomUtil.RandomElement<AudioClip>(cheeringClips));

                clappingOffset = Random.Range(1, 4);
            } else
            {
                clappingOffset -= Time.deltaTime;
            }
        }
    }
}
