﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This is on a Players child object.
public class SideStuntDetectorScript : MonoBehaviour
{
    private List<GameObject> nearMissContacts = new List<GameObject>();

    private List<GameObject> closePassingContacts = new List<GameObject>();

    private PlayerMovementController playerMovementController;

    private void Start()
    {
        playerMovementController = PlayerHolder.Instance.GetPlayer().GetComponentInChildren<PlayerMovementController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Side stunts only work when player has SOME speed
        if ((SpeedUtils.Instance.GetSpeed() / playerMovementController.carMaxSpeed) <= 0.1F)
        {
            return;
        }
        if (gameObject.transform.position.y > collision.gameObject.transform.position.y) // The player already passed the colliding object (it's higher on the screen)
        {
            return;
        }

        if (collision.CompareTag("BackwardCar") || collision.CompareTag("PoliceBack"))
        { 
            nearMissContacts.Add(collision.gameObject);
        }
        else
        if (collision.CompareTag("ForwardCar") || collision.CompareTag("Police"))
        {
            closePassingContacts.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.transform.position.y < collision.gameObject.transform.position.y)
        {
            return;
        }
        if (collision.CompareTag("BackwardCar"))
        {
            NpcCarController carController = collision.GetComponent<NpcCarController>();
            if (carController.wasHit)
            {
                nearMissContacts.Remove(collision.gameObject);
                return;
            }

            if (nearMissContacts.Contains(collision.gameObject))
            {
                nearMissContacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.NearMiss, PixelheartMath.Lerp(PlayerHolder.Instance.GetPlayerTransform().position, collision.gameObject.transform.position, 0.5F), carController, false);
            }
        }
        else
        if (collision.CompareTag("ForwardCar"))
        {
            NpcCarController carController = collision.GetComponent<NpcCarController>();
            if (carController.wasHit)
            {
                closePassingContacts.Remove(collision.gameObject);
                return;
            }
            if (closePassingContacts.Contains(collision.gameObject))
            {
                closePassingContacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.ClosePassing, PixelheartMath.Lerp(PlayerHolder.Instance.GetPlayerTransform().position, collision.gameObject.transform.position, 0.5F), carController, false);
            }
        }
        else if (collision.CompareTag("PoliceBack"))
        {
            PoliceController carController = collision.GetComponent<PoliceController>();
            if (carController.wasHit)
            {
                nearMissContacts.Remove(collision.gameObject);
                return;
            }

            if (nearMissContacts.Contains(collision.gameObject))
            {
                nearMissContacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.NearMiss, PixelheartMath.Lerp(PlayerHolder.Instance.GetPlayerTransform().position, collision.gameObject.transform.position, 0.5F), carController, true);
            }
        }
        else if (collision.CompareTag("Police"))
        {
            PoliceController carController = collision.GetComponent<PoliceController>();
            if (carController.wasHit)
            {
                closePassingContacts.Remove(collision.gameObject);
                return;
            }
            if (closePassingContacts.Contains(collision.gameObject))
            {
                closePassingContacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.ClosePassing, PixelheartMath.Lerp(PlayerHolder.Instance.GetPlayerTransform().position, collision.gameObject.transform.position, 0.5F), carController, true);
            }
        }
    }
}