﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(AudioSource))]
public class StuntManager : Singleton<StuntManager>
{

    public TextMeshProUGUI comboMultiplierUI;
    public TextMeshProUGUI potentialAwesomeUI;

    public StuntMarkerGenerator stuntMarkerGenerator;

    public AwesomeCounterController awesomeCounterController;

    public int pointsForNearMiss = 300;
    public int pointsForPassing = 100;
    public int pointsForClosePassing = 300;
    public int pointsForTorpedo = 500;
    public int pointsForPush = 100;
    public int pointsForNudge = 150;
    public int pointsForRam = 100;
    public int pointsForBrakeCheck = 600;
    public int pointsForShield = 100;
    public int pointsForChicken = 500;
    public int pointsForTailgate = 400;
    public int pointsForSlalom = 350;

    private int comboMultiplier = 1;
    private int potentialAwesome = 0;
    private int potentialAwesomeNoMulti = 0;

    public AudioSource audioSourceStunt;
    public AudioSource audioSourceComboComplete;
    public AudioSource audioSourceSpecial;

    public List<AudioClip> clappingClips;
    public List<AudioClip> booingClips;
    public List<AudioClip> cheeringClips;

    private float comboContinueTimer = 0F;
    public float comboContinueExtendBy = 3F;

    public static StuntManager Instance
    {
        get { return ((StuntManager)_Instance); }
        set { _Instance = value; }
    }

    private void Start()
    {
        AwesomeInfo.Instance.Reset();
    }

    private void Update()
    {
        comboContinueTimer -= Time.deltaTime;

        if (comboContinueTimer <= 0)
        {
            CompleteCombo();
        }
    }

    public void HandleStunt(StuntType type, Vector2 position, StuntSubject car, bool isPolice)
    {
        HandleStunt(type, position, car, isPolice, 0);
    }

    public static bool IsHardCrash(StuntType type)
    {
        switch (type)
        {
            case StuntType.Ram:
            case StuntType.Shield:
            case StuntType.Push:
            case StuntType.Null:
                return true;
        }

        return false;
    }

    public void HandleStunt(StuntType type, Vector2 position, StuntSubject car, bool isPolice, int count)
    {
        if (car != null)
        {
            if (!car.CanAddStunt(type))
            {
                return;
            }

            car.AddStunt(type);
        }

        if (isPolice)
        {
            comboMultiplier += 5;
        }
        switch (type)
        {
            case StuntType.ClosePassing:
                {
                    ContinueCombo(pointsForClosePassing, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice? "Close Cops": "Close", position);
                    break;
                }
            case StuntType.Passing:
                {
                    ContinueCombo(pointsForPassing, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Pass Cops" : "Pass", position);
                    break;
                }
            case StuntType.NearMiss:
                {
                    ContinueCombo(pointsForNearMiss, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Near Cops!" : "Near", position);
                    break;
                }
            case StuntType.Nudge:
                {
                    ContinueCombo(pointsForNudge, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Nudge Cops" : "Nudge", position);
                    break;
                }
            case StuntType.Push:
                {
                    ContinueCombo(pointsForPush, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Push Cops" : "Push", position);
                    break;
                }
            case StuntType.Ram:
                {
                    ContinueCombo(pointsForRam, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Ram Cops" : "Ram", position);
                    break;
                }
            case StuntType.Torpedo:
                {
                    ContinueCombo(pointsForTorpedo, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "TORPEDO COPS!" : "TORPEDO!", position);
                    break;
                }
            case StuntType.BrakeCheck:
                {
                    ContinueCombo(pointsForBrakeCheck, type);
                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "BrakeCheck Cops" : "BrakeCheck", position);
                    break;
                }
            case StuntType.Shield:
                {
                    ContinueCombo(pointsForShield, type);

                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Shield Cops" : "Shield", position);

                    break;
                }
            case StuntType.Chicken:
                {
                    ContinueCombo(pointsForChicken, type);

                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Chicken Cops!" : "Chicken!", position);

                    break;
                }
            case StuntType.Tailgate:
                {
                    ContinueCombo(pointsForTailgate, type);

                    stuntMarkerGenerator.GenerateStuntMarker(isPolice ? "Tailgate Cops" : "Tailgate", position);

                    break;
                }
            case StuntType.Slalom:
                {
                    ContinueCombo(pointsForSlalom * count, type);

                    stuntMarkerGenerator.GenerateStuntMarker("Slalom x" + count.ToString(), position);

                    break;
                }
        }

        AwesomeInfo.Instance.AddStunt(type);

        PlayStuntSound();
    }

    private void PlayStuntSound()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        audioSourceStunt.Stop();
        audioSourceStunt.pitch = Mathf.Lerp(1, 3, (comboMultiplier/20F));
        audioSourceStunt.Play();
    }

    private void ContinueCombo(int points, StuntType type)
    {

        int gainedPoints = points * comboMultiplier;

        if (IsHardCrash(type))
        {
            potentialAwesome = 0;
            potentialAwesomeUI.text = "";

            comboMultiplier = 1;
            comboMultiplierUI.text = "x" + comboMultiplier.ToString();
            return;
        }

        potentialAwesome += gainedPoints;
        potentialAwesomeNoMulti += points;

        comboMultiplier++;

        potentialAwesomeUI.text = potentialAwesome.ToString();
        comboMultiplierUI.text = "x" + comboMultiplier.ToString();

        comboContinueTimer = comboContinueExtendBy;
    }

    internal void CompleteCombo()
    {
        if (potentialAwesome <= 0)
        {
            return;
        }

        int totalAwesome = AwesomeInfo.Instance.AddPoints(potentialAwesome);

        AwesomeInfo.Instance.AddCombo(comboMultiplier - 1);

        if (comboMultiplier >= 2)
        {
            PlayCompleteComboSound();

            if (comboMultiplier >= 5)
            {
                PlayClappingSound();

                if (comboMultiplier >= 10)
                {
                    PlayCheeringSound();
                }
            }
        }



        stuntMarkerGenerator.GenerateStuntPointMarker("+" + potentialAwesome.ToString(), true);

        potentialAwesome = 0;
        potentialAwesomeNoMulti = 0;
        comboMultiplier = 1;
        potentialAwesomeUI.text = "";
        comboMultiplierUI.text = "x1";
    }

    private void PlayCompleteComboSound()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        audioSourceComboComplete.Stop();
        audioSourceComboComplete.Play();
    }

    internal void CancelCombo()
    {
        int totalAwesome = AwesomeInfo.Instance.AddPoints(potentialAwesomeNoMulti);

        if (potentialAwesome > potentialAwesomeNoMulti)
        {
            stuntMarkerGenerator.GenerateStuntPointMarker((potentialAwesome - potentialAwesomeNoMulti).ToString(), false);
        }

        if (comboMultiplier >= 10)
        {
            PlayBooingSound();
        }

        potentialAwesome = 0;
        potentialAwesomeNoMulti = 0;
        comboMultiplier = 1;
        potentialAwesomeUI.text = "";
        comboMultiplierUI.text = "x1";
    }

    private void PlayClappingSound()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }

        audioSourceSpecial.PlayOneShot(RandomUtil.RandomElement<AudioClip>(clappingClips));
    }

    private void PlayBooingSound()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }

        audioSourceSpecial.PlayOneShot(RandomUtil.RandomElement<AudioClip>(booingClips));
    }

    private void PlayCheeringSound()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }

        audioSourceSpecial.PlayOneShot(RandomUtil.RandomElement<AudioClip>(cheeringClips));
    }
}

public enum StuntType
{
    ClosePassing, NearMiss, Torpedo, Push, Nudge, Ram, BrakeCheck, Shield, Chicken, Tailgate, Slalom, Passing, Null
}