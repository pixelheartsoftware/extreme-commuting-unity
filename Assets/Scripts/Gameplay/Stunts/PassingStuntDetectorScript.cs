﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassingStuntDetectorScript : MonoBehaviour
{
    public float maxSlalomIntervalInSeconds = 3F;

    private List<GameObject> contacts = new List<GameObject>();

    private float lastStuntTime = 0;

    private int currentSlalomCounter = 0;

    // The slalom works when the player goes around one car from the left and the next from the right, and so on.
    private bool lastStuntSideLeft;

    // The slalom only applies when the player slaloms around ForwardCars or BackwardCars, not both at the same time.
    private bool lastStuntWithForwardCar;

    private PlayerMovementController playerMovementController;

    private void Start()
    {
        playerMovementController = PlayerHolder.Instance.GetPlayer().GetComponentInChildren<PlayerMovementController>();
    }

    private void Update()
    {
        if ((Time.realtimeSinceStartup - lastStuntTime) > maxSlalomIntervalInSeconds)
        {
            HandleSlalomEnd(null);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.transform.position.y > collision.gameObject.transform.position.y) // The player already passed the colliding object (it's higher on the screen)
        {
            return;
        }

        if (collision.CompareTag("BackwardCar") || collision.CompareTag("ForwardCar") || collision.CompareTag("Police") || collision.CompareTag("PoliceBack"))
        {
            contacts.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        try
        {
            if ((SpeedUtils.Instance.GetSpeed() / playerMovementController.carMaxSpeed) <= 0.1F)
            {
                return;
            }

            if (collision.CompareTag("BackwardCar"))
            {
                NpcCarController carController = collision.GetComponent<NpcCarController>();
                if (carController.wasHit)
                {
                    return;
                }

                if (contacts.Contains(collision.gameObject))
                {
                    HandleSlalom(collision.gameObject, false, carController);
                }
            }
            else
            if (collision.CompareTag("ForwardCar"))
            {
                if (gameObject.transform.position.y < collision.gameObject.transform.position.y) // The player already passed the colliding object (it's higher on the screen)
                {
                    return;
                }

                NpcCarController carController = collision.GetComponent<NpcCarController>();
                if (carController.wasHit)
                {
                    return;
                }
                if (contacts.Contains(collision.gameObject))
                {
                    StuntManager.Instance.HandleStunt(StuntType.Passing, collision.transform.position, carController, false);

                    HandleSlalom(collision.gameObject, true, carController);
                }
            }
            else
            if (collision.CompareTag("Police"))
            {
                PoliceController policeController = collision.GetComponent<PoliceController>();
                if (policeController.wasHit)
                {
                    return;
                }
                if (contacts.Contains(collision.gameObject))
                {
                    StuntManager.Instance.HandleStunt(StuntType.Passing, collision.transform.position, policeController, true);

                    HandleSlalom(collision.gameObject, true, policeController);
                }
            }
            if (collision.CompareTag("PoliceBack"))
            {
                PoliceController policeController = collision.GetComponent<PoliceController>();
                if (policeController.wasHit)
                {
                    return;
                }
                if (contacts.Contains(collision.gameObject))
                {
                    HandleSlalom(collision.gameObject, true, policeController);
                }
            }
        } finally
        {
            contacts.Remove(collision.gameObject);
        }
    }

    private void HandleSlalom(GameObject gameObject, bool forwardCar, StuntSubject car)
    {

        if (lastStuntTime == 0)
        {
            lastStuntTime = Time.realtimeSinceStartup;
            lastStuntSideLeft = gameObject.transform.position.x < PlayerHolder.Instance.GetPlayerTransform().position.x;
            lastStuntWithForwardCar = forwardCar;
        }
        else
        if ((Time.realtimeSinceStartup - lastStuntTime) <= maxSlalomIntervalInSeconds)
        {
            bool currentStuntSideLeft = gameObject.transform.position.x < PlayerHolder.Instance.GetPlayerTransform().position.x;

            if (((currentStuntSideLeft == lastStuntSideLeft) || (forwardCar != lastStuntWithForwardCar)))
            {
                HandleSlalomEnd(car);
                return;
            }
            lastStuntSideLeft = gameObject.transform.position.x < PlayerHolder.Instance.GetPlayerTransform().position.x;
            lastStuntWithForwardCar = forwardCar;
            currentSlalomCounter++;
            lastStuntTime = Time.realtimeSinceStartup;
        }
        else
        {
            HandleSlalomEnd(car);
        }
    }

    private void HandleSlalomEnd(StuntSubject car)
    {
        if (currentSlalomCounter > 3) {
            StuntManager.Instance.HandleStunt(StuntType.Slalom, PlayerHolder.Instance.GetPlayerTransform().position, car, false, currentSlalomCounter);
        }
        lastStuntTime = 0;
        currentSlalomCounter = 0;
    }

    internal void HandlePlayerContact()
    {
        currentSlalomCounter = 0;
        lastStuntTime = 0;
    }
}
