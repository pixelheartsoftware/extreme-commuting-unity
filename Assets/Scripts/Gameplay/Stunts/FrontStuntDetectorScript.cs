﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontStuntDetectorScript : MonoBehaviour
{
    private List<GameObject> contacts = new List<GameObject>();

    private PlayerMovementController playerMovementController;

    private void Start()
    {
        playerMovementController = PlayerHolder.Instance.GetPlayer().GetComponentInChildren<PlayerMovementController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Front stunts only work when player has SOME speed
        if ((SpeedUtils.Instance.GetSpeed() / playerMovementController.carMaxSpeed) <= 0.1F)
        {
            return;
        }
        if (collision.CompareTag("BackwardCar") || collision.CompareTag("ForwardCar") || collision.CompareTag("Police") || collision.CompareTag("PoliceBack"))
        {
            contacts.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("BackwardCar"))
        {
            NpcCarController carController = collision.GetComponent<NpcCarController>();
            if (carController.wasHit)
            {
                return;
            }
            if (contacts.Contains(collision.gameObject))
            {
                contacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.Chicken, collision.gameObject.transform.position, carController, false);
            }
        }
        else
        if (collision.CompareTag("ForwardCar"))
        {
            NpcCarController carController = collision.GetComponent<NpcCarController>();
            if (carController.wasHit)
            {
                return;
            }
            if (contacts.Contains(collision.gameObject))
            {
                contacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.Tailgate, collision.gameObject.transform.position, carController, false);
            }
        }
        else if (collision.CompareTag("PoliceBack"))
        {
            PoliceController carController = collision.GetComponent<PoliceController>();
            if (carController.wasHit)
            {
                return;
            }
            if (contacts.Contains(collision.gameObject))
            {
                contacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.Chicken, collision.gameObject.transform.position, carController, true);
            }
        }
        else if (collision.CompareTag("Police"))
        {
            PoliceController carController = collision.GetComponent<PoliceController>();
            if (carController.wasHit)
            {
                return;
            }
            if (contacts.Contains(collision.gameObject))
            {
                contacts.Remove(collision.gameObject);

                StuntManager.Instance.HandleStunt(StuntType.Tailgate, collision.gameObject.transform.position, carController, true);
            }
        }
    }
}
