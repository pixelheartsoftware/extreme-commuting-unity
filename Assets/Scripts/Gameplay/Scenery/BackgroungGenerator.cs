﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroungGenerator : MonoBehaviour
{
    public GameObject backgroundPrefab;

    public float initialInstancesCount = 10;

    float playerVerticalOffset;

    float removeY = -9;

    private GameObject topObject;

    List<GameObject> instances = new List<GameObject>();

    private void Start()
    {
        playerVerticalOffset = PlayerHolder.Instance.GetPlayerTransform().position.y - transform.position.y;

        for (int i = 0; i < initialInstancesCount; i++)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(backgroundPrefab);

            if (topObject == null)
            {
                instance.transform.position = new Vector2(transform.position.x, removeY);
            }
            else
            {
                SpriteUtil.PlaceAbove(instance, topObject, transform.position.x);
            }
            topObject = instance;
            instances.Add(instance);
        }
    }

    void Update()
    {
        if (instances.Count > 0)
        {
            GameObject bottomObject = instances[0];

            if (bottomObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y + removeY)
            {
                instances.Remove(bottomObject);

                InstancePoolManager.Instance.DestroyInstance(bottomObject);
            }
        }

        if (topObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(backgroundPrefab);
            SpriteUtil.PlaceAbove(instance, topObject, transform.position.x);
            instances.Add(instance);
            topObject = instance;
        }
    }
}
