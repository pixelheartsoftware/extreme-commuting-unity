﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSwitcher : MonoBehaviour
{
    public List<Sprite> sprites;

    private SpriteRenderer mySpriteRenderer;

    void Awake()
    {
        mySpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void SwitchSprite()
    {
        if (sprites != null)
        {
            int pos = Random.Range(0, sprites.Count - 1);

            Sprite s = sprites[pos];

            mySpriteRenderer.sprite = s;
        }
    }
}
