﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HousesGenerator : MonoBehaviour
{
    public GameObject housePrefab;

    public float initialInstancesCount = 5;

    public float minOffset = 1;

    float playerVerticalOffset;

    float removeY = -9;

    private GameObject topObject;

    List<SpriteSwitcher> instances;

    private void Start()
    {
        playerVerticalOffset = PlayerHolder.Instance.GetPlayerTransform().position.y - transform.position.y;

        float yy = 0;

        instances = new List<SpriteSwitcher>();

        for (int i = 0; i < initialInstancesCount; i++)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(housePrefab);

            instance.transform.position = new Vector2(transform.position.x, PlayerHolder.Instance.GetPlayerTransform().position.y + removeY + yy);

            topObject = instance;

            SpriteSwitcher switcher = instance.GetComponent<SpriteSwitcher>();
            switcher.SwitchSprite();

            instances.Add(switcher);

            yy += minOffset + Random.Range(0, minOffset);
        }
    }

    void Update()
    {
        if (instances.Count > 0)
        {
            GameObject bottomObject = instances[0].gameObject;

            if (bottomObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y + removeY)
            {
                instances.Remove(instances[0]);

                InstancePoolManager.Instance.DestroyInstance(bottomObject);
            }
        }

        if (topObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset - minOffset)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(housePrefab);
            instance.transform.position = new Vector2(transform.position.x, PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset + Random.Range(0, minOffset));

            SpriteSwitcher switcher = instance.GetComponent<SpriteSwitcher>();
            switcher.SwitchSprite();

            instances.Add(switcher);
            topObject = instance;
        }
    }
}
