﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RaceManager : Singleton<RaceManager>
{
    public float raceLength = 1000;

    public float currentPosition = 0;
    internal bool forceQuit;

    public static RaceManager Instance
    {
        get { return ((RaceManager)_Instance); }
        set { _Instance = value; }
    }

    void Awake()
    {
        SceneManager.sceneUnloaded += OnSceneUnload;
    }

    public float GetRacePercent()
    {
        return currentPosition / raceLength;
    }

    public bool IsRaceComplete()
    {
        return currentPosition >= raceLength;
    }

    void OnSceneUnload(Scene scene)
    {
        AwesomeInfo.Instance.SetRaceLength(raceLength);
        AwesomeInfo.Instance.SetRaceLengthCompleted(currentPosition);
        Time.timeScale = 1;
    }

    void Update()
    {
        if (forceQuit)
        {
            SceneManager.LoadScene("Summary", LoadSceneMode.Single);
            return;
        }
    }

    void FixedUpdate()
    {
        if (forceQuit)
        {
            SceneManager.LoadScene("Summary", LoadSceneMode.Single);
            return;
        }

        if (!IsRaceComplete()) {
            currentPosition += SpeedUtils.Instance.GetSpeedInMetersPerSecond();
        }
        
    }
}
