﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionDirectionsManager : MonoBehaviour
{
    public List<PlayerCollisionDirectionDetector> detectors;

    public HashSet<CollisionDirection> GetCollisionDirections(GameObject gameObject)
    {
        HashSet<CollisionDirection> directions = new HashSet<CollisionDirection>(); 
        foreach (PlayerCollisionDirectionDetector detector in detectors)
        {
            if (detector.IsObjectInCollision(gameObject))
            {
                directions.Add(detector.direction);
            }
        }

        return directions;
    }

}
