﻿using UnityEngine;
using static Controls;
using static ErrorCodes;

public class TurboController : MonoBehaviour
{
    public float carBaseTurbo = 5F;
    public float carBaseTurboCooldownTime = 5F;
    public float carTurboCoolingRate = 1F;
    public float carTurboHeatingRate = 1F;

    private float turboLeft = 0F;
    private bool turboCooldown = false;

    public ParticleSystem turboParticleSystem;

    private PlayerSoundsManager playerSoundsManager;

    private void Awake()
    {
        //turboParticleSystem = gameObject.FindComponentInChildWithTag<ParticleSystem>("TurboFlameOnCar");

        turboLeft = carBaseTurbo;

        playerSoundsManager = gameObject.GetComponent<PlayerSoundsManager>();
    }


    private void Update()
    {
        if (IsTurboActivated())
        {
            turboParticleSystem.Play();
            
            if (turboLeft >= carBaseTurbo)
            {
                playerSoundsManager.PlayTurboUp();
            }

            turboLeft -= Time.deltaTime * carTurboHeatingRate;
        } else if (turboCooldown)
        {
            turboLeft += carTurboCoolingRate * Time.deltaTime;

            if (turboLeft >= carBaseTurbo)
            {
                turboCooldown = false;
                turboLeft = carBaseTurbo;
            }
        } else if (turboLeft <= 0F)
        {
            playerSoundsManager.PlayTurboDown();
            turboCooldown = true;
            Controls.Instance.ResetTurbo();
        } else
        {
            playerSoundsManager.PlayTurboDown();
            turboLeft += carTurboCoolingRate * Time.deltaTime;
        }

        turboLeft = Mathf.Clamp(turboLeft, 0F, carBaseTurbo);
    }

    public bool IsTurboActivated()
    {
        return !Controls.Instance.GetBrake() && Controls.Instance.GetTurbo() && (turboLeft > 0F) && (!turboCooldown);
    }

    public float GetTurboLeftPercent()
    {
        return (turboLeft / carBaseTurbo) * 100F;
    }

    public bool IsTurboCooldown()
    {
        return turboCooldown;
    }
}
