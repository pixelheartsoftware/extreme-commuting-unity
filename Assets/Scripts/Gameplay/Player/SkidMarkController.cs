﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkidMarkController : MonoBehaviour
{
    public Sprite frontSkid;
    public Sprite backSkid;

    private float timeToLive = 2;

    public void Init(SkidType type)
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if (type == SkidType.FRONT)
        {
            spriteRenderer.sprite = frontSkid;
        } else if (type == SkidType.REAR)
        {
            spriteRenderer.sprite = backSkid;
        }

        timeToLive = 2;
    }

    private void Update()
    {
        if (timeToLive < 0)
        {
            InstancePoolManager.Destroy(gameObject);
            return;
        }
        else
        {
            timeToLive -= Time.deltaTime;
        }
    }
}

public enum SkidType
{
    FRONT, REAR
}
