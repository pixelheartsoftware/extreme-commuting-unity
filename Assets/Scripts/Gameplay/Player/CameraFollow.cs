﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class CameraFollow : MonoBehaviour
{
    public float vOffset = 0;
    public float shakeFactor = 1;

    private float baseFov;
    private Vector3 basePosition;

    internal float shakePower = 0;

    public float introTime = 2;
    private float currentIntroTimeout;

    public float introCameraOffset = 1;
    private float currentCameraOffset;

    public float outroCameraOffset = 2;

    private bool startOutroReady, startOutro, autoCloseStarted;

    public float outroAutoCloseTime = 2;

    private float speedYOffset = 0;

    private void Start()
    {
        baseFov = Camera.main.fieldOfView;
        basePosition = Camera.main.transform.position;

        currentIntroTimeout = introTime;
        currentCameraOffset = introCameraOffset;

    }

    void FixedUpdate()
    {
        if (RaceManager.Instance.IsRaceComplete())
        {
            VibrationUtil.Instance.AddLowFreq(Random.Range(1, 2));
            VibrationUtil.Instance.AddHiFreq(Random.Range(1, 2));

            if (!autoCloseStarted)
            {
                StartCoroutine(AutoEnd());
                autoCloseStarted = true;
            }

            if (startOutroReady)
            {
                if (startOutro)
                {
                    if (transform.position.y - Camera.main.transform.position.y > outroCameraOffset)
                    {
                        SceneManager.LoadScene("Summary", LoadSceneMode.Single);
                        return;
                    }
                    else
                    {
                        return;
                    }
                } else if (Input.anyKey)
                {
                    startOutro = true;
                }
            } else if (!Input.anyKey)
            {
                startOutroReady = true;
            }
        }

        if (currentIntroTimeout > 0)
        {
            introCameraOffset = Mathf.Lerp(currentCameraOffset, 0, 1 - currentIntroTimeout / introTime);
            currentIntroTimeout -= Time.fixedDeltaTime;
        }

        Vector3 shake = Vector3.zero;

        if (shakePower > 0)
        {
            float xx = Mathf.Clamp((Random.value * 2 - 1) * shakePower, -1, 1) * shakeFactor;
            float yy = Mathf.Clamp((Random.value - 0.5F) * shakePower, -1, 1) * shakeFactor;
            shake = new Vector3(xx, yy, 0F);

            shakePower *= 0.8F;
        }

        VibrationUtil.Instance.AddLowFreq(shakePower);

        speedYOffset = Mathf.Lerp(speedYOffset, SpeedUtils.Instance.GetSpeed() / 10, 0.5F);

        Camera.main.transform.position = new Vector3(basePosition.x, transform.position.y + vOffset + introCameraOffset + speedYOffset, basePosition.z) + shake;

        Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, baseFov + SpeedUtils.Instance.GetSpeed(), 0.2F);
    }

    IEnumerator AutoEnd()
    {
        float startTime = Time.realtimeSinceStartup;
        while (true)
        {
            if (outroAutoCloseTime <= Time.realtimeSinceStartup - startTime)
            {
                startOutro = true;
                startOutroReady = true;
                yield break;
            }
            yield return null;
        }
    }
}
