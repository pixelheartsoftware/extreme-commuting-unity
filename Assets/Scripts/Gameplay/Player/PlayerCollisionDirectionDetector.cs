﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionDirectionDetector : MonoBehaviour
{
    List<GameObject> touchingColliders = new List<GameObject>();

    public CollisionDirection direction;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        touchingColliders.Add(collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        touchingColliders.Remove(collision.gameObject);
    }

    public bool IsObjectInCollision(GameObject gameObject)
    {
        return touchingColliders.Contains(gameObject);
    }

}
