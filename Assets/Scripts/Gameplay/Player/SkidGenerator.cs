﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkidGenerator : MonoBehaviour
{
    public GameObject skidPrefab;

    public SkidType type;

    public void GenerateSkidMark(Quaternion rotation)
    {
        GameObject obj = InstancePoolManager.Instance.CreateInstance(skidPrefab);

        obj.transform.position = transform.position;
        obj.transform.rotation = rotation;

        obj.GetComponent<SkidMarkController>().Init(type);
    }
}
