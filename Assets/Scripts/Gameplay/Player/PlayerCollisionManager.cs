﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionManager : MonoBehaviour
{
    internal int leftLaneContacts, rightLaneContacts, roadContacts;

    public PlayerMovementController playerMovementController;

    public PlayerCollisionDirectionsManager playerCollisionDirectionsManager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Road"))
        {
            roadContacts++;
        }
        else if (collision.CompareTag("RoadLeft") || collision.CompareTag("LeftLaneBeginning") || collision.CompareTag("LeftLaneEnding"))
        {
            leftLaneContacts++;
        }
        else if (collision.CompareTag("BackgroundParallax")
          || collision.CompareTag("HouseLeft")
          || collision.CompareTag("HouseRight"))
        {
            if (playerMovementController.isDestroyed)
            {
                playerMovementController.Explode();
            }
        }
        else if (collision.CompareTag("RoadRight") || collision.CompareTag("RightLaneBeginning") || collision.CompareTag("RightLaneEnding"))
        {
            rightLaneContacts++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Road"))
        {
            roadContacts--;
        }
        else
        if (collision.CompareTag("RoadLeft") || collision.CompareTag("LeftLaneBeginning") || collision.CompareTag("LeftLaneEnding"))
        {
            leftLaneContacts--;
        }
        else
        if (collision.CompareTag("RoadRight") || collision.CompareTag("RightLaneBeginning") || collision.CompareTag("RightLaneEnding"))
        {
            rightLaneContacts--;
        }

        if (collision.CompareTag("ForwardCar") || collision.CompareTag("BackwardCar") || collision.CompareTag("Police") || collision.CompareTag("PoliceBack"))
        {
            playerMovementController.touchingCrashedCars.Remove(collision.gameObject);
        }
    }

}
