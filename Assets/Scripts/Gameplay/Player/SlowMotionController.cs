﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionController : MonoBehaviour
{
    public bool slowMotionEnabled = true;
    public float minTimeScale = 0.3F;
    public float baseTimeScale = 0.8F;
    public float collisionMaxDistance = 2F;

    private List<Collider2D> collidingTransforms = new List<Collider2D>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BackwardCar"))
        {
            NpcCarController carController = collision.GetComponent<NpcCarController>();
            if (!carController.wasHit)// don't slow the motion when near crashed cars
            {
                carController.soundsManager.PlayHonk();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("BackwardCar"))
        {
            NpcCarController carController = collision.GetComponent<NpcCarController>();

            if (carController.wasHit)// don't slow the motion when near crashed cars
            {
                Time.timeScale = 1;
                return;
            }
            if (!collidingTransforms.Contains(collision))
            {
                collidingTransforms.Add(collision);
            }

            SetTimeScale();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "BackwardCar")
        {
            collidingTransforms.Remove(collision);

            SetTimeScale();
        }
    }

    private void SetTimeScale()
    {
        if (!slowMotionEnabled)
        {
            return;
        }
        if (collidingTransforms.Count > 0)
        {
            float minY = int.MaxValue;
            Collider2D minCollider = null;
            foreach (Collider2D collider in collidingTransforms)
            {
                if (collider.transform.position.y < minY)
                {
                    minY = collider.transform.position.y;
                    minCollider = collider;
                }
            }

            float offsetY = 1F;

            float distance = minY - (gameObject.transform.position.y + offsetY);
            distance = Mathf.Min(distance, collisionMaxDistance);

            Time.timeScale = Mathf.Max(Mathf.Lerp(1, minTimeScale, baseTimeScale - (distance / collisionMaxDistance)), minTimeScale);
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
