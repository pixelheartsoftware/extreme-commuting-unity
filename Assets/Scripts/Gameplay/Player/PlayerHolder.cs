﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHolder : Singleton<PlayerHolder>
{
    internal CarDefinition selectedCar;

    private GameObject player;

    private TurboController playerTurboController;

    private Transform playerTransform;

    private PlayerMovementController playerMovementController;

    private void Awake()
    {
        if (PlayerHolder.Instance != this)
        {
            PlayerHolder.Instance.Awake();
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Gameplay")
        {
            if (selectedCar == null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
                playerTransform = player.GetComponentInChildren<PlayerCollisionManager>().transform;
                playerMovementController = player.GetComponentInChildren<PlayerMovementController>();
                playerTurboController = player.GetComponentInChildren<TurboController>();
                player.GetComponentInChildren<PlayerSoundsManager>().StartEngine();
                AwesomeInfo.Instance.CarName = "Yellow";
            } else 
            {
                GameObject oldPlayer = GameObject.FindGameObjectWithTag("Player");

                GameObject carPrefab = selectedCar.playerPrefab;

                player = GameObject.Instantiate(carPrefab, oldPlayer.transform.position, oldPlayer.transform.rotation);

                playerTransform = player.GetComponentInChildren<PlayerCollisionManager>().transform;

                playerMovementController = player.GetComponentInChildren<PlayerMovementController>();

                GameObject.Destroy(oldPlayer);

                playerTurboController = player.GetComponentInChildren<TurboController>();

                player.GetComponentInChildren<PlayerSoundsManager>().StartEngine();

                AwesomeInfo.Instance.CarName = selectedCar.carName;
            }

        }
    }

    private void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Gameplay")
        {
            GameObject oldPlayer = GameObject.FindGameObjectWithTag("Player");

            player = GameObject.Instantiate(selectedCar.playerPrefab, oldPlayer.transform.position, oldPlayer.transform.rotation);

            playerTransform = player.GetComponentInChildren<PlayerCollisionManager>().transform;

            playerMovementController = player.GetComponentInChildren<PlayerMovementController>();

            GameObject.Destroy(oldPlayer);

            playerTurboController = player.GetComponentInChildren<TurboController>();

            player.GetComponentInChildren<PlayerSoundsManager>().StartEngine();

            AwesomeInfo.Instance.CarName = selectedCar.carName;
        } else
        {
            player = null;
        }
    }

    public static PlayerHolder Instance
    {
        get { return ((PlayerHolder)_Instance); }
        set { _Instance = value; }
    }

    internal GameObject GetPlayer()
    {
        return player;
    }

    internal Transform GetPlayerTransform()
    {
        return playerTransform;
    }

    internal PlayerMovementController GetPlayerMovementController()
    {
        return playerMovementController;
    }

    internal TurboController GetTurboController()
    {
        return playerTurboController;
    }
}
