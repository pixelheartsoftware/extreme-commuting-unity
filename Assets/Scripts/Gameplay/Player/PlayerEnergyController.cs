﻿using System;
using UnityEngine;

public class PlayerEnergyController : MonoBehaviour
{
    public delegate void EnergySet(float maxEnergy);
    public static event EnergySet OnEnergySet;

    public delegate void EnergyChange(float energyChange);
    public static event EnergyChange OnEnergyChange;

    public float playerEnergyReplenishRate = 1;

    public float replenishWaitTime = 0;
    private float currentReplenishWaitTime = 0;

    private float currentEnergy;
    public float maxEnergy;

    private void Start()
    {
        currentEnergy = maxEnergy;
        OnEnergySet(maxEnergy);
    }

    private void Update()
    {
        if (currentReplenishWaitTime > 0)
        {
            currentReplenishWaitTime -= Time.deltaTime;
            return;
        }

        OnEnergyChange?.Invoke(playerEnergyReplenishRate * Time.deltaTime);
    }

    public void DeductEnergy(float energy)
    {
        OnEnergyChange?.Invoke(-energy);

        currentReplenishWaitTime = replenishWaitTime;
    }

}
