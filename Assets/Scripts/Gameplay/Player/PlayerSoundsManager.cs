﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SoundsManager;

[RequireComponent(typeof(AudioSource))]
public class PlayerSoundsManager : MonoBehaviour
{
    public AudioSource staticSource;
    public AudioSource specialSource;
    public AudioSource turboSource;
    public AudioSource brakingSource;

    public AudioClip engineStart;
    public AudioClip engineStop;
    public AudioClip engineRoar;
    public AudioClip engineStatic;

    public AudioClip[] explosion;

    public AudioClip turboUp;
    public AudioClip turboDown;

    public void PlayEngineStart()
    {
        if (SoundsDisabled())
        {
            return;
        }
        specialSource.PlayOneShot(engineStart);
    }

    public void PlayEngineStop()
    {
        if (SoundsDisabled())
        {
            return;
        }
        specialSource.PlayOneShot(engineStop);
    }

    public void PlayBrakingSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        if (brakingSource.isPlaying)
        {
            return;
        }
        brakingSource.Play();
    }

    public void StopBrakingSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        brakingSource.Stop();
    }

    public void PlayEngineRoar()
    {
        if (SoundsDisabled())
        {
            return;
        }
        specialSource.PlayOneShot(engineRoar);
    }

    internal void StartEngine()
    {
        if (SoundsDisabled())
        {
            return;
        }
        PlayEngineRoar();
        LoopEngineStatic();
    }

    public void LoopEngineStatic()
    {
        if (SoundsDisabled())
        {
            return;
        }
        staticSource.Play();
    }

    public void PlayTurboUp()
    {
        if (SoundsDisabled())
        {
            return;
        }
        if (turboSource.clip == turboUp)
        {
            return;
        }
        turboSource.Stop();
        turboSource.clip = turboUp;
        turboSource.Play();
    }


    public void PlayTurboDown()
    {
        if (SoundsDisabled())
        {
            return;
        }
        if (turboSource.clip == turboDown || turboSource.clip == null)
        {
            return;
        }
        turboSource.Stop();
        turboSource.clip = turboDown;
        turboSource.Play();
    }

    internal void SetSpeed(float minSpeed, float currentSpeed, float maxSpeed)
    {
        staticSource.pitch = Mathf.Lerp(1, 3, (currentSpeed - minSpeed) / (maxSpeed - minSpeed));
    }

    public void PlayExplosion()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        AudioClip toPlay = explosion[UnityEngine.Random.Range(0, explosion.Length)];

        specialSource.PlayOneShot(toPlay);
    }
}
