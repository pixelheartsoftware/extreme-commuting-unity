﻿using System;
using UnityEngine;
using static Controls;
using static ErrorCodes;
using System.Collections;
using System.Collections.Generic;
using static StuntManager;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class PlayerMovementController : MonoBehaviour
{
    public float revsPerFrameFactor = 0.15F;

    public bool CanMoveLeft { get; set; } = true;
    public bool CanMoveRight { get; set; } = true;

    public float carMinSpeed = 2F;
    public float carMaxSpeed = 12F;
    public float carTurboRevsPerFrameFactor = 1.9F;
    public float carBrakePerFrameFactor = 2F;
    public float carIdleRevs = 2F;
    public int carSlowMoAgilityPercent = 60;
    public int carAgilityPercent = 60;

    public float brakeCheckMaxTime = 0.3F;

    public float carBrakeCheckPerFrameFactor = 4F;

    public float horizontalClamp = 0.01F;

    public float revs = 0F;
    public float baseRevs = 10F;

    internal float roadCenterX = 0;

    internal TurboController turboController;

    private Rigidbody2D collisionManagerRigidBody;

    private Rigidbody2D myRigidBody;

    public List<float> roadAnchors = new List<float>();

    private float lastHorizontalMovement = 0F;
    public float horizontalSnapMinDistance = 0.01F;

    internal bool crashed = false;
    public float crashRevsPerFrameFactor = 0.5F;

    public float maxRotationAngle = 3;

    private PlayerEnergyController energyController;

    private PlayerSoundsManager soundsManager;

    public PassingStuntDetectorScript passingStuntDetectorScript;

    private float runStartTime;

    private CameraFollow cameraFollow;

    public List<SkidGenerator> skidMarkGenerators;

    internal bool isDestroyed = false;

    public ParticleSystem smokeEffect;

    public ParticleSystem explosionEffect;

    public List<GameObject> carPartPrefabs;

    private bool isExploded;

    public PlayerCollisionDirectionsManager playerCollisionDirectionsManager;

    public float minHitForce = 1f;

    public PlayerCollisionManager playerCollisionManager;

    internal List<GameObject> touchingCrashedCars = new List<GameObject>();

    public float touchingCrashedCarMaxRevsFactor = 0.8F;

    private float collisionStayCrashOffset = 0.5F;

    public SnapAnchormanager snapAnchor;

    private SnapAnchormanager currentSnappedAnchor;

    private void Awake()
    {
        turboController = GetComponent<TurboController>();

        collisionManagerRigidBody = playerCollisionManager.GetComponent<Rigidbody2D>();

        myRigidBody = GetComponent<Rigidbody2D>();

        energyController = GetComponent<PlayerEnergyController>();

        soundsManager = GetComponent<PlayerSoundsManager>();

        runStartTime = Time.realtimeSinceStartup;

        cameraFollow = GetComponent<CameraFollow>();
    }

    private void FixedUpdate()
    {
        if (Time.timeScale == 0)
        {
            return;
        }

        if (!RaceManager.Instance.IsRaceComplete() && !isDestroyed) {
            myRigidBody.velocity = new Vector2(GetHorizontalMovement(), GetVerticalMovement());
        } else if (RaceManager.Instance.IsRaceComplete())
        {
            float movementX = -transform.position.x / 3;

            myRigidBody.velocity = new Vector2(movementX, myRigidBody.velocity.y);
            myRigidBody.angularVelocity = 0;
            myRigidBody.rotation = 0;
        }
    }

    private float GetHorizontalMovement()
    {
        float calculatedAgility = (carSlowMoAgilityPercent - 100) / 100F;
        if (revs > baseRevs * carTurboRevsPerFrameFactor)
        {
            calculatedAgility -= 0.3F;
        }
        calculatedAgility = Mathf.Clamp(calculatedAgility, 0, 1);

        float maxLeft = -horizontalClamp;
        float maxRight = horizontalClamp;

        bool isOnRightSide = transform.position.x > roadCenterX;
        if (playerCollisionManager.roadContacts == 0) { 
            if (!isOnRightSide && (playerCollisionManager.leftLaneContacts == 0)) { maxLeft = 0; }
            if (isOnRightSide && (playerCollisionManager.rightLaneContacts == 0)) { maxRight = 0; }
        }

        float horizontal = 0;

        if (Controls.Instance.GetLeft())
        {
            horizontal = Mathf.Clamp(-1 * Controls.Instance.GetLeftFloat(), maxLeft, 0);
        }
        else
        if (Controls.Instance.GetRight())
        {
            horizontal = Mathf.Clamp(1 * Controls.Instance.GetRightFloat(), 0, maxRight);

        }
        else
        if (snapAnchor != null && currentSnappedAnchor != snapAnchor && Mathf.Abs(lastHorizontalMovement) > 0)
        {
            float dist = snapAnchor.transform.position.x - transform.position.x;

            if (Mathf.Sign(lastHorizontalMovement) != Mathf.Sign(dist))
            {
                horizontal = Mathf.Abs(lastHorizontalMovement * 0.5F) * Mathf.Sign(dist);
            } else
            {
                horizontal = Mathf.Abs(lastHorizontalMovement) * Mathf.Sign(dist);
            }

            if (Mathf.Abs(horizontal) < 0.005F)
            {
                currentSnappedAnchor = snapAnchor; 
            }
        }

        lastHorizontalMovement = horizontal;

        SetCarRotation(horizontal, carAgilityPercent/100F);

        return horizontal * (Time.deltaTime / Mathf.Clamp(Time.timeScale, calculatedAgility, 1F)) * (carAgilityPercent);
    }

    private void SetCarRotation(float horizontal, float agility)
    {
        float currentRotationAngle = Mathf.Lerp(myRigidBody.rotation, -horizontal * maxRotationAngle, agility);

        myRigidBody.rotation = currentRotationAngle;
    }

    private float FindNearestAnchor()
    {
        float minDistance = int.MaxValue;

        float playerX = transform.position.x;

        foreach (float anchor in roadAnchors)
        {
            float dist = Mathf.Abs(anchor - playerX);
            if (dist < minDistance)
            {
                minDistance = dist;
            }
        }

        Debug.Log("Nearest anchor: " + minDistance);

        return minDistance;
    }


    private float GetVerticalMovement()
    {
        bool acceleration = Controls.Instance.GetAcceleration();
        bool brake = Controls.Instance.GetBrake();
        bool turboActivated = turboController.IsTurboActivated();

        bool brakeCheck = Controls.Instance.GetBrakeCheck() && (Controls.Instance.GetBrakeCheckSeconds() < brakeCheckMaxTime);

        myRigidBody.isKinematic = turboActivated;

        bool isOnRightSide = transform.position.x > roadCenterX;
        bool isOnSidewalk = false;
        if (playerCollisionManager.roadContacts == 0)
        {
            isOnSidewalk = (!isOnRightSide && (playerCollisionManager.leftLaneContacts == 0)) || (isOnRightSide && (playerCollisionManager.rightLaneContacts == 0));
        }

        float x = revs / baseRevs;

        if (acceleration)
        {
            AwesomeInfo.Instance.SetAccelerationUsed();
        }

        if (!acceleration && !turboActivated) 
        {
            if (Time.realtimeSinceStartup - runStartTime > 2)
            {
                AwesomeInfo.Instance.SetNoImpossible();
            }
        }

        if (brake && (revs > 0))
        {
            if (brakeCheck)
            {
                revs -= revsPerFrameFactor * carBrakeCheckPerFrameFactor;
            } else
            {
                revs -= revsPerFrameFactor * carBrakePerFrameFactor;
            }
        } else if (isOnSidewalk)
        {
            revs -= revsPerFrameFactor;
            StuntManager.Instance.CompleteCombo();
            VibrationUtil.Instance.AddHiFreq(0.2F);
        } else if (crashed && !turboActivated)
        {
            revs -= crashRevsPerFrameFactor;
        } else if (turboActivated)
        {
            revs += revsPerFrameFactor * carTurboRevsPerFrameFactor;
        } else if ((acceleration || (!brake && Controls.Instance.GetTurbo())) && (x <= 1))
        {
            revs += revsPerFrameFactor;
        } else if (!brake && Controls.Instance.GetTurbo() && (x > 1) && !turboActivated)
        {
            revs -= revsPerFrameFactor;
        } else if (!brake && (revs < carIdleRevs))
        {
            revs += revsPerFrameFactor;

            revs = Mathf.Clamp(revs, 0F, carIdleRevs);
        } else if (!brake)
        {
            revs -= revsPerFrameFactor;
            revs = Mathf.Clamp(revs, carIdleRevs, int.MaxValue);
        }

        if (revs <= 0)
        {
            crashed = false;
        }

        float calculatedMaxRevs = baseRevs;
        if (turboActivated || (x > 1))
        {
            calculatedMaxRevs = baseRevs * carTurboRevsPerFrameFactor;
            VibrationUtil.Instance.AddHiFreq(0.2F + Mathf.Clamp(x - 1, 0, 1));
        }
        else
        {
            if (touchingCrashedCars.Count > 0)
            {
                calculatedMaxRevs *= touchingCrashedCarMaxRevsFactor;
            }
        }

        float calculatedMinRevs = carIdleRevs;
        if (brake || (revs <= carIdleRevs))
        {
            calculatedMinRevs = 0;
        }

        revs = Mathf.Clamp(revs, calculatedMinRevs, calculatedMaxRevs);

        float multiplier = Mathf.Log(x + 1F)/3F;

        float calculatedMinSpeed = carMinSpeed;
        if (brakeCheck)
        {
            calculatedMinSpeed = 0;
        }

        float speed = Mathf.Clamp(calculatedMinSpeed + (carMaxSpeed * (float)multiplier), calculatedMinSpeed, carMaxSpeed);
        
        if (brake && (speed > carMinSpeed)) {
            soundsManager.PlayBrakingSound();
            GenerateSkidMarks();
        } else
        {
            soundsManager.StopBrakingSound();
        }

        soundsManager.SetSpeed(calculatedMinSpeed, speed, carMaxSpeed);

        SpeedUtils.Instance.SetSpeed(speed);

        return speed;
    }

    private void GenerateSkidMarks()
    {
        foreach (SkidGenerator sg in skidMarkGenerators)
        {
            sg.GenerateSkidMark(transform.rotation);
        }
    }

    internal bool HandleCollision(StuntSubject carController, float hitForce, HashSet<CollisionDirection> dirs, bool isForward, bool isPolice)
    {
        if (hitForce < minHitForce)
        {
            hitForce = minHitForce;
        }

        if (dirs.Contains(CollisionDirection.UP))
        {
            if (isForward)
            {
                HandleCrash(20 * hitForce, StuntType.Ram, PixelheartMath.Lerp(carController.GetTransform().position, transform.position, 0.5F), carController, isPolice);
            }
            else
            {
                HandleCrash(50 * hitForce, StuntType.Ram, PixelheartMath.Lerp(carController.GetTransform().position, transform.position, 0.5F), carController, isPolice);
            }

            return true;
        }
        else if (dirs.Contains(CollisionDirection.DOWN))
        {
            if (Controls.Instance.GetBrakeCheck())
            {
                HandleCrash(hitForce, StuntType.BrakeCheck, PixelheartMath.Lerp(carController.GetTransform().position, transform.position, 0.5F), carController, isPolice);
                return true;
            } else
            {
                HandleCrash(hitForce, StuntType.Null, Vector2.zero, carController, isPolice);
                return false;
            }
        }
        else
        {
            if (dirs.Contains(CollisionDirection.LEFT) || dirs.Contains(CollisionDirection.RIGHT))
            {
                HandleCrash(5 * hitForce, StuntType.Push, PixelheartMath.Lerp(carController.GetTransform().position, transform.position, 0.5F), carController, isPolice);
            }
            else
            {
                HandleCrash(2 * hitForce, StuntType.Nudge, PixelheartMath.Lerp(carController.GetTransform().position, transform.position, 0.5F), carController, isPolice);
            }
            return true;
        }

        return false;
    }

    internal void Crash(PoliceController carController)
    {
        carController.wasHit = true;
        AwesomeInfo.Instance.AddCrashedCar();

        carController.myRigidbody.constraints = RigidbodyConstraints2D.None;
        carController.myRigidbody.drag = carController.linearDrag;

        carController.soundsManager.PlayCrash();
        carController.soundsManager.StopHonk();

        crashed = true;

        carController.smokeEffect.Play();
    }

    // Copy this because I don't have time to refactor properly:(
    internal void Crash(NpcCarController carController)
    {
        carController.wasHit = true;
        AwesomeInfo.Instance.AddCrashedCar();

        carController.myRigidbody.constraints = RigidbodyConstraints2D.None;
        carController.myRigidbody.drag = carController.linearDrag;

        carController.soundsManager.PlayCrash();
        carController.soundsManager.StopHonk();

        crashed = true;

        carController.smokeEffect.Play();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject colObj = collision.gameObject;

        if (colObj.CompareTag("ForwardCar") || colObj.CompareTag("BackwardCar"))
        {
            if (isDestroyed)
            {
                Explode();
                return;
            }

            NpcCarController carController = colObj.GetComponent<NpcCarController>();
            if (carController.wasHit)
            {
                touchingCrashedCars.Add(colObj);
                carController.soundsManager.PlayPuntSound();
                VibrationUtil.Instance.AddHiFreq(0.2F);
                return;
            }

            float hitForce = collision.relativeVelocity.magnitude / 10;

            bool forwardCar = carController.CompareTag("ForwardCar");

            HashSet<CollisionDirection> dirs = playerCollisionDirectionsManager.GetCollisionDirections(colObj);

            HandleCollision(carController, hitForce, dirs, forwardCar, false);

            Crash(carController);
            carController.rammed = true;
        }

        if (colObj.CompareTag("Police") || colObj.CompareTag("PoliceBack"))
        {
            if (isDestroyed)
            {
                Explode();
                return;
            }

            PoliceController carController = colObj.GetComponent<PoliceController>();

            float hitForce = collision.relativeVelocity.magnitude / 10;

            if (carController.wasHit)
            {
                touchingCrashedCars.Add(colObj);
                carController.soundsManager.PlayPuntSound();
                return;
            }

            HashSet<CollisionDirection> dirs = playerCollisionDirectionsManager.GetCollisionDirections(colObj);

            bool forwardCar = carController.isForwardPolice;

            bool shouldCrash = HandleCollision(carController, hitForce, dirs, forwardCar, true);

            if (shouldCrash)
            {
                Crash(carController);
                carController.sirenSource.Stop();
                carController.rammed = true;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collisionStayCrashOffset <= 0)
        {
            collisionStayCrashOffset = 0.5F;
            OnCollisionEnter2D(collision);
        }
        else
        {
            collisionStayCrashOffset -= Time.deltaTime;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        GameObject colObj = collision.gameObject;
        if (colObj.CompareTag("ForwardCar") || colObj.CompareTag("BackwardCar") || colObj.CompareTag("Police") || colObj.CompareTag("PoliceBack"))
        {
            touchingCrashedCars.Remove(colObj);
        }
    }


    private void HandleCrash(float hitForce, StuntType type, Vector2 position, StuntSubject car, bool isPolice)
    {
        if (turboController.IsTurboActivated())
        {
            cameraFollow.shakePower += hitForce * 0.5F;
            hitForce = hitForce * 0.1F;
            StuntManager.Instance.HandleStunt(type, position, car, isPolice);
        } else if (type == StuntType.BrakeCheck || type == StuntType.Nudge || type == StuntType.Push)
        {
            cameraFollow.shakePower += hitForce;
            StuntManager.Instance.HandleStunt(type, position, car, isPolice);
        } else 
        {
            if (isPolice)
            {
                hitForce *= 5;
            }
            cameraFollow.shakePower += hitForce;
            if (IsHardCrash(type))
            {
                StuntManager.Instance.CancelCombo();
            } else
            {
                StuntManager.Instance.CompleteCombo();
            }
            
            passingStuntDetectorScript.HandlePlayerContact();
        }

        AwesomeInfo.Instance.SetNoImpossible();

        energyController.DeductEnergy(hitForce);
    }

    internal void Destroy()
    {

        VibrationUtil.Instance.AddLowFreq(10F);

        if (isDestroyed)
        {
            return;
        }

        isDestroyed = true;

        collisionManagerRigidBody.constraints = RigidbodyConstraints2D.None;
        myRigidBody.constraints = RigidbodyConstraints2D.None;

        myRigidBody.bodyType = RigidbodyType2D.Dynamic;

        smokeEffect.Play();

        StartCoroutine(ExplodeTimeout());
    }

    public void Explode()
    {

        VibrationUtil.Instance.AddLowFreq(10F);
        VibrationUtil.Instance.AddHiFreq(10F);

        if (isExploded)
        {
            return;
        }

        explosionEffect.Play();
        smokeEffect.Stop();
        soundsManager.PlayExplosion();

        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<Collider2D>().enabled = false;

        isExploded = true;

        GenerateCarParts();

        StartCoroutine(ExitTimeout());
    }

    private void GenerateCarParts()
    {
        foreach (GameObject carPartPrefab in carPartPrefabs)
        {
            if (RandomUtil.Chance(50))
            {
                GameObject inst = InstancePoolManager.Instance.CreateInstance(carPartPrefab);

                inst.transform.position = transform.position;

                Rigidbody2D rb = inst.GetComponent<Rigidbody2D>();

                rb.AddForce(new Vector2(UnityEngine.Random.Range(-400, 400), UnityEngine.Random.Range(-400, 400)));
            }
        }
    }

    IEnumerator ExplodeTimeout()
    {
        float startTime = Time.realtimeSinceStartup;

        while ((Time.realtimeSinceStartup - startTime) < 2.5F)
        {
            if (isExploded)
            {
                yield break;
            }

            yield return null;
        }

        Explode();
    }

    IEnumerator ExitTimeout()
    {
        float startTime = Time.realtimeSinceStartup;

        while ((Time.realtimeSinceStartup - startTime) < 1F)
        {
            yield return null;
        }

        SceneManager.LoadScene("Summary");
    }
}

public enum CollisionDirection
{
    LEFT, RIGHT, UP, DOWN, LEFT_UP, RIGHT_UP, LEFT_DOWN, RIGHT_DOWN
}