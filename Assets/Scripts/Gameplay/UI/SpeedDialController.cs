﻿using UnityEngine;

public class SpeedDialController : MonoBehaviour
{
    void Update()
    {
        transform.eulerAngles = Vector3.forward * - (SpeedUtils.Instance.GetSpeedInKmph() - 120F);
    }
}
