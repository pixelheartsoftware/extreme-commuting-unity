﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

#if UNITY_ANDROID
public class ControlsButtonController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public BasicActionType buttonType;

    public float alphaOnEnter = 0.6F;
    public float alphaOnLeave = 0.3F;

    public Controls controls;

    private Image image;

    private bool isPointerOnTheButton = false;
    private bool isJustPressed = false;
    private bool isJustReleased = false;

    private void Start()
    {
#if !UNITY_ANDROID
            Destroy(gameObject);
            return;
#endif

        image = gameObject.GetComponent<Image>();
    }

    private void Update()
    {
        if (isJustPressed)
        {
            controls.SetButton(buttonType, InputAction.PRESS);
            isJustPressed = false;
        }
        else if (isJustReleased) {
            controls.SetButton(buttonType, InputAction.RELEASE);
            isJustReleased = false;
        }
        else if (isPointerOnTheButton)
        {
            controls.SetButton(buttonType, InputAction.STAY);
        }
        else
        {
            controls.SetButton(buttonType, InputAction.NONE);
        }
    }



    public void OnPointerEnter(PointerEventData eventData)
    {
        isPointerOnTheButton = true;
        isJustPressed = true;

        image.color = new Color(image.color.r, image.color.g, image.color.b, alphaOnEnter);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerOnTheButton = false;
        isJustReleased = true;

        image.color = new Color(image.color.r, image.color.g, image.color.b, alphaOnLeave);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnPointerExit(eventData);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnPointerEnter(eventData);
    }
}
#else
public class ControlsButtonController : MonoBehaviour
{
    private void Start()
    {
        Destroy(gameObject);
        return;
    }
}
#endif