﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurboAnimationController : MonoBehaviour
{
    Animator animator;
    TurboController turboController;

    void Awake()
    {
        animator = GetComponent<Animator>();

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        turboController = player.GetComponentInChildren<TurboController>();
    }

    void Update()
    {
        animator.SetBool("TurboActivated", PlayerHolder.Instance.GetTurboController().IsTurboActivated());
    }
}
