﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurboHotAnimationController : MonoBehaviour
{
    Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        animator.SetFloat("TurboLeftPercent", PlayerHolder.Instance.GetTurboController().GetTurboLeftPercent());
        animator.SetBool("TurboActivated", PlayerHolder.Instance.GetTurboController().IsTurboActivated());
        animator.SetBool("TurboCooldown", PlayerHolder.Instance.GetTurboController().IsTurboCooldown());
    }
}
