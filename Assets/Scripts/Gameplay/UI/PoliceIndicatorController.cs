﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoliceIndicatorController : MonoBehaviour
{
    public Image redImage, blueImage;
    public float maxAlpha = 0.5F;

    public bool flipVertically;

    internal bool active;

    private float counter = 0;

    private void Start()
    {
        if (flipVertically)
        {
            redImage.gameObject.transform.localScale = new Vector3(1, -1, 1);
            blueImage.gameObject.transform.localScale = new Vector3(1, -1, 1);
        }
    }

    private Color SetAlpha(Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }

    void Update()
    {
        if (active)
        {
            redImage.color = SetAlpha(redImage.color, Mathf.Abs(Mathf.Sin(counter)) * maxAlpha);
            blueImage.color = SetAlpha(blueImage.color, Mathf.Abs(Mathf.Sin(counter + 90)) * maxAlpha);

            counter += Time.deltaTime * 3;
        } else
        {
            redImage.color = SetAlpha(redImage.color, Mathf.Lerp(redImage.color.a, 0, 0.3F));
            blueImage.color = SetAlpha(blueImage.color, Mathf.Lerp(blueImage.color.a, 0, 0.3F));
        }
    }
}
