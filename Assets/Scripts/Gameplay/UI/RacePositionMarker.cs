﻿using UnityEngine.UI;
using UnityEngine;

public class RacePositionMarker : MonoBehaviour
{
    public RectTransform barRect;

    private RectTransform thisRect;

    public RaceManager raceManager;

    private float initialPosX;

    public void Awake()
    {
        thisRect = GetComponent<RectTransform>();
        initialPosX = thisRect.localPosition.x;
    }

    void Update()
    {
        float step = (raceManager.currentPosition / raceManager.raceLength) * (barRect.rect.width - thisRect.rect.width);

        thisRect.localPosition = new Vector2(initialPosX + step, thisRect.localPosition.y);
    }
}
