﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StuntMarkerGenerator : MonoBehaviour
{
    public GameObject stuntMarkerPrefab;

    public GameObject stuntPointsMarkerPrefab;

    public TextMeshProUGUI potentialAwesomeCounterUI;

    public Camera referenceCamera;

    public void GenerateStuntMarker(string text, Vector2 position)
    {
        GameObject marker = InstancePoolManager.Instance.CreateInstance(stuntMarkerPrefab, potentialAwesomeCounterUI.gameObject);
        marker.GetComponent<StuntMarkerPhysixController>().Reset();

        TextMeshProUGUI textMesh = marker.GetComponent<TextMeshProUGUI>();
        textMesh.text = text;

        marker.transform.position = referenceCamera.WorldToScreenPoint(position);

        Rigidbody2D rigidbody = marker.GetComponent<Rigidbody2D>();

        rigidbody.angularVelocity = UnityEngine.Random.Range(-100, 100);

        rigidbody.AddForce(new Vector2(Random.Range(-1, 1), 1) * Random.Range(0, 500), ForceMode2D.Force);
    }

    public void GenerateStuntPointMarker(string text, bool positive)
    {
        GameObject marker = InstancePoolManager.Instance.CreateInstance(stuntPointsMarkerPrefab, potentialAwesomeCounterUI.gameObject);
        marker.GetComponent<StuntMarkerPhysixController>().Reset();

        marker.transform.position = potentialAwesomeCounterUI.transform.position;

        TextMeshProUGUI textMesh = marker.GetComponent<TextMeshProUGUI>();

        textMesh.SetText(text);
        textMesh.color = positive ? Color.yellow : Color.red;
    }
}
