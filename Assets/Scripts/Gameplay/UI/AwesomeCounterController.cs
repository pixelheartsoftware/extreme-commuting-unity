﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AwesomeCounterController : MonoBehaviour
{
    private int awesomeShown;

    private TextMeshProUGUI awesomeCounterUI;

    void Start()
    {
        awesomeCounterUI = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        int awesomeTotal = AwesomeInfo.Instance.GetPoints();

        if (awesomeTotal == awesomeShown)
        {
            return;
        }

        awesomeShown = (int)Mathf.Ceil(Mathf.Lerp(awesomeShown, awesomeTotal, 10 * Time.deltaTime));

        awesomeShown = Mathf.Clamp(awesomeShown, 0, awesomeTotal);

        awesomeCounterUI.SetText(awesomeShown.ToString());
    }
}
