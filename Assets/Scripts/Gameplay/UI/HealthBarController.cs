﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthBarController : MonoBehaviour
{
    public bool isFirstBar, isLastBar;

    public float energyLeft;
    public float maxEnergy;

    public HealthBarController next;

    private void Awake()
    {
        if (isFirstBar)
        {
            PlayerEnergyController.OnEnergySet += HandleEnergySet;
            PlayerEnergyController.OnEnergyChange += HandleEnergyChange;
        }
    }

    private void OnDestroy()
    {
        PlayerEnergyController.OnEnergySet -= HandleEnergySet;
        PlayerEnergyController.OnEnergyChange -= HandleEnergyChange;
    }

    void HandleEnergySet(float maxEnergy)
    {
        SetEnergy(maxEnergy);
    }

    private void SetEnergy(float maxEnergy)
    {
        this.maxEnergy = maxEnergy / 4;

        if (next != null)
        {
            next.SetEnergy(maxEnergy);
        }

        this.energyLeft = this.maxEnergy;
    }

    void HandleEnergyChange(float energyChange)
    {
        if (energyChange < 0)
        {
            DeductEnergy(-energyChange);
        } else
        {
            ReplenishEnergy(energyChange);
        }
    }

    internal void DeductEnergy(float energyToDeduct)
    {
        float energyToDeductLeft = energyToDeduct;
        if (energyToDeduct > energyLeft)
        {
            energyToDeductLeft = energyToDeduct - energyLeft;
            energyLeft = 0;

            if (isFirstBar)
            {
                SetFirstBar(false);
                if (next != null)
                {
                    next.SetFirstBar(true);
                }
            }
        }
        else
        {
            energyToDeductLeft = 0;
            energyLeft -= energyToDeduct;
        }

        RefreshScale();

        if (isLastBar && energyLeft <= 0)
        {
            PlayerHolder.Instance.GetPlayerMovementController().Destroy();
            return;
        }

        if (next != null)
        {
            next.DeductEnergy(energyToDeductLeft);
        }
    }

    private void SetFirstBar(bool first)
    {
        if (first)
        {
            PlayerEnergyController.OnEnergySet += HandleEnergySet;
            PlayerEnergyController.OnEnergyChange += HandleEnergyChange;
        } else
        {
            PlayerEnergyController.OnEnergySet -= HandleEnergySet;
            PlayerEnergyController.OnEnergyChange -= HandleEnergyChange;
        }
        isFirstBar = first;
    }

    private void RefreshScale()
    {
        float scaleY = energyLeft / maxEnergy;

        transform.localScale = new Vector3(1F, scaleY, 1F);
    }

    internal void ReplenishEnergy(float energy)
    {
        if (energyLeft == 0)
        {
            if (next != null)
            {
                next.ReplenishEnergy(energy);
            }
        }
        if (energyLeft == maxEnergy)
        {
            return;
        }

        energyLeft += energy;

        energyLeft = Mathf.Clamp(energyLeft, 0, maxEnergy);

        RefreshScale();
    }

    internal bool IsEmpty()
    {
        return energyLeft == 0;
    }
}
