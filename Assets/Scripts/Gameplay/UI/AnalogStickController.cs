﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AnalogStickController : MonoBehaviour //, IDragHandler, IEndDragHandler
{
    /*
public Controls controls;

public float maxTilt = 40F;

public float deadzone = 0F;

public bool horizontal, vertical;

public void OnDrag(PointerEventData eventData)
{

Vector2 newPosition = eventData.position;
Vector2 center = transform.parent.transform.position;

float xx = newPosition.x, yy = newPosition.y;

if (!horizontal)
{
xx = center.x;
}

if (!vertical)
{
yy = center.y;
}

newPosition = new Vector2(xx, yy);

float distance = Vector2.Distance(center, newPosition);
Vector2 translationNormalized = (newPosition - center).normalized;

if (distance < maxTilt)
{
transform.position = newPosition;
} else
{
transform.position = center + (translationNormalized * maxTilt);
}

Vector2 virtualInput = translationNormalized * (distance / maxTilt);

float vx = virtualInput.x, vy = virtualInput.y;

if ((Mathf.Abs(vx) < deadzone))
{
vx = 0F;
}

if ((Mathf.Abs(vy) < deadzone))
{
vy = 0F;
}

virtualInput = new Vector2(vx, vy);

if (horizontal)
{
if (vx >= 0.5F)
{
controls.SetButton(BasicActionType.RIGHT, true);
controls.SetButton(BasicActionType.LEFT, false);
}
if (vx <= -0.5F)
{
controls.SetButton(BasicActionType.RIGHT, false);
controls.SetButton(BasicActionType.LEFT, true);
}
}

if (vertical)
{
if (vy >= 0.5F)
{
controls.SetButton(BasicActionType.ACCELERATION, true);
controls.SetButton(BasicActionType.BRAKE, false);
}
if (vy <= -0.5F)
{
controls.SetButton(BasicActionType.ACCELERATION, false);
controls.SetButton(BasicActionType.BRAKE, true);
}
}

}

public void OnEndDrag(PointerEventData eventData)
{

transform.localPosition = Vector2.zero;

if (horizontal)
{
controls.SetButton(BasicActionType.RIGHT, false);
controls.SetButton(BasicActionType.LEFT, false);
}

if (vertical)
{
controls.SetButton(BasicActionType.ACCELERATION, false);
controls.SetButton(BasicActionType.BRAKE, false);
}

}
*/
}
