﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StuntMarkerPhysixController : MonoBehaviour
{
    private Rigidbody2D myRigidbody;
    private TextMeshProUGUI textMesh;

    public float maxSizeMultiplier = 1.5F;

    private float fontSize;

    internal float sizeMultiplier = 1;

    private bool growing = true, shrinking = false;

    private float alpha = 1;

    internal void Reset()
    {
        transform.localRotation = Quaternion.identity;

        growing = true;
        shrinking = false;
        sizeMultiplier = 1;
        alpha = 1;
    }

    void Start()
    {
        myRigidbody = gameObject.GetComponent<Rigidbody2D>();

        textMesh = GetComponent<TextMeshProUGUI>();

        fontSize = textMesh.fontSize;
    }

    void Update()
    {
        myRigidbody.AddForce(Vector2.down * 1000, ForceMode2D.Force);

        if (growing)
        {
            sizeMultiplier = Mathf.Lerp(sizeMultiplier, maxSizeMultiplier, 0.1F);
            textMesh.fontSize = fontSize * sizeMultiplier;

            if (maxSizeMultiplier - sizeMultiplier < 0.1)
            {
                growing = false;
                shrinking = true;
            }
        }
        else if (shrinking)
        {
            sizeMultiplier = Mathf.Lerp(sizeMultiplier, 1, 0.1F);
            textMesh.fontSize = fontSize * sizeMultiplier;

            if (sizeMultiplier - 1 < 0.1)
            {
                shrinking = false;
            }
        }

        alpha = Mathf.Lerp(alpha, 0, 0.01F);
        textMesh.alpha = alpha;

        if (alpha < 0.05F)
        {
            InstancePoolManager.Instance.DestroyInstance(gameObject);
        }
    }
}
