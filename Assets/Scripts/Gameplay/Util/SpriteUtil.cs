﻿using UnityEngine;

public class SpriteUtil : MonoBehaviour
{

    public static Vector3 GetImageSize(GameObject obj)
    {
        float pixelsPerUnit = obj.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        Rect spriteRect = obj.GetComponent<SpriteRenderer>().sprite.rect;

        return new Vector2(spriteRect.width / pixelsPerUnit, spriteRect.height / pixelsPerUnit);
    }

    public static float GetPixelSize(GameObject obj)
    {
        return 1 / obj.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
    }

    public static void PlaceAbove(GameObject top, GameObject bottom, float x)
    {
        float bottomPosY = bottom.transform.position.y;

        float pixelSize = GetPixelSize(top);

        float translation = (0.5F * SpriteUtil.GetImageSize(bottom).y) + (0.5F * SpriteUtil.GetImageSize(top).y);

        translation = translation - (translation % pixelSize);

        top.transform.position = new Vector2(x, bottomPosY + translation);
    }

    public static void PlaceBelow(GameObject below, GameObject above, float x)
    {
        float abovePosY = above.transform.position.y;

        float pixelSize = GetPixelSize(below);

        float translation = (0.5F * SpriteUtil.GetImageSize(above).y) + (0.5F * SpriteUtil.GetImageSize(below).y);

        translation = translation - (translation % pixelSize);

        below.transform.position = new Vector2(x, abovePosY - translation);
    }
}
