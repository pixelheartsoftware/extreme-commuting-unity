﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T myInstance;

    protected static Singleton<T> _Instance
    {
        get
        {
            if (!myInstance)
            {
                T[] instances = GameObject.FindObjectsOfType(typeof(T)) as T[];
                if (instances != null)
                {
                    if (instances.Length == 1)
                    {
                        myInstance = instances[0];
                        return myInstance;
                    }
                    else 
                    if (instances.Length > 1)
                    {
                        Debug.LogError("More than one " + typeof(T).Name);
                    }
                }
                GameObject myGameObject = new GameObject(typeof(T).Name, typeof(T));
                myInstance = myGameObject.GetComponent<T>();
                DontDestroyOnLoad(myInstance.gameObject);
            }
            return myInstance;
        }
        set
        {
            myInstance = value as T;
        }
    }
}