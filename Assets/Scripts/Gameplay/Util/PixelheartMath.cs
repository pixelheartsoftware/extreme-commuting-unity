﻿using System;
using UnityEngine;

public class PixelheartMath : MonoBehaviour
{

    public static float RoundPlaces(float number, int places)
    {
        int multiplier = 10 ^ places;

        return Mathf.Round(number * multiplier) / multiplier;
    }

    public static Vector3 Lerp2D(Vector3 from, Vector3 to, float between, float z)
    {
        Vector2 lerped = Lerp((Vector2)from, (Vector2)to, between);

        return (new Vector3(lerped.x, lerped.y, z));
    }

    public static Vector2 Lerp(Vector2 from, Vector2 to, float between)
    {
        return Vector2.Lerp(from, to, between);
    }
}
