﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InstancePoolManager : Singleton<InstancePoolManager>
{
    public static Dictionary<string, int> suggestedLevels = new Dictionary<string, int>();

    private static Dictionary<String, List<GameObject>> activeInstances = new Dictionary<String, List<GameObject>>();

    private static Dictionary<String, List<GameObject>> inactiveInstances = new Dictionary<String, List<GameObject>>();

    public List<String> activeInstancesCount = new List<String>();

    public List<String> inactiveInstancesCount = new List<String>();

    public static InstancePoolManager Instance
    {
        get { return ((InstancePoolManager)_Instance); }
        set { _Instance = value; }
    }

    public void Start()
    {
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    private void OnSceneUnloaded(Scene current)
    {
        Reset();
    }

    public GameObject CreateInstance(GameObject prefab, GameObject parent)
    {
        List<GameObject> inactiveObjects = GetInactiveInstances(prefab.tag);

        if (inactiveObjects.Count > 0)
        {
            GameObject selected = inactiveObjects[0];

            inactiveObjects.Remove(selected);

            SetActiveWithChildren(selected, true);

            GetActiveInstances(prefab.tag).Add(selected);

            return selected;
        } else
        {
            GameObject instance = (parent != null) ? Instantiate(prefab, Vector2.zero, Quaternion.identity, parent.transform) : Instantiate(prefab);

            GetActiveInstances(prefab.tag).Add(instance);

            return instance;
        }
    }

    public GameObject CreateInstance(GameObject prefab)
    {
        return CreateInstance(prefab, null);
    }

    public void DestroyInstance(GameObject instance)
    {
        List<GameObject> activeObjects = GetActiveInstances(instance.tag);

        if (activeObjects.Count == 0 || !activeObjects.Contains(instance))
        {
            Destroy(instance);
            Debug.LogError("Destroying an instance that was never created with this manager: " + instance);
            return;
        }

        activeObjects.Remove(instance);

        SetActiveWithChildren(instance, false);

        List<GameObject> inactive = GetInactiveInstances(instance.tag);

        if (suggestedLevels.ContainsKey(instance.tag) && (inactive.Count > suggestedLevels[instance.tag]))
        {
            Destroy(instance);
        } else
        {
            inactive.Add(instance);
        }
    }

    private void SetActiveWithChildren(GameObject instance, bool active)
    {
        for (int i = 0; i < instance.transform.childCount; i++)
        {
            instance.transform.GetChild(i).gameObject.SetActive(active);
        }
        instance.SetActive(active);
    }

    private List<GameObject> GetActiveInstances(string tag)
    {
        if (!activeInstances.ContainsKey(tag))
        {
            activeInstances.Add(tag, new List<GameObject>());
        }

        return activeInstances[tag];
    }

    private List<GameObject> GetInactiveInstances(string tag)
    {
        if (!inactiveInstances.ContainsKey(tag))
        { 
            inactiveInstances.Add(tag, new List<GameObject>());
        }

        return inactiveInstances[tag];
    }

    public void Reset()
    {
        List<GameObject> allActiveInstances = new List<GameObject>();
        foreach (KeyValuePair<string, List<GameObject>> entry in activeInstances)
        {
            foreach (GameObject inst in entry.Value)
            {
                allActiveInstances.Add(inst);
            }
        }
        foreach (GameObject inst in allActiveInstances)
        {
            Destroy(inst);
        }
        activeInstances.Clear();

        List<GameObject> allInactiveInstances = new List<GameObject>();
        foreach (KeyValuePair<string, List<GameObject>> entry in inactiveInstances)
        {
            foreach (GameObject inst in entry.Value)
            {
                allInactiveInstances.Add(inst);
            }
        }
        foreach (GameObject inst in allInactiveInstances)
        {
            Destroy(inst);
        }
        inactiveInstances.Clear();
    }
}
