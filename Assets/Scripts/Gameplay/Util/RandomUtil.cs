﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomUtil : MonoBehaviour
{

    public static bool Chance(int percent)
    {
        return UnityEngine.Random.value < (percent / 100F);
    }

    internal static T RandomElement<T>(List<T> sprites)
    {
        return sprites[UnityEngine.Random.Range(0, sprites.Count)];
    }
}
