﻿using UnityEngine;

public class SpeedUtils : Singleton<SpeedUtils>
{
    private static float METERS_PER_UNIT = 6;

    private float currentSpeed;

    public static SpeedUtils Instance
    {
        get { return ((SpeedUtils)_Instance); }
        set { _Instance = value; }
    }

    public float GetSpeedInMetersPerSecond()
    {
        return METERS_PER_UNIT * currentSpeed;
    }

    public float GetSpeedInKmph()
    {
        float speed = GetSpeedInMetersPerSecond();

        return speed * 3.6F;
    }

    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    }

    public float GetSpeed()
    {
        return currentSpeed;
    }
}
