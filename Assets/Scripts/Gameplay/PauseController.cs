﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    private bool paused = false;
    private float prevTimeScale = 1;

    public Image curtainImage;
    public TextMeshProUGUI text;

    public bool pressed;

    private List<AudioSource> pausedAudioSources = new List<AudioSource>();

    private void Start()
    {
        curtainImage.enabled = false;
        text.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetAxisRaw("PadBack") >= 0.5F)
        {
            if (!pressed)
            {
                if (paused)
                {
                    RaceManager.Instance.forceQuit = true;
                    prevTimeScale = 1;
                }
                else
                {
                    if (PlayerHolder.Instance.GetPlayerMovementController().isDestroyed)
                    {
                        return;
                    }
                    pressed = true;
                    paused = true;
                    prevTimeScale = Time.timeScale;
                    Time.timeScale = 0;
                    curtainImage.enabled = true;
                    text.enabled = true;

                    SwitchSounds(paused);
                }
            }
        }
        else if (paused &&
            (Input.anyKeyDown ||
            Input.GetAxisRaw("PadA") > 0.2F ||
            Input.GetAxisRaw("PadB") > 0.2F ||
            Input.GetAxisRaw("PadHorizontal") > 0.2F ||
            Input.GetAxisRaw("PadVertical") > 0.2F ||
            Input.GetAxisRaw("PadBrake") > 0.2F ||
            Input.GetAxisRaw("PadAccelerate") > 0.2F ||
            Input.GetAxisRaw("PadBumperLeft") > 0.2F ||
            Input.GetAxisRaw("PadBumperRight") > 0.2F ||
            Input.GetAxisRaw("PadMenu") > 0.2F))
        {
            if (!pressed)
            {
                paused = false;
                Time.timeScale = prevTimeScale;
                curtainImage.enabled = false;
                text.enabled = false;

                Cursor.visible = false;

                SwitchSounds(paused);
            }
        } else
        {
            pressed = false;
        }


    }

    private void SwitchSounds(bool paused)
    {
        if (paused)
        {
            AudioSource[] components = GameObject.FindObjectsOfType<AudioSource>();

            foreach (AudioSource auso in components)
            {
                if (auso.isPlaying)
                {
                    auso.Pause();
                    pausedAudioSources.Add(auso);
                }
            }
        }
        else
        {
            foreach (AudioSource auso in pausedAudioSources)
            {
                auso.UnPause();
            }

            pausedAudioSources.Clear();
        }
    }
}
