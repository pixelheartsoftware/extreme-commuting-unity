﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopCars : MonoBehaviour
{
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "ForwardCar" || collision.tag == "BackwardCar")
        {
            NpcCarController carController = collision.GetComponent<NpcCarController>();

            if (carController.wasHit)
            {
                return;
            }

            Rigidbody2D rigidbody = collision.GetComponent<NpcCarController>().myRigidbody;
            rigidbody.velocity = new Vector2(0F, Mathf.Lerp(rigidbody.velocity.y, 0, 0.2F));
        }
    }
}
