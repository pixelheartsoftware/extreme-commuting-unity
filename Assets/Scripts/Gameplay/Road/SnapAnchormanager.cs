﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapAnchormanager : MonoBehaviour
{
    public float width = 0.2F;

    private float minX, maxX;

    private void Awake()
    {
        minX = transform.position.x - 0.5F * width;
        maxX = transform.position.x + 0.5F * width;
    }

    void FixedUpdate()
    {
        Transform pt = PlayerHolder.Instance.GetPlayerTransform();

        if (minX < pt.position.x && pt.position.x < maxX)
        {
            PlayerHolder.Instance.GetPlayerMovementController().snapAnchor = this;
        }
        else
        {
            PlayerMovementController pmc = PlayerHolder.Instance.GetPlayerMovementController();
            if (pmc.snapAnchor == this)
            {
                pmc.snapAnchor = null;
            }
        }
    }
}
