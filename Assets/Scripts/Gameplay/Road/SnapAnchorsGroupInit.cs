﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapAnchorsGroupInit : MonoBehaviour
{
    private void FixedUpdate()
    {
        Vector2 playerPos = PlayerHolder.Instance.GetPlayerTransform().position;
        transform.position = new Vector2(transform.position.x, playerPos.y);
    }
}
