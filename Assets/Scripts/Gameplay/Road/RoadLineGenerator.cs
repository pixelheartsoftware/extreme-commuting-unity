﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadLineGenerator : MonoBehaviour
{
    List<GameObject> instances = new List<GameObject>();

    GameObject topObject;
    GameObject bottomObject;

    public List<GameObject> linePrefabs;

    float bottomLine = 9;

    public float xOffset = 0.2F;

    float playerVerticalOffset;

    private void Start()
    {
        playerVerticalOffset = PlayerHolder.Instance.GetPlayerTransform().position.y - transform.position.y;

        for (int i = 0; i < 20; i++)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(linePrefabs[UnityEngine.Random.Range(0, linePrefabs.Count)]);

            if (topObject == null)
            {
                instance.transform.position = new Vector2(transform.position.x + xOffset, -bottomLine);
                bottomObject = instance;
            }
            else
            {
                SpriteUtil.PlaceAbove(instance, topObject, transform.position.x + xOffset);
            }
            topObject = instance;
            instances.Add(instance);
        }

        gameObject.SetActive(true);
    }

    void Update()
    {
        if (bottomObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - bottomLine)
        {
            instances.Remove(bottomObject);

            InstancePoolManager.Instance.DestroyInstance(bottomObject);

            bottomObject = instances[0];
        }

        if (topObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(linePrefabs[UnityEngine.Random.Range(0, linePrefabs.Count)]);
            SpriteUtil.PlaceAbove(instance, topObject, transform.position.x + xOffset);
            instances.Add(instance);
            topObject = instance;
        }
    }
}
