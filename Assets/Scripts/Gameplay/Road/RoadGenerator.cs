﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour
{
    List<GameObject> instances = new List<GameObject>();

    GameObject topObject;
    GameObject bottomObject;

    public GameObject roadPrefab;

    float bottomLine = 9;

    float playerVerticalOffset;

    private void Start()
    {
        playerVerticalOffset = PlayerHolder.Instance.GetPlayerTransform().position.y - transform.position.y;

        for (int i = 0; i < 20; i++)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(roadPrefab);

            if (topObject == null)
            {
                instance.transform.position = new Vector2(transform.position.x, -bottomLine);
                bottomObject = instance;
            }
            else
            {
                SpriteUtil.PlaceAbove(instance, topObject, transform.position.x);
            }
            topObject = instance;
            instances.Add(instance);
        }

        PlayerMovementController playerMovementController = PlayerHolder.Instance.GetPlayer().GetComponentInChildren<PlayerMovementController>();

        playerMovementController.roadCenterX = transform.position.x;
        playerMovementController.roadAnchors.Add(transform.position.x - 0.18F);
        playerMovementController.roadAnchors.Add(transform.position.x + 0.18F);

        gameObject.SetActive(true);
    }

        void Update()
    {
        if (bottomObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - bottomLine)
        {
            instances.Remove(bottomObject);

            InstancePoolManager.Instance.DestroyInstance(bottomObject);

            bottomObject = instances[0];
        }

        if (topObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - playerVerticalOffset)
        {
            GameObject instance = InstancePoolManager.Instance.CreateInstance(roadPrefab);
            SpriteUtil.PlaceAbove(instance, topObject, transform.position.x);
            instances.Add(instance);
            topObject = instance;
        }
    }

}
