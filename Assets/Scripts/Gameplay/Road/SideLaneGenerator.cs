﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideLaneGenerator : MonoBehaviour
{
    List<GameObject> instances = new List<GameObject>();

    GameObject topObject;

    public GameObject lanePrefab;
    public GameObject laneStartPrefab;
    public GameObject laneEndPrefab;

    public int minRoadLength = 5;
    internal int currentElementsLeftToAdd = 0;
    internal int currentElementsAlreadyAdded = 0;

    public float minRoadPause = 3;
    float currentRoadPause = 0;

    float removeY = 9;

    public float xOffset = 0.2F;

    float playerVerticalOffset;

    bool newRoad;

    bool isPlayerOnLeftLane = false;

    State roadOnState = new RoadOnState();
    State roadOffState = new RoadOffState();

    State currentState;

    private void Start()
    {
        playerVerticalOffset = PlayerHolder.Instance.GetPlayerTransform().position.y - transform.position.y;

        currentState = roadOffState;

        PlayerMovementController playerMovementController = PlayerHolder.Instance.GetPlayer().GetComponentInChildren<PlayerMovementController>();
        playerMovementController.roadAnchors.Add(transform.position.x + xOffset);
    }

    void Update()
    {
        if (instances.Count > 0)
        {
            GameObject bottomObject = instances[0];

            if (bottomObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - removeY)
            {
                instances.Remove(bottomObject);

                InstancePoolManager.Instance.DestroyInstance(bottomObject);
            }
        }

        if (PlayerHolder.Instance.GetPlayerTransform().position.x < transform.position.x)
        {
            if (isPlayerOnLeftLane)
            {
                AwesomeInfo.Instance.AddLeftLaneTotalTime(Time.deltaTime);
            }
            isPlayerOnLeftLane = true;
        } else
        {
            isPlayerOnLeftLane = false;
        }

        currentState.Update(this);
    }

    abstract class State
    {
        public abstract void Update(SideLaneGenerator generator);
    }

    private class RoadOffState : State
    {
        public override void Update(SideLaneGenerator generator)
        {
            generator.currentRoadPause -= Time.deltaTime;

            if (generator.currentRoadPause <= 0)
            {
                generator.currentState = generator.roadOnState;

                generator.currentElementsLeftToAdd = generator.minRoadLength + UnityEngine.Random.Range(0, 20);
                generator.currentElementsAlreadyAdded = 0;

                generator.newRoad = true;
                return;
            }
        }
    }

    class RoadOnState : State
    {
        public override void Update(SideLaneGenerator generator)
        {
            if (!(generator.topObject == null || generator.topObject.transform.position.y < PlayerHolder.Instance.GetPlayerTransform().position.y - generator.playerVerticalOffset))
            {
                return;
            }

            GameObject instance = InstancePoolManager.Instance.CreateInstance(generator.lanePrefab);
            if ((generator.topObject == null) || generator.newRoad)
            {
                GameObject laneStartInstance = InstancePoolManager.Instance.CreateInstance(generator.laneStartPrefab);
                generator.instances.Add(laneStartInstance);

                laneStartInstance.transform.position = new Vector2(generator.transform.position.x + generator.xOffset, PlayerHolder.Instance.GetPlayerTransform().position.y - generator.playerVerticalOffset);

                SpriteUtil.PlaceAbove(instance, laneStartInstance, generator.transform.position.x + generator.xOffset);
                generator.instances.Add(instance);
            }
            else
            {
                SpriteUtil.PlaceAbove(instance, generator.topObject, generator.transform.position.x + generator.xOffset);
                generator.instances.Add(instance);
            }

            generator.topObject = instance;
            generator.newRoad = false;

            generator.currentElementsLeftToAdd -= 1;
            generator.currentElementsAlreadyAdded++;

            if (generator.currentElementsLeftToAdd == 0)
            {
                generator.currentState = generator.roadOffState;

                generator.currentRoadPause = generator.minRoadPause + UnityEngine.Random.Range(0, 5);

                GameObject laneEndInstance = InstancePoolManager.Instance.CreateInstance(generator.laneEndPrefab);
                SpriteUtil.PlaceAbove(laneEndInstance, instance, generator.transform.position.x + generator.xOffset);
                generator.instances.Add(laneEndInstance);

                return;
            }
        }
    }
}


