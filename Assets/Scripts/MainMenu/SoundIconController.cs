﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SoundIconController : MonoBehaviour
{
    public Sprite enabledSprite, disabledSprite;

    private Image myImage;

    private void Awake()
    {
        myImage = GetComponent<Image>();

        bool soundEnabled = !SoundsManager.SoundsDisabled();
        SetSprite(soundEnabled);

        MusicManager.Instance.SetMusic();
    }

    private void SetSprite(bool soundEnabled)
    {
        if (!soundEnabled)
        {
            myImage.sprite = disabledSprite;
        }
        else
        {
            myImage.sprite = enabledSprite;
        }
    }

    public void OnClick()
    {
        bool soundEnabled = !SoundsManager.SoundsDisabled();

        SoundsManager.SetSounds(!soundEnabled);

        MusicManager.Instance.SetMusic();

        SetSprite(!soundEnabled);
    }
}
