﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleController : MonoBehaviour
{
    private Vector2 targetPosition;

    public float hiddenPosX;
    public float speed = 2;

    void Start()
    {
        targetPosition = transform.position;
        transform.position = new Vector2(transform.position.x + hiddenPosX, transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        float newX = Mathf.Lerp(transform.position.x, targetPosition.x, Time.deltaTime * speed);

        transform.position = new Vector2(newX, transform.position.y);
    }
}
