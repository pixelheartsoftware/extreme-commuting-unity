﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAnimationController : MonoBehaviour
{
    public Animator mirrorAnimator;
    public Animator linesAnimator;

    public RectTransform wheel;

    public float minSpeed = 1;
    public float maxSpeed = 2;

    internal bool isUp = false;

    public List<RectTransform> backgrounds;
    public float backgroundUpMovement = 1;

    public Rigidbody2D pine;

    private Dictionary<RectTransform, float> backgroundDownYPositions = new Dictionary<RectTransform, float>();

    float upDownPause = 0;

    private float speed = 1;

    float wheelTargetRotation = 0;
    float wheelRotation = 0;
    float wheelPause = 0;

    public RectTransform speedDial;
    private float speedDialAngle = 0;

    public List<RectTransform> dials;
    private Dictionary<RectTransform, float> dialTargetRotations = new Dictionary<RectTransform, float>();
    private Dictionary<RectTransform, float> dialCurrentRotations = new Dictionary<RectTransform, float>();
    private Dictionary<RectTransform, float> dialPauses = new Dictionary<RectTransform, float>();

    private void Start()
    {
        foreach (RectTransform background in backgrounds)
        {
            backgroundDownYPositions.Add(background, background.transform.position.y);
        }

        foreach (RectTransform dial in dials)
        {
            dialPauses[dial] = 0;
            dialTargetRotations[dial] = 0;
            dialCurrentRotations[dial] = 0;
        }
    }

    void Update()
    {
        if (upDownPause > 0)
        {
            upDownPause -= Time.deltaTime;
        }
        else
        {
            isUp = !isUp;
            mirrorAnimator.SetBool("isUp", isUp);
            if (!isUp)
            {
                upDownPause = UnityEngine.Random.Range(0.1F, 0.3F);
                pine.AddForce(Vector2.down * 2, ForceMode2D.Force);
            } else
            {
                upDownPause = UnityEngine.Random.Range(0.5F, 3);
                pine.AddForce(Vector2.up * 2, ForceMode2D.Force);
            }
            
        }

        UpdateBackgrounds();

        UpdateSpeed();

        UpdateWheel();

        UpdateDials();
    }

    private void UpdateDials()
    {
        foreach (RectTransform dial in dials)
        {
            float dialPause = dialPauses[dial];
            if (dialPause > 0)
            {
                dialPauses[dial] -= Time.deltaTime;
            }
            else
            {
                dialPauses[dial] = UnityEngine.Random.Range(0.2F, 1);
                dialTargetRotations[dial] = UnityEngine.Random.Range(-30F, 30F);
            }

            dialCurrentRotations[dial] = Mathf.Lerp(dialCurrentRotations[dial], dialTargetRotations[dial], Time.deltaTime);

            dial.rotation = Quaternion.Euler(new Vector3(0, 0, dialCurrentRotations[dial]));
        }
        speedDialAngle = Mathf.Lerp(speedDialAngle, (maxSpeed - speed) * 50 - 100, Time.deltaTime * 10);
        speedDial.rotation = Quaternion.Euler(new Vector3(0, 0, speedDialAngle));
    }

    private void UpdateBackgrounds()
    {
        foreach (RectTransform background in backgrounds)
        {
            float targetY;
            if (!isUp)
            {
                targetY = Mathf.Lerp(background.position.y, backgroundDownYPositions[background], Time.deltaTime * 10);
            }
            else
            {
                targetY = Mathf.Lerp(background.position.y, backgroundDownYPositions[background] + backgroundUpMovement, Time.deltaTime * 10);
            }

            background.position = new Vector3(background.position.x, targetY, 0);
        }
    }

    private void UpdateWheel()
    {
        if (wheelPause > 0)
        {
            wheelPause -= Time.deltaTime;
        } else
        {
            wheelPause = UnityEngine.Random.Range(0.2F, 2);
            wheelTargetRotation = UnityEngine.Random.Range(-10, 10);
        }

        wheelRotation = Mathf.Lerp(wheelRotation, wheelTargetRotation, Time.deltaTime);

        wheel.rotation = Quaternion.Euler(new Vector3(0, 0, wheelRotation));
    }

    private void UpdateSpeed()
    {
        speed = Mathf.Clamp(speed + UnityEngine.Random.Range(-0.3F, 0.3F), minSpeed, maxSpeed);
        mirrorAnimator.speed = speed;
        linesAnimator.speed = speed;
    }
}
