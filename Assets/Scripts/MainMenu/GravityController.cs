﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GravityController : MonoBehaviour
{
    void Start()
    {
        Physics2D.gravity = new Vector2(0, -9.81F);

        SceneManager.sceneUnloaded += OnSceneUnload;
    }

    void OnSceneUnload(Scene scene)
    {
        Physics2D.gravity = Vector2.zero;
    }
}
