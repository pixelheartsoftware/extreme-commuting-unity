﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchAreaController : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    public float itemsDistance = 80;

    public List<MenuItemController> menuItems;

    private MenuItemController currentSelection;

    public Image selectionLabelImage;

    public MainMenuSoundManager soundManager;

    public ButtonController audioButton;

    private static string lastSelectedItemName = null;

    public void OnDrag(PointerEventData eventData)
    {
        InitSelection(eventData.position);
    }

    public void InitSelection(Vector2 position)
    {
        currentSelection.transform.position = new Vector2(position.x, currentSelection.transform.position.y);

        SetPositions();
    }

    private MenuItemController FindNearestItem(Vector2 position)
    {
        float minDist = float.MaxValue;
        MenuItemController selected = null;

        foreach (MenuItemController item in menuItems)
        {
            float dist = Vector2.Distance(item.transform.position, transform.position);

            if (dist < minDist)
            {
                minDist = dist;
                selected = item;
            }
        }

        return selected;
    }

    internal void OnClick(MenuItemController item)
    {
        if (currentSelection == item)
        {
            lastSelectedItemName = item.gameObject.name;
            item.DoAction();
        } else
        {
            SetSelection(item);
            SetPositions();
        }
    }

    private void SetPositions()
    {
        int curPos = menuItems.IndexOf(currentSelection);

        for (int i = 0; i < menuItems.Count; i++)
        {
            menuItems[i].transform.position = new Vector3(Camera.main.pixelWidth * itemsDistance * (i - curPos), 0, 0) + currentSelection.transform.position;

            float newAlpha = Mathf.Abs(menuItems[i].transform.position.x - transform.position.x) / (Camera.main.pixelWidth * itemsDistance * 1.5F);
            newAlpha = 1 - Mathf.Clamp(newAlpha, 0, 1);

            menuItems[i].image.color = new Color(menuItems[i].image.color.r, menuItems[i].image.color.g, menuItems[i].image.color.b, newAlpha);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        CommitSelection();
    }

    public void CommitSelection()
    {
        MenuItemController newSelection = currentSelection;

        newSelection = FindNearestItem(transform.position);

        SetSelection(newSelection);
        SetPositions();

        soundManager.PlayClickSound();
    }

    void Start()
    {
        if (lastSelectedItemName == null)
        {
            SetSelection(menuItems[0]);
            menuItems[0].GetComponent<ButtonController>().selectController.SelectButton(menuItems[0].gameObject, SelectionMethod.NULL);
        } else
        {
            foreach (MenuItemController item in menuItems)
            {
                if (item.gameObject.name == lastSelectedItemName)
                {
                    SetSelection(item);
                    item.GetComponent<ButtonController>().selectController.SelectButton(item.gameObject, SelectionMethod.NULL);
                    break;
                }
            }
        }

        SetPositions();

        MenuItemController previous = null;
        foreach (MenuItemController item in menuItems)
        {
            if (previous != null)
            {
                ButtonController prevButton = previous.GetComponent<ButtonController>();
                ButtonController button = item.GetComponent<ButtonController>();

                prevButton.right = button.gameObject;
                button.left = prevButton.gameObject;
            } else
            {
                ButtonController button = item.GetComponent<ButtonController>();

                audioButton.right = button.gameObject;
                button.left = audioButton.gameObject;
            }

            previous = item;

            item.touchAreaController = this;
        }
    }

    internal void SetSelection(MenuItemController menuItemController)
    {
        currentSelection = menuItemController;

        menuItemController.transform.position = transform.position;

        selectionLabelImage.sprite = currentSelection.labelSprite;
        selectionLabelImage.SetNativeSize();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        currentSelection = FindNearestItem(eventData.position);
    }
}
