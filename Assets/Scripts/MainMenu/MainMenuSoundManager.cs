﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSoundManager : SoundsManager
{
    public AudioClip menuItemSwitchSound;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayMenuItemSwitchSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        audioSource.PlayOneShot(menuItemSwitchSound);
    }
}
