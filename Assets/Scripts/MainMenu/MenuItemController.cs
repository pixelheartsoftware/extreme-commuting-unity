﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuItemController : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public Sprite labelSprite;

    internal Image image;

    public TouchAreaController touchAreaController;

    public UnityEvent onClickAction;

    private bool dragging;

    private void Awake()
    {
        image = GetComponent<Image>();

        GetComponent<ButtonController>().onSelect.AddListener(Select);
    }

    public void Select(SelectionMethod method)
    {
        if (method == SelectionMethod.KEYBOARD)
        {
            touchAreaController.SetSelection(this);
            touchAreaController.InitSelection(transform.position);
            touchAreaController.CommitSelection();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        dragging = true;
        touchAreaController.SetSelection(this);
        touchAreaController.OnDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        dragging = false;
        touchAreaController.OnEndDrag(eventData);
    }

    public void DoAction()
    {
        onClickAction.Invoke();
    }

    public void OnClick()
    {
        if (!dragging)
        {
            touchAreaController.OnClick(this);
        }
    }
}
