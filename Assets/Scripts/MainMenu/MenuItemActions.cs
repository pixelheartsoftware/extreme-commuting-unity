﻿using GameJolt.API;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_ANDROID
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
#endif

public class MenuItemActions : MonoBehaviour
{
    public GameJoltAPI GJApi;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetAxis("PadBack") > 0)
        {
            Application.Quit();
            return;
        }
    }

#if UNITY_ANDROID
    private void Start()
    {
        PlayGamesPlatform.Activate();

        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success, string errors) => {
                    Debug.Log("Auth ok: " + success + "Auth error: " + errors);
            });
        }
    }
#endif


    public void Play()
    {
        SceneManager.LoadScene("CarSelection");
    }

    public void Leaderboards()
    {

#if !UNITY_ANDROID

        if (GameJoltUtil.IsGameJoltEnabled())
        {
            GameJoltUtil.ShowLeaderboards();
        } else
        {
            SceneManager.LoadScene("HallOfFame");
        }

#else
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success, string errors) => {
                if (success)
                {
                    Social.ShowLeaderboardUI();
                } else
                {
                    Debug.Log("Auth error: " + errors);
                }
            });
        } else
        {
            Social.ShowLeaderboardUI();
        }
#endif
    }

    public void Achievements()
    {
#if !UNITY_ANDROID
        
        if (GameJoltUtil.IsGameJoltEnabled())
        {
            GameJoltUtil.ShowAchievements();
        }
        else
        {
            SceneManager.LoadScene("Achievements");
        }
#else
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success) => {
                if (success)
                {
                    Social.ShowAchievementsUI();
                }
                else
                {
                    Debug.Log("Auth error");
                }
            });
        } else
        {
            Social.ShowAchievementsUI();
        }
#endif

    }

    public void Authors()
    {
        SceneManager.LoadScene("Authors");
    }

    public void Settings()
    {
        //TODO
        //SceneManager.LoadScene("Settings");
    }
}
