﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PineController : MonoBehaviour, IPointerEnterHandler
{
    private Rigidbody2D myRigidbody;

    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Vector2 force = (Vector2)transform.position - eventData.position;
        myRigidbody.AddForceAtPosition(force * 50F, eventData.position, ForceMode2D.Force);
    }
}
