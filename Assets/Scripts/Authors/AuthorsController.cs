﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AuthorsController : MonoBehaviour
{
    public Image logoImage;

    public float lineDistance = 0.2F;
    public float labelToTextDistance = 0.1F;

    public float textSpeed = 0.5F;
    private Vector2 textSpeedVector;

    public GameObject labelPrefab;
    public GameObject textPrefab;
    public GameObject smallLabelPrefab;

    public Canvas canvas;

    public string[] lines;

    public string[] resources;

    public string[] companyNameLines;

    private List<GameObject> createdLines = new List<GameObject>();

    private Vector2 lineDistanceVector;
    private Vector2 labelToTextDistanceVector;

    public RawImage curtain;

    public GameObject parentObject;

    private bool stopped;

    private Coroutine fadeout;

    void Start()
    {
        lineDistanceVector = new Vector2(0, lineDistance);
        labelToTextDistanceVector = new Vector2(0, labelToTextDistance);
        textSpeedVector = new Vector2(0, textSpeed);
        Vector2 pos = canvas.transform.position;
        foreach (string line in lines)
        {
            if (line == null || line == "")
            {
                pos -= lineDistanceVector;
                continue;
            }
            AddLine(line, pos);
            pos -= lineDistanceVector;
        }

        pos -= lineDistanceVector;
        CreateLabel("used resources:", pos);
        pos -= lineDistanceVector;

        foreach (string res in resources)
        {
            if (res == null || res == "")
            {
                pos -= lineDistanceVector;
                continue;
            }
            CreateSmallLabel(res, pos);
            pos -= lineDistanceVector;
        }

        pos -= (2 * lineDistanceVector);

        logoImage.transform.position = pos;
        createdLines.Add(logoImage.gameObject);

        pos -= 2 * lineDistanceVector;

        foreach (string line in companyNameLines)
        {
            if (line == null || line == "")
            {
                pos -= labelToTextDistanceVector;
                continue;
            }
            AddLine(line, pos);
            pos -= labelToTextDistanceVector;
        }
    }

    private void AddLine(string line, Vector2 pos)
    {
        string[] parts = line.Split('|');

        CreateLabel(parts[0], pos);
        CreateText(parts[1], pos);
    }

    private void CreateText(string text, Vector2 pos)
    {
        GameObject instance = Instantiate(textPrefab, pos - labelToTextDistanceVector, Quaternion.identity, parentObject.transform);

        instance.GetComponent<TMPro.TextMeshProUGUI>().SetText(text);

        createdLines.Add(instance);
    }

    private void CreateLabel(string text, Vector2 pos)
    {
        GameObject label = Instantiate(labelPrefab, pos, Quaternion.identity, parentObject.transform);

        label.GetComponent<TMPro.TextMeshProUGUI>().SetText(text);

        createdLines.Add(label);
    }

    private void CreateSmallLabel(string text, Vector2 pos)
    {
        GameObject label = Instantiate(smallLabelPrefab, pos, Quaternion.identity, canvas.transform);

        label.GetComponent<TMPro.TextMeshProUGUI>().SetText(text);

        createdLines.Add(label);
    }

    void Update()
    {

        if (stopped)
        {
            if (Input.anyKeyDown)
            {
                StopCoroutine(fadeout);
                SceneManager.LoadScene("MainMenu");
                return;
            }
            return;
        }
        foreach (GameObject obj in createdLines)
        {
            obj.transform.position += (Vector3)textSpeedVector;
        }
        GameObject lastElement = createdLines[createdLines.Count - 1];
        if ((lastElement.transform.position.y >= canvas.transform.position.y) || Input.anyKeyDown)
        {
            stopped = true;
            fadeout = StartCoroutine(FadeToBlack());
        }
    }

    IEnumerator FadeToBlack()
    {
        while (true)
        {
            curtain.color = new Color(curtain.color.r, curtain.color.g, curtain.color.b, Mathf.Lerp(curtain.color.a, 1, 0.002F));

            if (curtain.color.a > 0.9F)
            {
                SceneManager.LoadScene("MainMenu");
                yield break;
            }
            yield return null;
        }
    }
}
