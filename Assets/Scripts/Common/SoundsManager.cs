﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsManager : MonoBehaviour
{
    public AudioClip clickSound;

    internal AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayClickSound()
    {
        if (SoundsDisabled())
        {
            return;
        }
        audioSource.PlayOneShot(clickSound);
    }

    public static bool SoundsDisabled()
    {
        return PlayerPrefs.GetInt("SoundEnabled", 1) == 0;
    }

    public static void SetSounds(bool enabled)
    {
        PlayerPrefs.SetInt("SoundEnabled", enabled ? 1 : 0);
    }
}
