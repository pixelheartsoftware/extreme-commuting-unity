﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedButtonBorderController : MonoBehaviour
{
    public GameObject topLeft, topRight, bottomLeft, bottomRight;

    public GameObject selectedObject;

    public float maxOffset = 10;

    public float minOffset = -10;

    public float stepSize = 0.1F;
    public float stepSizePulsating = 0.01F;

    private bool growing;

    public Vector3 minPulsatingAddScale = Vector3.zero, maxPulsatingAddScale = new Vector3(0.5F, 0.5F, 0);
    public Vector3 minPulsatingScale, maxPulsatingScale;

    private Vector3 currentPulsatingScale, previousPulsatingScale;

    private RectTransform selectedTransform;
    private Image selectedImage;
    private ButtonController selectedButtonController;

    private Dictionary<KeyCode, bool> pressedKeys = new Dictionary<KeyCode, bool>();

    private float offset;

    private Vector3 prevMousePos;

    void Start()
    {
        growing = true;
        offset = maxOffset;

        SelectButton(selectedObject, SelectionMethod.NULL);

        SetupBorders();

        pressedKeys[KeyCode.UpArrow] = false;
        pressedKeys[KeyCode.DownArrow] = false;
        pressedKeys[KeyCode.LeftArrow] = false;
        pressedKeys[KeyCode.RightArrow] = false;
        pressedKeys[KeyCode.Return] = false;

        prevMousePos = Input.mousePosition;
    }

    public void SelectButton(GameObject go, SelectionMethod method)
    {

        if (selectedButtonController != null && selectedButtonController.blocked)
        {
            return;
        }

        if (go == null)
        {
            selectedTransform = null;
            selectedImage = null;
            selectedButtonController = null;
            return;
        }

        ButtonController newController = go.GetComponent<ButtonController>();

        if (newController == selectedButtonController)
        {
            return;
        }

        if (selectedButtonController != null && selectedButtonController.isPulsatingButton)
        {
            selectedButtonController.transform.localScale = previousPulsatingScale;
        }
        
        selectedObject = go;
        selectedTransform = (RectTransform)go.transform;
        selectedImage = go.GetComponent<Image>();

        selectedButtonController = newController;

        if (selectedButtonController.onSelect != null)
        {
            selectedButtonController.onSelect.Invoke(method);
        }

        if (selectedButtonController.isPulsatingButton)
        {
            previousPulsatingScale = selectedButtonController.transform.localScale;

            minPulsatingScale = selectedTransform.localScale + (selectedButtonController.minPulsatingAddScale != Vector3.zero ? selectedButtonController.minPulsatingAddScale : minPulsatingAddScale);
            maxPulsatingScale = selectedTransform.localScale + (selectedButtonController.maxPulsatingAddScale != Vector3.zero ? selectedButtonController.maxPulsatingAddScale : maxPulsatingAddScale);

            currentPulsatingScale = minPulsatingScale;

            growing = true;

            topLeft.SetActive(false);
            topRight.SetActive(false);
            bottomLeft.SetActive(false);
            bottomRight.SetActive(false);
        } else
        {
            offset = minOffset;

            topLeft.SetActive(true);
            topRight.SetActive(true);
            bottomLeft.SetActive(true);
            bottomRight.SetActive(true);
        }
    }

    private void SetupBorders()
    {
        if (selectedButtonController == null || selectedButtonController.blocked)
        {
            return;
        }
        if (selectedButtonController.isPulsatingButton)
        {
            selectedButtonController.transform.localScale = currentPulsatingScale;
        } else
        {
            float halfWidht = selectedTransform.rect.width * 0.5F;
            float halfHeight = selectedTransform.rect.height * 0.5F;

            topLeft.transform.position = selectedImage.transform.position - new Vector3(halfWidht + offset, -halfHeight - offset, 0);
            topRight.transform.position = selectedImage.transform.position - new Vector3(-halfWidht - offset, -halfHeight - offset, 0);
            bottomLeft.transform.position = selectedImage.transform.position - new Vector3(halfWidht + offset, halfHeight + offset, 0);
            bottomRight.transform.position = selectedImage.transform.position - new Vector3(-halfWidht - offset, halfHeight + offset, 0);
        }
    }


    private void Update()
    {
        if (MouseMoved())
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if (selectedButtonController == null)
        {
            return;
        }

        if (!selectedButtonController.blocked)
        {
            if (selectedButtonController.up != null && (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxis("PadVertical") > 0))
            {
                if (!pressedKeys[KeyCode.UpArrow])
                {
                    SelectButton(selectedButtonController.up, SelectionMethod.KEYBOARD);
                    pressedKeys[KeyCode.UpArrow] = true;
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                }
            } else
            {
                pressedKeys[KeyCode.UpArrow] = false;
            }
            if (selectedButtonController.down != null && (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxis("PadVertical") < 0))
            {
                if (!pressedKeys[KeyCode.DownArrow])
                {
                    SelectButton(selectedButtonController.down, SelectionMethod.KEYBOARD);
                    pressedKeys[KeyCode.DownArrow] = true;
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                }
            }
            else
            {
                pressedKeys[KeyCode.DownArrow] = false;
            }
            if (selectedButtonController.left != null && (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxis("PadHorizontal") < 0))
            {
                if (!pressedKeys[KeyCode.LeftArrow])
                {
                    SelectButton(selectedButtonController.left, SelectionMethod.KEYBOARD);
                    pressedKeys[KeyCode.LeftArrow] = true;
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                }
            }
            else
            {
                pressedKeys[KeyCode.LeftArrow] = false;
            }
            if (selectedButtonController.right != null && (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxis("PadHorizontal") > 0))
            {
                if (!pressedKeys[KeyCode.RightArrow]) {
                    SelectButton(selectedButtonController.right, SelectionMethod.KEYBOARD);
                    pressedKeys[KeyCode.RightArrow] = true;
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                }
            }
            else
            {
                pressedKeys[KeyCode.RightArrow] = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space) || Input.GetAxis("PadA") > 0F)
        {
            if (selectedButtonController.isActiveAndEnabled && selectedButtonController.interactable)
            {
                if (!pressedKeys[KeyCode.Return])
                {
                    selectedButtonController.onAction.Invoke();
                    VibrationUtil.Instance.AddHiFreq(0.5F);
                    pressedKeys[KeyCode.Return] = true;
                }

            }
        } else
        {
            pressedKeys[KeyCode.Return] = false;
        }

        if (!selectedButtonController.isActiveAndEnabled || !selectedButtonController.interactable)
        {
            if (selectedButtonController.defaultIfInactive != null)
            {
                SelectButton(selectedButtonController.defaultIfInactive, SelectionMethod.NULL);
            } else
            {
                SelectButton(null, SelectionMethod.NULL);
            }
        }

        if (selectedButtonController != null && !selectedButtonController.blocked)
        {
            if (selectedButtonController.isPulsatingButton)
            {
                if (growing)
                {
                    currentPulsatingScale = Vector3.MoveTowards(currentPulsatingScale, maxPulsatingScale, (selectedButtonController.stepSizePulsating != 0 ? selectedButtonController.stepSizePulsating : stepSizePulsating) * Time.deltaTime);

                    if (Mathf.Abs((currentPulsatingScale - maxPulsatingScale).magnitude) < 0.01F)
                    {
                        growing = false;
                    }
                }
                else
                {
                    currentPulsatingScale = Vector3.MoveTowards(currentPulsatingScale, minPulsatingScale, (selectedButtonController.stepSizePulsating != 0 ? selectedButtonController.stepSizePulsating : stepSizePulsating) * Time.deltaTime);

                    if (Mathf.Abs((currentPulsatingScale - minPulsatingScale).magnitude) < 0.01F)
                    {
                        growing = true;
                    }
                }
            }
            else
            {
                if (growing)
                {
                    offset = Mathf.MoveTowards(offset, maxOffset, stepSize * Time.deltaTime);

                    if (Mathf.Abs(maxOffset - offset) < 0.01F)
                    {
                        growing = false;
                    }
                }
                else
                {
                    offset = Mathf.MoveTowards(offset, minOffset, stepSize * Time.deltaTime);

                    if (Mathf.Abs(minOffset - offset) < 0.01F)
                    {
                        growing = true;
                    }
                }
            }
        }


        SetupBorders();
    }

    private bool MouseMoved()
    {
        if ((prevMousePos - Input.mousePosition).magnitude > 0)
        {
            prevMousePos = Input.mousePosition;
            return true;
        }
        return false;
    }
}

public enum SelectionMethod
{
    MOUSE, KEYBOARD, NULL
}