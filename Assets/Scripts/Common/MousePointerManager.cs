﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MousePointerManager : Singleton<MousePointerManager>
{
#if !UNITY_ANDROID
    public Texture2D cursorTexture;
    public Texture2D cursorPressedTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    private bool cursorDisabled;

    public static MousePointerManager Instance
    {
        get { return ((MousePointerManager)_Instance); }
        set { _Instance = value; }
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += OnSceneLoad;
    }

    private void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Gameplay")
        {
            cursorDisabled = true;
        }
        else
        {
            cursorDisabled = false;
        }

        Cursor.visible = false;
    }

    private void Update()
    {
        if (cursorDisabled)
        {
            return;
        }
        if (cursorPressedTexture == null || cursorTexture == null)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Cursor.SetCursor(cursorPressedTexture, hotSpot, cursorMode);  
        } else
        if (Input.GetMouseButtonUp(0))
        {
            Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        }
    }

    void Start()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
#endif
}
