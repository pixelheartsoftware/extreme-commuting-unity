﻿using GameJolt.API;
using GameJolt.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameJoltUtil : MonoBehaviour
{
    public static bool __GameJoltEnabled = false;

    public bool GameJoltEnabled = false;

    void Start()
    {
        __GameJoltEnabled = GameJoltEnabled;
    }

    public static bool IsGameJoltEnabled()
    {
        return __GameJoltEnabled;
    }

    internal static void ShowLeaderboards()
    {
        bool isSignedIn = GameJoltAPI.Instance.HasSignedInUser;
        if (!isSignedIn)
        {
            GameJoltUI.Instance.ShowSignIn(
              (bool signInSuccess) =>
              {
                  if (signInSuccess)
                  {
                      GameJoltUI.Instance.ShowLeaderboards();
                  }
              });
        }
        else
        {
            GameJoltUI.Instance.ShowLeaderboards();
        }
    }

    internal static void ShowAchievements()
    {
        bool isSignedIn = GameJoltAPI.Instance.HasSignedInUser;
        if (!isSignedIn)
        {
            GameJoltUI.Instance.ShowSignIn(
              (bool signInSuccess) =>
              {
                  if (signInSuccess)
                  {
                      GameJoltUI.Instance.ShowTrophies();
                  }
              });
        }
        else
        {
            GameJoltUI.Instance.ShowTrophies();
        }
    }

    internal static void PublishLeaderboards(string carName, int score)
    {
        int board = 0;
        switch (carName)
        {
            case "Red":
                board = 502923;
                break;
            case "Blue":
                board = 502922;
                break;
            case "Yellow":
                board = 502924;
                break;
        }

        bool isSignedIn = GameJoltAPI.Instance.HasSignedInUser;
        if (!isSignedIn)
        {
            GameJoltUI.Instance.ShowSignIn(
              (bool signInSuccess) =>
              {
                  if (signInSuccess)
                  {
                      GameJolt.API.Scores.Add(score, score.ToString(), 0, null, (bool success) => { });
                      GameJolt.API.Scores.Add(score, score.ToString(), board, null, (bool success) => { });
                  }
              });
        }
        else
        {
            GameJolt.API.Scores.Add(score, score.ToString(), 0, null, (bool success) => { });
            GameJolt.API.Scores.Add(score, score.ToString(), board, null, (bool success) => { });
        }
    }

    internal static void UlockAchievement(int achievement)
    {

        bool isSignedIn = GameJoltAPI.Instance.HasSignedInUser;
        if (!isSignedIn)
        {
            GameJoltUI.Instance.ShowSignIn(
              (bool signInSuccess) =>
              {
                  if (signInSuccess)
                  {
                      GameJolt.API.Trophies.Unlock(achievement);
                  }
              });
        }
        else
        {
            GameJolt.API.Trophies.Unlock(achievement);
        }
    }
}
