﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class VibrationUtil : Singleton<VibrationUtil>
{
    public float lowFreq = 0;
    public float hiFreq = 0;

    public static VibrationUtil Instance
    {
        get { return ((VibrationUtil)_Instance); }
        set { _Instance = value; }
    }

    public void AddHiFreq(float power)
    {
        hiFreq = Mathf.Clamp(hiFreq + power, 0, 10);
    }

    public void ResetHiFreq(float power)
    {
        hiFreq = 0;
    }

    public void AddLowFreq(float power)
    {
        lowFreq = Mathf.Clamp(lowFreq + power, 0, 10);
    }

    public void ResetLowFreq(float power)
    {
        lowFreq = 0;
    }

    private void Update()
    {
#if UNITY_ANDROID
        if (lowFreq > 0 || hiFreq > 0) {
            int vib = Mathf.Clamp((int)(Mathf.Lerp(0, 255, hiFreq + lowFreq / 20)), 0, 255);
            if (vib > 0) {
                VibrationHelper.VibrateWithAmplitude((int)(Time.deltaTime * 1000), vib);
            }
        }
#elif !UNITY_WEBGL
        if (Gamepad.current != null)
        {
            Gamepad.current.SetMotorSpeeds(lowFreq, hiFreq);
        }
#endif

        lowFreq = Mathf.Lerp(Mathf.Clamp(lowFreq, 0, 1000), 0, 0.5F);
        hiFreq = Mathf.Lerp(Mathf.Clamp(hiFreq, 0, 1000), 0, 0.5F);

        if (lowFreq < 0.001F)
        {
            lowFreq = 0;
        }
        if (hiFreq < 0.001F)
        {
            hiFreq = 0;
        }
    }
}
