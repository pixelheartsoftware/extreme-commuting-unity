﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetStateController : MonoBehaviour
{
    public bool deleteUserPrefs;

    void Awake()
    {
        if (!deleteUserPrefs)
        {
            return;
        }
        PlayerPrefs.DeleteAll();
    }

}
