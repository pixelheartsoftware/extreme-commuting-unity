﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonController : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler
{
    public SelectedButtonBorderController selectController;

    public UnityEvent onAction;

    public ButtonSelectedEvent onSelect;

    public GameObject left, right, up, down;

    public GameObject defaultIfInactive;

    public string padButton;
    private bool padDebounce = false;

    public bool isPulsatingButton = false;
    internal bool interactable = true;

    internal bool blocked = false;

    public Vector3 minPulsatingAddScale, maxPulsatingAddScale;
    public float stepSizePulsating = 0;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isActiveAndEnabled && interactable)
        {
            onAction.Invoke();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        selectController.SelectButton(gameObject, SelectionMethod.MOUSE);
    }

    private void Update()
    {
        if (padButton != null && padButton != "" && Input.GetAxis(padButton) > 0F)
        {
            if (isActiveAndEnabled && interactable)
            {
                if (!padDebounce) {
                    onAction.Invoke();
                    padDebounce = true;
                }
            } 
        }
        else
        {
            padDebounce = false;
        }
    }
}


[System.Serializable]
public class ButtonSelectedEvent : UnityEvent<SelectionMethod>
{
}