﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : Singleton<Controls>
{
    public static Controls Instance
    {
        get { return ((Controls)_Instance); }
        set { _Instance = value; }
    }

#if !UNITY_ANDROID
    public static bool onScreenControls = false;
#elif UNITY_ANDROID
    public static bool onScreenControls = true;
#endif

    public float tapPressSeconds = 0.2F;
    public float tapReleaseSeconds = 0.2F;

    public float keyIncreaseFraction = 01F;

    public float joypadDeadZone = 0.1F;

    private bool padTurbo = false;
    private float padTurboTime = 0;

    private bool padBrakeCheck = false;
    private float padBrakeCheckTime = 0;

    private Dictionary<BasicActionType, float> realButtonPressTimes = new Dictionary<BasicActionType, float>();
    private Dictionary<BasicActionType, float> realButtonReleaseTimes = new Dictionary<BasicActionType, float>();

    private Dictionary<BasicActionType, float> basicActions = new Dictionary<BasicActionType, float>();
    private Dictionary<ReleaseActionType, float> releaseActions = new Dictionary<ReleaseActionType, float>();

    private static BasicActionType[] BASIC_ACTIONS = (BasicActionType[])Enum.GetValues(typeof(BasicActionType));

    private void Start()
    {
        foreach (BasicActionType basicAction in BASIC_ACTIONS)
        {
            realButtonPressTimes[basicAction] = 0F;
            realButtonReleaseTimes[basicAction] = 0F;

            basicActions[basicAction] = 0F;
        }

        foreach (ReleaseActionType releaseAction in (ReleaseActionType[])Enum.GetValues(typeof(ReleaseActionType)))
        {
            releaseActions[releaseAction] = 0F;
        }
    }

    internal void ResetTurbo()
    {
        releaseActions[ReleaseActionType.TURBO] = 0F;
    }

#if !UNITY_ANDROID
    private void Update()
    {
        bool keyboardUsed = false;
        foreach (BasicActionType buttonType in BASIC_ACTIONS)
        {
            KeyCode code = KeyCodeForButtonType(buttonType);

            InputAction inputAction = InputAction.NONE;

            if (Input.GetKeyUp(code))
            {
                inputAction = InputAction.RELEASE;
                keyboardUsed = true;
            }
            else if (Input.GetKeyDown(code))
            {
                inputAction = InputAction.PRESS;
                keyboardUsed = true;
            }
            else if (Input.GetKey(code))
            {
                inputAction = InputAction.STAY;
                keyboardUsed = true;
            }

            SetButton(buttonType, inputAction);
        }

#if !UNITY_ANDROID
        if (!keyboardUsed)
        {
            HandlePad();
        }
#endif
    }

    private void HandlePad()
    {
        float padHor = Input.GetAxis("PadHorizontal");

        if (padHor < -joypadDeadZone)
        {
            SetButtonTime(BasicActionType.LEFT, Mathf.Abs(padHor));
            SetButtonTime(BasicActionType.RIGHT, 0);
        }
        else if (padHor > joypadDeadZone) 
        {
            SetButtonTime(BasicActionType.LEFT, 0);
            SetButtonTime(BasicActionType.RIGHT, Mathf.Abs(padHor));
        }
        else
        {
            SetButtonTime(BasicActionType.RIGHT, 0);
            SetButtonTime(BasicActionType.LEFT, 0);
        }

        float padAcc = Input.GetAxis("PadAccelerate");
        if (padAcc > joypadDeadZone)
        {
            SetButtonTime(BasicActionType.ACCELERATION, Mathf.Abs(padAcc));
        }

        float padBrake = Input.GetAxis("PadBrake");
        if (padBrake > joypadDeadZone)
        {
            SetButtonTime(BasicActionType.BRAKE, Mathf.Abs(padBrake));
        }

        float padTr = Input.GetAxis("PadA");
        if (padTr > joypadDeadZone)
        {
            if (!padTurbo)
            {
                padTurboTime = Time.realtimeSinceStartup;
            }

            padTurbo = true;
        } else
        {
            padTurbo = false;
        }

        float padBrCh = Input.GetAxis("PadB");
        if (padBrCh > joypadDeadZone)
        {
            if (!padBrakeCheck)
            {
                padBrakeCheckTime = Time.realtimeSinceStartup;
            }

            padBrakeCheck = true;
        } else
        {
            padBrakeCheck = false;
        }
    }
#endif

    private ReleaseActionType GetReleaseAction(BasicActionType buttonType)
    {
        switch (buttonType)
        {
            case BasicActionType.ACCELERATION:
                return ReleaseActionType.TURBO;
            case BasicActionType.BRAKE:
                return ReleaseActionType.BRAKE_CHECK;
        }
        return ReleaseActionType.NONE;
    }

    private KeyCode KeyCodeForButtonType(BasicActionType buttonType)
    {
        switch (buttonType)
        {
            case BasicActionType.ACCELERATION:
                return KeyCode.UpArrow;
            case BasicActionType.BRAKE:
                return KeyCode.DownArrow;
            case BasicActionType.LEFT:
                return KeyCode.LeftArrow;
            case BasicActionType.RIGHT:
                return KeyCode.RightArrow;
        }
        throw new ArgumentException("This also should not happen. Argument: " + buttonType);
    }

    public void SetButton(BasicActionType buttonType, InputAction inputAction)
    {
        if (inputAction == InputAction.PRESS)
        {
            basicActions[buttonType] = Mathf.Clamp(basicActions[buttonType] + Time.deltaTime * 5F, 0, 1);
            realButtonPressTimes[buttonType] = Time.realtimeSinceStartup;

            float lastReleaseTime = realButtonReleaseTimes[buttonType];

            if ((Time.realtimeSinceStartup - lastReleaseTime) < tapReleaseSeconds)
            {
                releaseActions[GetReleaseAction(buttonType)] = Time.realtimeSinceStartup;
            }
            else
            {
                releaseActions[GetReleaseAction(buttonType)] = 0;
            }
        }
        else
        if (inputAction == InputAction.RELEASE)
        {
            float lastPressTime = realButtonPressTimes[buttonType];

            realButtonReleaseTimes[buttonType] = Time.realtimeSinceStartup;
        }
        else
        if (inputAction == InputAction.STAY)
        {
            basicActions[buttonType] = Mathf.Clamp(basicActions[buttonType] + Time.deltaTime * 5F, 0, 1);
        }
        else
        if (basicActions[buttonType] > 0)
        {
            basicActions[buttonType] = 0;
            releaseActions[GetReleaseAction(buttonType)] = 0;
        }
        else
        {
            releaseActions[GetReleaseAction(buttonType)] = 0;
        }
    }

    public void SetButtonTime(BasicActionType buttonType, float value)
    {
        if ((basicActions[buttonType] == 0) && (value > 0))
        {
            basicActions[buttonType] = Mathf.Clamp(value, 0, 1);
            realButtonPressTimes[buttonType] = Time.realtimeSinceStartup;

            float lastReleaseTime = realButtonReleaseTimes[buttonType];

            if ((Time.realtimeSinceStartup - lastReleaseTime) < tapReleaseSeconds)
            {
                releaseActions[GetReleaseAction(buttonType)] = Time.realtimeSinceStartup;
            }
            else
            {
                releaseActions[GetReleaseAction(buttonType)] = 0;
            }
        } else
        if (value == 0)
        {
            float lastPressTime = realButtonPressTimes[buttonType];

            realButtonReleaseTimes[buttonType] = Time.realtimeSinceStartup;
        }
        else
        if ((basicActions[buttonType] > 0) && (value > 0))
        {
            basicActions[buttonType] = Mathf.Clamp(value, 0, 1);
        }
        else
        if (basicActions[buttonType] > 0)
        {
            basicActions[buttonType] = 0;
            releaseActions[GetReleaseAction(buttonType)] = 0;
        }
        else
        {
            releaseActions[GetReleaseAction(buttonType)] = 0;
        }
    }

    // bool
    public bool GetAcceleration()
    {
        return basicActions[BasicActionType.ACCELERATION] > 0;
    }

    public bool GetBrake()
    {
        return basicActions[BasicActionType.BRAKE] > 0;
    }

    public bool GetLeft()
    {
        return basicActions[BasicActionType.LEFT] > 0;
    }

    public bool GetRight()
    {
        return basicActions[BasicActionType.RIGHT] > 0;
    }

    // float
    public float GetAccelerationFloat()
    {
        return basicActions[BasicActionType.ACCELERATION];
    }

    public float GetBrakeFloat()
    {
        return basicActions[BasicActionType.BRAKE];
    }

    public float GetLeftFloat()
    {
        return basicActions[BasicActionType.LEFT];
    }

    public float GetRightFloat()
    {
        return basicActions[BasicActionType.RIGHT];
    }

    // Special

    public bool GetTurbo()
    {
        return (padTurbo) || (releaseActions[ReleaseActionType.TURBO] > 0);
    }

    public bool GetBrakeCheck()
    {
        return (padBrakeCheck) || (releaseActions[ReleaseActionType.BRAKE_CHECK] > 0);
    }

    public float GetBrakeCheckSeconds()
    {
        if (padBrakeCheck)
        {
            return Time.realtimeSinceStartup - padBrakeCheckTime;
        }
        return Time.realtimeSinceStartup - releaseActions[ReleaseActionType.BRAKE_CHECK];
    }
}

public enum BasicActionType
{
    ACCELERATION, BRAKE, LEFT, RIGHT
}

public enum ReleaseActionType
{
    TURBO, BRAKE_CHECK, LEFT_RAM, RIGHT_RAM, NONE
}

public enum InputAction
{
    PRESS, RELEASE, STAY, NONE
}