﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : Singleton<MusicManager>
{
    public AudioClip mainMusic;

    public AudioClip carSelectionMusic;

    public AudioClip authorsMusic;

    public AudioClip[] gameplayMusic;

    public AudioClip gameplayWinMusic;

    AudioSource audioSource;

    public static MusicManager Instance
    {
        get { return ((MusicManager)_Instance); }
        set { _Instance = value; }
    }

    public void LerpVolume(float desiredVolume, float percent)
    {
        if (SoundsManager.SoundsDisabled()) {
            return;
        }
        audioSource.volume = Mathf.Lerp(audioSource.volume, desiredVolume, percent);
    }

    public void PlayWinMusic()
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        audioSource.Stop();
        audioSource.clip = gameplayWinMusic;
        audioSource.Play();
    }

    public void SetMusic()
    {
        if (audioSource == null)
        {
            audioSource = GetComponent<AudioSource>();
        }

        if (!SoundsManager.SoundsDisabled())
        {
            if (audioSource.isPlaying)
            {
                audioSource.UnPause();
            } else
            {
                audioSource.Play();
            }
            
        } else
        {
            audioSource.Pause();
        }
    }

    private void Start()
    {
        if (!MusicManager.Instance.Equals(this))
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        audioSource = GetComponent<AudioSource>();

        audioSource.clip = mainMusic;

        if (!SoundsManager.SoundsDisabled())
        {
            audioSource.Play();
        }

        SceneManager.sceneLoaded += OnSceneLoad;
    }

    private void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        if (SoundsManager.SoundsDisabled())
        {
            return;
        }
        if (scene.name == "MainMenu")
        {
            if (audioSource.isPlaying && (audioSource.clip.Equals(mainMusic)))
            {
                return;
            }
            audioSource.Stop();
            audioSource.clip = mainMusic;
            audioSource.Play();
        } else if (scene.name == "Gameplay")
        {
            AudioClip selected = gameplayMusic[UnityEngine.Random.Range(0, gameplayMusic.Length)];

            audioSource.Stop();
            audioSource.clip = selected;
            audioSource.Play();
        } else if (scene.name == "CarSelection" || scene.name == "Achievements")
        {
            if (audioSource.isPlaying && (audioSource.clip == carSelectionMusic))
            {
                return;
            }
            audioSource.Stop();
            audioSource.clip = carSelectionMusic;
            audioSource.Play();
        } else if (scene.name == "Summary")
        {
            if (audioSource.isPlaying && (audioSource.clip == carSelectionMusic || audioSource.clip == gameplayWinMusic))
            {
                return;
            }
            audioSource.Stop();
            audioSource.clip = carSelectionMusic;
            audioSource.Play();
        } else if (scene.name == "Authors")
        {
            if (audioSource.isPlaying && (audioSource.clip == authorsMusic))
            {
                return;
            }
            audioSource.Stop();
            audioSource.clip = authorsMusic;
            audioSource.Play();
        }
    }
}
