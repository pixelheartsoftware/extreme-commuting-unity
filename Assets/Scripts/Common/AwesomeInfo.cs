﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwesomeInfo : Singleton<AwesomeInfo>
{
    private int totalAwesome;
    private Dictionary<StuntType, int> stuntCounts = new Dictionary<StuntType, int>();

    private int longestCombo;

    private int crashedCars;

    private bool accelerationUsed;

    private float leftLaneTotalTime = 0;

    private float raceLength = 0;
    private float raceLengthCompleted = 0;

    private bool impossible = true;

    internal string CarName { get; set; }

    public static AwesomeInfo Instance
    {
        get { return ((AwesomeInfo)_Instance); }
        set { _Instance = value; }
    }

    private void Awake()
    {
        if (AwesomeInfo.Instance != this)
        {
            AwesomeInfo.Instance.Awake();
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    internal int GetCalculatedPoints()
    {
        return (int)(GetRacePercentage() * totalAwesome * 0.001F) * 10;
    }

    internal Dictionary<StuntType, int> GetStuntCounts()
    {
        return stuntCounts;
    }

    internal void Reset()
    {
        foreach (StuntType stuntType in (StuntType[])System.Enum.GetValues(typeof(StuntType)))
        {
            stuntCounts[stuntType] = 0;
        }
        totalAwesome = 0;
        longestCombo = 0;
        crashedCars = 0;
        leftLaneTotalTime = 0;
        accelerationUsed = false;
        impossible = true;
    }

    internal void AddCombo(int comboLength)
    {
        if (longestCombo < comboLength)
        {
            longestCombo = comboLength;
        }
    }

    internal void SetNoImpossible()
    {
        impossible = false;
    }

    internal void AddStunt(StuntType type)
    {
        stuntCounts[type]++;
    }

    internal int AddPoints(int points)
    {
        return totalAwesome += points;
    }

    internal int GetPoints()
    {
        return totalAwesome;
    }

    internal void AddCrashedCar()
    {
        crashedCars++;
    }

    internal int GetCrashedCars()
    {
        return crashedCars;
    }

    internal void SetRaceLengthCompleted(float length)
    {
        raceLengthCompleted = length;
    }

    internal void SetRaceLength(float length)
    {
        raceLength = length;
    }

    internal int GetRacePercentage()
    {
        if (raceLength == 0)
        {
            return 0;
        }
        return (int)(raceLengthCompleted * 100 / raceLength);
    }

    internal int GetLongestCombo()
    {
        return longestCombo;
    }

    internal float GetLeftLaneTotalTime()
    {
        return leftLaneTotalTime;
    }

    internal void AddLeftLaneTotalTime(float time)
    {
        leftLaneTotalTime += time;
    }

    internal bool WasAccelerationUsed()
    {
        return accelerationUsed;
    }

    internal void SetAccelerationUsed()
    {
        accelerationUsed = true;
    }

    internal bool ImpossibleAchieved()
    {
        return impossible;
    }
}
